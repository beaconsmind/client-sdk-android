
# Usage guide

## Prerequisites

As a Beaconsmind client, a contained environment is provided for Beaconsmind services being hosted and maintained by Beaconsmind.

The hostname of the provided environment is required to properly access Beaconsmind API and to use Beaconsmind SDK

> example: `https://adidas.bms.beaconsmind.com`

Please contact Beaconsmind support for the required information about acquiring a valid environment and hostname.

## 1. Initialize the SDK

Initialize the SDK by calling the BeaconsmindSdk.initialize() method, passing that method an Application context and BeaconsmindConfig object. 
Do this as soon as possible after your app starts, in the onCreate() method of your Application class.

```java
public class DemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        String suitUri = "SUITE_URL";
        BeaconsmindConfig.Builder builder = new BeaconsmindConfig.Builder(suitUri)
                .usePassiveScanning(false) // true for scanning without persistent notification
                // notification setting is not-required for passive scanning
                .setNotificationBadge(R.drawable.ic_beacons)
                .setNotificationTitle("Beaconsmind")
                .setNotificationText("Listening for Beacons")
                .setNotificationChannelName("Beacon Listener Notification");

        BeaconsmindSdk.initialize(this, builder.build(this));
    }
}
```

## 2. Authentication 

### When using Beaconsmind authentication

This approach implies that the customer data is kept in Beaconsmind backend. Use `UserManager.login` method to perform customer authentication.

```java
UserManager userManager = UserManager.getInstance();
userManager.login(username, password, new OnCompleteListener() {
    @Override
    public void onSuccess() {
        // succesfull login, proceed with your flow
    }

    @Override
    public void onError(Error error) {
        // login failed, handle error
    }
});
```

To register new customer use `UserManager.register`

```java
UserManager userManager = UserManager.getInstance();

CreateUserRequest userRequest = new CreateUserRequest();
userRequest.setUsername("email");
userRequest.setFirstName("firstname");
userRequest.setLastName("lastname");
userRequest.password("password");
userRequest.confirmPassword("confirmPassword");

userManager.createAccount(username, password, new OnCompleteListener() {
    @Override
    public void onSuccess() {
        // succesfull register, user is authenticated, proceed with your flow
    }

    @Override
    public void onError(Error error) {
        // register failed, handle error
    }
});
```

### When using own authentication mechanism

This approach implies that the customer data is kept in clients backend. The authentication is done by the client app before the Beaconsmind API is used. Whenever customer is authenticate, use `UserManager.importAccount` method to send basic customer data to Beaconsmind. The customer data is updated only the first time the method is called. The client app can track if the customer was imported before and omit personal data from following `importAccount` calls

```java
UserManager userManager = UserManager.getInstance();

ImportUserRequest importUserRequest = new ImportUserRequest();
importUserRequest.setId("id") // must not change for customer
importUserRequest.setUsername("username");
importUserRequest.setFirstName("firstname");
importUserRequest.setLastName("lastname");
importUserRequest.setBirthDate(Date);
importUserRequest.setGender("gender");
importUserRequest.setLanguage("language");
importUserRequest.setDeviceRegistration(DeviceRegistrationModel);

userManager.importAccount(importUserRequest, new OnCompleteListener() {
    @Override
    public void onSuccess() {
        // succesfull login, proceed with your flow
    }

    @Override
    public void onError(Error error) {
        // login failed, handle error
    }
});
```

### Third party authentication

If you are using third party authentication providers use `UserManager.externalLogin` after the user has successfully authenticated via the provider 

- Currently supported providers are Facebook, Google and Apple

### Logging out

Logging out by unsubscribing from push notifications, clearing the current device session.

```kotlin
UserManager.getInstance().logout(object : OnCompleteListener {
    override fun onSuccess() {
        
    }
    
    override fun onError(error: Error) {
        
    }
})
```

### Logging out (Deprecated)

Logging out by clearing the current device session.

```kotlin
UserManager.getInstance().logout()
```

### Update user profile

Update user profile stored on beaconsmind servers.

```kotlin
UserManager.getInstance().updateAccount(request, listener)
```

## 3. Beacon ranging

For beacon ranging you should prompt the user to grant the required [permissions](Beacons.md/#permissions).

Basic functionality provided by the SDK is beacon ranging and proximity reporting. Based on these signals, the backend can react accordingly and sent marketing push notifications to the customer. Beacon ranging is enabled by default after a valid session was obtained by calling `register`, `login` or `importAccount`. You can also opt out of beacon ranging by calling these functions with additional parameter `shouldStartService: Boolean` and start them on your own terms

- Beacon ranging is only possible after the user has granted required permissions. So make sure to call `BeaconListenerService#startListeningBeacons` manually in your app to ensure beacon ranging starts after obtaining permissions. 
> When requesting permissions using SDK's `SdkPermissionsHelper#requestPermissions` beacon ranging is (re)started after the user grants permissions succesfully.

The SDK provides two [scanning modes](Beacons.md#scanning-modes), with and without persistent notification. Evaluate your use case, with pros and cons for each one.

To learn more about using beacons, please follow our [Using beacons in general guide](../Documentation/Beacons.md) .

## 4. Push notifications

Push notifications are being sent to User Devices using Beaconsmind SDK to allow customers to receive Offers as results of physical proximity to Beacons.

### Prerequisites

To receive push notifications from Beaconsmind backend by default the SDK uses [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging/android/client). Please contact Beaconsmind developers to request google-services.json for your application and follow steps #3 and #4 in [Get started with Firebase](https://firebase.google.com/docs/android/setup#add-config-file) to set it up.

### Receiving push notifications

In order to receive push notifications you need to create FCM service and declare it in your App's Manifest.

* Android Manifest

```xml
<!--        FCM Push Notification-->
<meta-data
    android:name="com.google.firebase.messaging.default_notification_icon"
    android:resource="@drawable/ic_offers" />
<meta-data
    android:name="com.google.firebase.messaging.default_notification_color"
    android:resource="@color/primary" />
<meta-data
    android:name="com.google.firebase.messaging.default_notification_channel_id"
    android:value="@string/offers_notification_channel_id" />
<service
    android:name=".FCMService"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT" />
    </intent-filter>
</service>
```

> Our beckend sends FCM messages with notification and data payloads set. Which means you will receive messages in your FCMService when the app is in foreground and in your Launcher Activity's intent extras when the app is in the background. Check [Receiving messages](https://firebase.google.com/docs/cloud-messaging/android/receive) for how FCM handles messages.

* [FCMService.kt](../DemoApp/src/main/java/com/beaconsmind/demo/FCMService.kt)

```kotlin
class FCMService : FirebaseMessagingService() {

    override fun onNewToken(s: String) {

        // update device token
        UserManager.getInstance().updateDeviceInfo(s)

    }

    override fun onCreate() {
        super.onCreate()
        
        // initialize notification channel if needed

    }

    override fun handleIntent(intent: Intent) {
        super.handleIntent(intent)

        // intercepts all incoming firebase messages

        val offer = OfferPayload.fromIntent(intent)
        if (offer != null) {
            // mark it read
            OffersManager.getInstance().markOfferAsReceived(offer.offerId)
        }

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        // handle messages when app is in foreground

    }
}
```

* [SplashActivity.java](../DemoApp/src/main/java/com/beaconsmind/demo/ui/SplashActivity.java) (Launcher activity)

```java
public class SplashActivity extends AppCompatActivity {
    

    @Override
    protected void onCreate() {
        super.onCreate();

        // ...

        // parse the FCM message data
        OfferPayload offerPayload = OfferPayload.fromIntent(getIntent());
        if (offerPayload != null) {
            // started from FCM notification
            // handle offer
        }

    }
}
```

> You can check the full example in our [Demo APP](../DemoApp)

### Update device token

Device token is sent automatically by the sdk upon successful authentication via `NativeIdProvider` set in builder.

To update it when it changes use

```kotlin
UserManager.getInstance().updateDeviceInfo(token: String)
```

### Custom push notification delivery

If you do not want to use FCM to deliver push notifications you can plug custom notification delivery in our SDK. Just extend our `NativeIdProvider` class and implement it's `provideId` method, where you can provide your own device registration token which will be delivered to Beaconsmind API after successful user authentication.

> Check out our own [FcmTokenProvider](../BeaconsmindSDK/src/main/java/com/beaconsmind/sdk/FcmTokenProvider.java) for implementation details

Plug it upon initialization with `BeaconsmindConfig.Builder`

```java
BeaconsmindConfig.Builder builder = new BeaconsmindConfig.Builder("suiteUri")
        // ...
        .setNativeIdProvider(new YourIdProvider());
```

## 5. Reporting interaction with received offers

When users receive pushes about a new offer, then reads the offer and optionally redeems the voucher in the offer, backend should be notified about such activity. To get offers use `OffersManager`, which is a direct interface toward the API offered by Beaconsmind.

`OffersManager` contains offer utilites to mark an offer as read an offer as received by an Offer ID. Offer ID can be extracted from push notification data, or from response when fetching list of offers.

> To extract offer from notification data use `OfferPayload.tryParse`

* `OffersManager.markOfferAsReceived` should be called on incoming notifications, immediately after received

* `OffersManager#markOfferAsRead` should be called when offer details are opened

* `OffersManager#markOfferAsRedeemed` should be called when user explicitly Redeems an offer using Redeemed button (should be used only for offers with `isRedeemable` flag set)

* `OffersManager#markOfferClicked` should be called when the user clicks on offer's call-to-action button 
