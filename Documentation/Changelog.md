# Changelog

### 1.12.8

- Offer fragment UI updates
- API Build 2023-11-24

### 1.12.7

- API Build 2023-11-16

### 1.12.6

- API Build 2023-11-02

### 1.12.5

- API Build 2023-10-25

### 1.12.4

- Exposed more info about api network failures
- API Build 2023-09-26

### 1.12.3

- Fix bluetooth failure crash on Android 12+

### 1.12.2

- Fetch remote logging and device diagnostics configuration from backend
- Send device UUID with all touchpoint requests

### 1.12.1

- Added Xamarin sdk platform detection
- Changed proguard rules to include all GSON serialization objects

### 1.12.0

- Added more metrics to SDK health diagnostics
- Refactored internal packages
- Removed deprecated classes
- Complete code documentation coverage

### 1.11.2

- Download touchpoint now tracks app download per device

### 1.11.1

***UI Component***

- Enable offer details hyperlink clicks

***Demo App***

- Display version code
- Better notification navigation handling

### 1.11.0

- Enhancement: Expose more control for **startListeningBeacons**.
  - *breaking change* deprecated startListeningBeacons now throws **StartListeningBeaconsException**. Use `startListeningBeacons(null)` to replicate old behavior. 
  - use `startListeningBeaconsUncaught` for thrown exceptions or `startListeningBeacons { exception ->  }` for inlined call.

- Enhancement: Add async **UserManager#logout(OnCompleteListener)** which unregisters device token from notifications. Deprecate no param version.
    - *minor breaking change* if using custom NativeIdProvider, you have to override `platformType` value

- Feature: Autodetect host app version. Setting app version on builder is optional.

- Enhancement: Expose SdkPermissionsHelper callback via Activity interface

- Feature: Expose OffersManager#markOfferClicked

***UI Component***

- Implemented markOfferClicked touchpoint on CTA button click

***Demo App*** 

- Added predefined backend servers on the Demo app.
- Added missing profile fields

### 1.10.2

- Make `BeaconsmindSdk#initializeDevelop` easier to debug.
- Expose additional offer keys and raw map in OfferPayload

- Feature: Deprecate PermissionChecker in favor of **SdkPermissionsHelper**.
    - SdkPermissionHelper contains a workaround for ActivityResultCaller API limitations.
    - Can be used from any AppCompatActivity or FragmentActivity context.

### 1.10.1

- Add country in register account request.
- Added new diagnostic metrics. Send some metric keys only when in erroneous state.

- Feature: **BeaconsmindSdk#initializeDevelop**. Kickstart sdk testing and integration by a single method call from `Application#onCreate` or `Activity#onCreate`. When initialized with this method the sdk requests permissions on it own until they are granted.

### 1.10.0

- Bump altbeacon lib to v2.18.5-beta9
- Fix: passive scanning not stopping after logout.
- Added a new component for SDK initialization when Application#onCreate is not accessible. See usage in [BeaconsmindSDK/Manifest](../BeaconsmindSDK/src/main/AndroidManifest.xml)

- Added SDK version field in BuildConfig. Not available prior to this release. Needs to be updated in [build.gradle](../BeaconsmindSDK/build.gradle).

- Feature: SDK diagnostics. Periodically send data for SDK health check.

---

### 1.9.5

- Added a param to `PermissionChecker#askForMissingPermission` to toggle "pushy" permission prompts. Defaults to exactly once if the user denied permissions.

- Fix: Incorrect beacon listening state peristance.

### 1.9.4

- Fix: Crash when starting beacon listening without BLUETOOTH_SCAN permission granted.

    Beacon listening is possible only after the permissions are granted. Make sure to manually call `BeaconListenerService#startListeningBeacons` in your app to ensure beacon listening starts after permissions are granted

### 1.9.3

- Deprecated BootCompleteReceiver
- Throw SdkNotInitializedException if sdk was not initialized before accessing sdk.
- Fix: Increase background scan period on android O+. Bug introduced in 1.8.0 affecting active scanning strategy

### 1.9.2

- Fixed login functionality.

### 1.9.1

- Fixed boot completion events handling in BootCompleteReceiver.

### 1.9.0

- Updated API version 2022-07-15
- Simplified `getFrequency` for BeaconContactsSummary
- Improved session handling in BeaconListenerService

---

### 1.8.0

***SDK***

- Introduce log levels. Disabled logging by default.
- Add `isInRange` field to BeaconContactsSummary

***Demo***

- Tint card green if beacon is in range

---

### 1.7.0

***SDK***

- Background detections without persistent notification
- Explicitly enable/disable logging.
- Refactored PermissionChecker. Initialize it with your custom text for permission dialogs.

***Demo***

- Toggle for passive vs notification scanning

---

### 1.6.0

***SDK***

- New Notifications API model
- Avatar upload API
- Request password change API

***UI Component***

- Added offer redeemed listener interface
- Implement rounded corner stylesheet customization
- Wrap offer action buttons on overflow

***Demo***

- Mark each FCM offer delivered

---

### 1.5.1

***SDK***

- Add external social login api

***Demo***

- Update FCM token in FirebaseService.onNewToken callback

### 1.5.0

- Convert main classes to Kotlin
- Target Android 12 (SDK 31) and implement bluetooth scan behavioral change

---

### 1.4.1

***SDK***

- Hide TouchPointHelper class

***Demo***

- Parse FCM notification intent in SplashActivity to correctly open OfferDetails when notification is received in background
- Create "emulator" Build Variant for easier debugging on emulated devices

### 1.4.0

- Exposed offers UI components
- Recover after silent bluetooth errors
- Publish sources .jar to JitPack.io

---

### 1.3.3

- Fix code optimisation which disabled SDK logging when imported from JitPack.

### 1.3.2

- Optionally start listening for beacons after successful login, account creation or account import.
- Persist listening state and restore correct context after process death.

### 1.3.1

- Crash recovery. SDK new recovers from app crashes and correctly re-starts after being in background for too long.
- Automaticaly start BeaconListenerService on SDK initialization.

### 1.3.0

***SDK***

- Allow simulator to be enabled by client, default is disabled.
- Filter beacons with invalid RSSI values.
- Upgrade OpenAPI generator.
- Implement Timber logging. Log altbeacon messages.

***Demo***

- Refactor Beacon Contacts screen to show more information, and have faster refresh rate.
- Log to file. Send application logs from Profile tab.

---

### 1.2.2

- Remove `org.altbeacon` as dependency for client applications.

### 1.2.1

- Add `getBLEActivityLiveData` method to `BeaconListenerService` to allow querying BL interface

### 1.2.0

***SDK***

- Create `ContactBeaconSummary` model and `BeaconUtils.createBeaconSummary` util class
- Add beacon summary getter `BeaconListenerService.getBeaconsSummary` for diagnostics

***Demo***

- Refactor Beacons screen with beacon summary

---

### 1.1.0

***SDK***

- Move FCM token fetcing to SDK
    - Create `NativeIdProvider` abstract class and `FcmTokenProvider` implementation
    - Add `nativeIdProvider` property to `BeaconsmindConfig.Builder`

- Create documentation

- Overload `OffersManager` offer actions

***Demo***

- Stop background monitoring after user log out

---

### 1.0.1

- Update API specification file and generate API classes

### 1.0.0

- Initial release
