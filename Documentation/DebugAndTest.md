# Debug logging

- SDK logging is disabled by default. To toggle logging use `BeaconsmindSdk.minLogLevel`. We support 4 leveles of logging + silent (no logs)
- Your *Logcat* will also contain logs from the underlying [AltBeacon](https://github.com/AltBeacon/android-beacon-library) library we use for beacon ranging prefixed with **BMS.ab** tag. 

## When using Timber for logging

> We use Timber for logging. If you are not using it in your project you can skip this section.

- Make sure to plant your Timber trees before initializing beaconsmind SDK. In that case our SDK won't plant any additional trees. 
- Pay attention to production logging in your application since your Timber trees can still pick up our Timber log messages.

# Beaconsmind SDK integration testing

- You need to have access to your beaconsmind admin suite, to set up the beacons and campaigns you want to test for.
- Android emulator doesn’t have bluetooth capabilities so the tests have to be executed on real android devices. One device runs a beacon simulator, the other device runs your application with beaconsmind SDK integrated.
- You can test SDK touchpoints and beacon monitoring in your debug logs with BMS tag.

## Android devices

### 1. Device no. 1 (acts as a beacon)

- Download Beacon Simulator app from play store
- Setup beacon in the Beacon Simulator app
    - choose iBeacon
    - set any name
    - copy the generated uuid to set it up in the suite

### 2. Device no. 2 (targeted testing device)
- Install your application with integrated beaconsmind SDK
    - using android studio or generated .apk file
- Make sure to have both bluetooth and location settings turned on
    - BLE requires both settings turned on on some devices
    - check for permissions

Test use cases observing logs with tag BMS

## Beaconsmind suite

### 1. Create a beacon 

- From side menu create a store if there is none already
    - store location (currently) doesn’t affect the testing environment
- From side menu create a new beacon
    - fill it with data from the beacon created on the device no. 1
    - copy & paste UUID, major and minor values
    - assign it to any store you have

With this beacon in your suite and the beacon simulator near your test device, you should be able to observe the SDK logs in your testing application with a tag BMS. What you can test here is beacon ranging and monitoring. 

### 2. Create campaigns and messages

Next step is adding campaigns and messages in your admin suite to test specific use cases, offers and messages when interacting with the setup beacon.

#### (2.a) Test messages

In your suites side navigation select **Customer Utilities**. From there you can manually send the message to a targeted user.
