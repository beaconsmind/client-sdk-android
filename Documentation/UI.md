# Beaconsmind UI

## Theming

The SDK is using default values that are exposed through the `BeaconsmindUI` singleton to the client-side. Should you need to override some defaults, simply update the style sheet or the localized strings used by the SDK before starting any UI component. Custom Application class is a good place for that.

- If you are calling it from java code, you can use supplied `StyleSheet.Builder` and `LocalizationStrings.Builder` classes

> Make us of Kotlin named arguments:

```kotlin
BeaconsmindUI.styleSheet = StyleSheet(
    backgroundColor = Color.WHITE,
    redeemButtonTitleColor = Color.BLACK,
    redeemButtonBackgroundColor = Color.BLACK,
    primaryButtonTitleColor = Color.WHITE,
    primaryButtonBackgroundColor = Color.BLACK,
    buttonHeight = 48,
    useToolbar = false,
)
```

```kotlin
BeaconsmindUI.localizationStrings = LocalizationStrings(
    redeemButtonTitle = "Redeem",
    redeemQuestionMessage = "Do you want to redeem this Offer?",
    redeemSuccessMessage = "Offer redeemed!",
    alertOKTitle = "OK",
    alertCancelTitle = "Cancel",
    readingOfferErrorMessage = "Couldn't read offer, please try again",
    redeemingOfferErrorMessage = "Couldn't redeem offer, please try again",
    noOffersMessage = "No offers",
    readMoreMessage = "Read more",
)
```

## Screens

### Offer list

The SDK exposes a fragment to display the list of offers. Default colors and localized strings can be overrided via `BeaconsmindUI.styleSheet` and `BeaconsmindUI.localizedStrings`. Fragment handles fetching, displaying and navigating to `OfferDetailsActivity`.

### Offer details

We also expose a reusable and easily themable fragment to display and redeem offers. The basic usage is `OfferDetailsFragment.newInstance(offerResponse: OfferResponse)` where offer is an `OfferResponse` object or `OfferDetailsFragment.newInstance(offerId: Int)` where offerId is a valid offer id. In the second case the fragment will take care of fetching the offer and displaying potential loading and error states.

You can also use our activity component `OfferDetailsActivity` which holds the fragment. Simply get the intent by calling `OfferDetailsActivity.getIntent(context: Context, offerId: Int)` or `OfferDetailsActivity.getIntent(context: Context, offer: OfferResponse)`

