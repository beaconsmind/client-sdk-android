# Beacons

Beacons feature uses BLE transmitters which can be picked up by Android Devices. In Beaconsmind, BLE transmitters are used to present offers based on a physical location of a device.

Every beacon is defined by it's UUID and major and minor numbers. A list of beacons is persisted and maintained on Beaconsmind environment for each client.

## Requirements

Android devices can detect the proximity of the beacons only if the Bluetooth and Location services are enabled and if the device user has approved the use of Location permission.

> **Important**: On Android 10 and higher, the user also needs to approve Background Location permission to be able to detect or range beacons while running in the background.

## Permissions

Here is a list of the permission the SDK's manifest declares:

* android.permission.BLUETOOTH
* android.permission.BLUETOOTH_ADMIN (checking the state of bluetooth adapter)
* android.permission.BLUETOOTH_SCAN (scanning for BLE devices, android 31+)
* android.permission.INTERNET
* android.permission.ACCESS_FINE_LOCATION
* android.permission.ACCESS_COARSE_LOCATION (android 23)
* android.permission.ACCESS_BACKGROUND_LOCATION (android 29+)
* android.permission.FOREGROUND_SERVICE (for active scanning in background)
* android.permission.RECEIVE_BOOT_COMPLETED

You have to prompt programatically the user for the following permissions:
* Manifest.permission.ACCESS_FINE_LOCATION
* Manifest.permission.ACCESS_BACKGROUND_LOCATION
* Manifest.permission.BLUETOOTH_SCAN

Be sure to prompt the user as soon as you would like to start listening for beacons. The SDK is shipped with `SdkPermissionsHelper` class which can be used to check missing permissions and prompt the user to allow them. It is recommended to initialize it in your Activity's `onCreate()`, but can be initialized from any valid Activity context.
> You can fully customize the dialog texts which explain the permission usage.

<br>

```kotlin
// customize your permission request dialog texts here
val permissionTexts = PermissionTexts(
    /*
    permissionsDeniedTitle = "permissionsDeniedTitle",
    permissionsDeniedAction = "permissionsDeniedAction",
    permissionsDeniedText = "permissionsDeniedText",
    permissionRationaleTitle = "permissionRationaleTitle",
    permissionRationaleAction = "permissionRationaleAction",
    permissionRationaleText = "permissionRationaleText",
    backgroundPermissionTitle = "backgroundPermissionTitle",
    backgroundPermissionAction = "backgroundPermissionAction",
    backgroundPermissionText = "backgroundPermissionText",
    */
)
SdkPermissionsHelper.requestPermissions(activity = this, alwaysAlertOnPermissionsDenied = true, permissionTexts = permissionTexts)
```

## SDK Usage

`BeaconListenerService` class is the main entry point for Beacon tasks. It is a singleton attached to your Application Context. By default beacon scanning starts after successfull authorization  

### Scanning modes

The SDK supports two types of BLE scanning. Notification scanning and passive (no notification) scanning. To set it up use BeaconsmindConfig.Builder on Beaconsmind SDK initialization point. 
```java
BeaconsmindConfig.Builder.usePassiveScanning(true);
```

#### Notification scanning

Notification scanning under the hood uses foreground service with attached permanent notification.

\+ Scanning with this mode allows for faster and more consistent results.

\- Requires persistent notification

#### Passive (no notification) scanning

Starting from Android O uses intent scanning. It allows for fully background periodic scans. 

\+ Less battery consumption while away from store beacons

\- Slower results and less consistent

Requires higher transmission rate from broadcast beacons for better accuracy.

### Start Listening for Beacons

This is called automatically by the SDK upon successful register, login or account import

Registering for beacons manualy is also possible. Once you're logged in and the user has granted permissions you can do it by calling
```
BeaconListenerService.getInstance().startListeningBeacons()
```

Executing this method will start periodically fetching the beacons available for your Beaconsmind environment and register them to the `BeaconManager` for ranging and monitoring purposes. Pushing contacts to the server is done by the [PingAPI](../BeaconsmindAPI/docs/PingApi.md).

The background ranging and pinging of the beacons is executed automatically by the SDK even if the app is in the background.

### Stop Listening

This is called automatically by the SDK upon logout.

To stop listening for the beacons please call 
```
BeaconListenerService.getInstance().stopListeningBeacons()
``` 
Ranging beacons and automatic pings to Beaconsmind will terminate along with the running foreground service notification identified by `Constants.NOTIFICATION_FG_SERVICE` and `Constants.BEACON_CHANNEL_ID`

### Continuous scanning

To keep beacon scanning working even if the device is restarted or app process was killed initialize the SDK in your `Application#onCreate`.

### Remind user to turn on Bluetooth & Location

For scanning to work, Bluetooth & Location needs to be turned on in devices settings. 

- The SDK provides utility method for checking if both settings are active with `BLEUtils.getState()`.

- If you want to provide reactive reminder to the user, the SDK provides BLE state LiveData object which you can observe with `BeaconListenerService.getBLEActivityLiveData`

> Check [DemoApplication.java](../DemoApp/src/main/java/com/beaconsmind/demo/DemoApplication.java) for active notification implementation

### Get Contacted Beacons

BeaconListenerService maintains a list of store bracons and contacted beacons along with their statistics. To get a list call `getBeaconsSummary` method.

### Development Simulator

`BeaconsmindSdk.enableSimulator` static method can be used for simulating beacon contacts on Android Emulators for development purposes. 
