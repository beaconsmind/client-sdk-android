/*
 * Client Beaconsmind API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.beaconsmind.api;

import com.beaconsmind.api.client.ApiCallback;
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.ApiResponse;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.Pair;
import com.beaconsmind.api.client.ProgressRequestBody;
import com.beaconsmind.api.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;


import com.beaconsmind.api.models.SaveTouchpointRequest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TouchpointsApi {
    private ApiClient localVarApiClient;
    private int localHostIndex;
    private String localCustomBaseUrl;

    public TouchpointsApi() {
        this(Configuration.getDefaultApiClient());
    }

    public TouchpointsApi(ApiClient apiClient) {
        this.localVarApiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return localVarApiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.localVarApiClient = apiClient;
    }

    public int getHostIndex() {
        return localHostIndex;
    }

    public void setHostIndex(int hostIndex) {
        this.localHostIndex = hostIndex;
    }

    public String getCustomBaseUrl() {
        return localCustomBaseUrl;
    }

    public void setCustomBaseUrl(String customBaseUrl) {
        this.localCustomBaseUrl = customBaseUrl;
    }

    /**
     * Build call for saveTouchpoint
     * @param saveTouchpointRequest  (optional)
     * @param _callback Callback for upload/download progress
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Touchpoint was saved correctly. </td><td>  -  </td></tr>
        <tr><td> 400 </td><td> Validation did not pass, response will contain informational message. </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call saveTouchpointCall(SaveTouchpointRequest saveTouchpointRequest, final ApiCallback _callback) throws ApiException {
        String basePath = null;

        // Operation Servers
        String[] localBasePaths = new String[] {  };

        // Determine Base Path to Use
        if (localCustomBaseUrl != null){
            basePath = localCustomBaseUrl;
        } else if ( localBasePaths.length > 0 ) {
            basePath = localBasePaths[localHostIndex];
        } else {
            basePath = null;
        }

        Object localVarPostBody = saveTouchpointRequest;

        // create path and map variables
        String localVarPath = "/api/touchpoints";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        Map<String, String> localVarCookieParams = new HashMap<String, String>();
        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = localVarApiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) {
            localVarHeaderParams.put("Accept", localVarAccept);
        }

        final String[] localVarContentTypes = {
            "application/json-patch+json", "application/json", "text/json", "application/_*+json"
        };
        final String localVarContentType = localVarApiClient.selectHeaderContentType(localVarContentTypes);
        if (localVarHeaderParams != null) {
            localVarHeaderParams.put("Content-Type", localVarContentType);
        }

        String[] localVarAuthNames = new String[] { "BmsClientKey" };
        return localVarApiClient.buildCall(basePath, localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarCookieParams, localVarFormParams, localVarAuthNames, _callback);
    }

    @SuppressWarnings("rawtypes")
    private okhttp3.Call saveTouchpointValidateBeforeCall(SaveTouchpointRequest saveTouchpointRequest, final ApiCallback _callback) throws ApiException {
        

        okhttp3.Call localVarCall = saveTouchpointCall(saveTouchpointRequest, _callback);
        return localVarCall;

    }

    /**
     * Save touchpoint of the mobile application.  Touchpoints are application events reported to the suite, like &#39;Application Downloaded&#39;, &#39;Application Opened&#39;, etc.
     * When tracking AppDownload, a unique device id has to be present to allow the suite to ignore future requests.  If the application developer decides to implement single AppDownload reporting on the application side, a random  UUID can be sent instead.  For other touchpoints, user needs to be authenticated and Bearer JWT needs to be passed with the request.
     * @param saveTouchpointRequest  (optional)
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Touchpoint was saved correctly. </td><td>  -  </td></tr>
        <tr><td> 400 </td><td> Validation did not pass, response will contain informational message. </td><td>  -  </td></tr>
     </table>
     */
    public void saveTouchpoint(SaveTouchpointRequest saveTouchpointRequest) throws ApiException {
        saveTouchpointWithHttpInfo(saveTouchpointRequest);
    }

    /**
     * Save touchpoint of the mobile application.  Touchpoints are application events reported to the suite, like &#39;Application Downloaded&#39;, &#39;Application Opened&#39;, etc.
     * When tracking AppDownload, a unique device id has to be present to allow the suite to ignore future requests.  If the application developer decides to implement single AppDownload reporting on the application side, a random  UUID can be sent instead.  For other touchpoints, user needs to be authenticated and Bearer JWT needs to be passed with the request.
     * @param saveTouchpointRequest  (optional)
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Touchpoint was saved correctly. </td><td>  -  </td></tr>
        <tr><td> 400 </td><td> Validation did not pass, response will contain informational message. </td><td>  -  </td></tr>
     </table>
     */
    public ApiResponse<Void> saveTouchpointWithHttpInfo(SaveTouchpointRequest saveTouchpointRequest) throws ApiException {
        okhttp3.Call localVarCall = saveTouchpointValidateBeforeCall(saveTouchpointRequest, null);
        return localVarApiClient.execute(localVarCall);
    }

    /**
     * Save touchpoint of the mobile application.  Touchpoints are application events reported to the suite, like &#39;Application Downloaded&#39;, &#39;Application Opened&#39;, etc. (asynchronously)
     * When tracking AppDownload, a unique device id has to be present to allow the suite to ignore future requests.  If the application developer decides to implement single AppDownload reporting on the application side, a random  UUID can be sent instead.  For other touchpoints, user needs to be authenticated and Bearer JWT needs to be passed with the request.
     * @param saveTouchpointRequest  (optional)
     * @param _callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     * @http.response.details
     <table summary="Response Details" border="1">
        <tr><td> Status Code </td><td> Description </td><td> Response Headers </td></tr>
        <tr><td> 200 </td><td> Touchpoint was saved correctly. </td><td>  -  </td></tr>
        <tr><td> 400 </td><td> Validation did not pass, response will contain informational message. </td><td>  -  </td></tr>
     </table>
     */
    public okhttp3.Call saveTouchpointAsync(SaveTouchpointRequest saveTouchpointRequest, final ApiCallback<Void> _callback) throws ApiException {

        okhttp3.Call localVarCall = saveTouchpointValidateBeforeCall(saveTouchpointRequest, _callback);
        localVarApiClient.executeAsync(localVarCall, _callback);
        return localVarCall;
    }
}
