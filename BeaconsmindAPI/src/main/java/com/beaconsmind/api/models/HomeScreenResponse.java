/*
 * Client Beaconsmind API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.beaconsmind.api.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * HomeScreenResponse
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class HomeScreenResponse {
  public static final String SERIALIZED_NAME_NEWS_ID = "newsId";
  @SerializedName(SERIALIZED_NAME_NEWS_ID)
  private Integer newsId;

  public static final String SERIALIZED_NAME_NEWS_TITLE = "newsTitle";
  @SerializedName(SERIALIZED_NAME_NEWS_TITLE)
  private String newsTitle;

  public static final String SERIALIZED_NAME_IMAGE_URL = "imageUrl";
  @SerializedName(SERIALIZED_NAME_IMAGE_URL)
  private String imageUrl;

  public static final String SERIALIZED_NAME_VIDEO_URL = "videoUrl";
  @SerializedName(SERIALIZED_NAME_VIDEO_URL)
  private String videoUrl;

  public HomeScreenResponse() { 
  }

  public HomeScreenResponse newsId(Integer newsId) {
    
    this.newsId = newsId;
    return this;
  }

   /**
   * Get newsId
   * @return newsId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Integer getNewsId() {
    return newsId;
  }


  public void setNewsId(Integer newsId) {
    this.newsId = newsId;
  }


  public HomeScreenResponse newsTitle(String newsTitle) {
    
    this.newsTitle = newsTitle;
    return this;
  }

   /**
   * Get newsTitle
   * @return newsTitle
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getNewsTitle() {
    return newsTitle;
  }


  public void setNewsTitle(String newsTitle) {
    this.newsTitle = newsTitle;
  }


  public HomeScreenResponse imageUrl(String imageUrl) {
    
    this.imageUrl = imageUrl;
    return this;
  }

   /**
   * Get imageUrl
   * @return imageUrl
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getImageUrl() {
    return imageUrl;
  }


  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }


  public HomeScreenResponse videoUrl(String videoUrl) {
    
    this.videoUrl = videoUrl;
    return this;
  }

   /**
   * Get videoUrl
   * @return videoUrl
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getVideoUrl() {
    return videoUrl;
  }


  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HomeScreenResponse homeScreenResponse = (HomeScreenResponse) o;
    return Objects.equals(this.newsId, homeScreenResponse.newsId) &&
        Objects.equals(this.newsTitle, homeScreenResponse.newsTitle) &&
        Objects.equals(this.imageUrl, homeScreenResponse.imageUrl) &&
        Objects.equals(this.videoUrl, homeScreenResponse.videoUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(newsId, newsTitle, imageUrl, videoUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HomeScreenResponse {\n");
    sb.append("    newsId: ").append(toIndentedString(newsId)).append("\n");
    sb.append("    newsTitle: ").append(toIndentedString(newsTitle)).append("\n");
    sb.append("    imageUrl: ").append(toIndentedString(imageUrl)).append("\n");
    sb.append("    videoUrl: ").append(toIndentedString(videoUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

