/*
 * Client Beaconsmind API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.beaconsmind.api.models;

import java.util.Objects;
import java.util.Arrays;
import io.swagger.annotations.ApiModel;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * Values represent:     1 (Verbose)     2 (Debug)     3 (Info)     4 (Warning)     5 (Error)     999 (Silent)
 */
@JsonAdapter(LoggingLevel.Adapter.class)
public enum LoggingLevel {
  
  /**
   * Verbose
   */
  Verbose(1),
  
  /**
   * Debug
   */
  Debug(2),
  
  /**
   * Info
   */
  Info(3),
  
  /**
   * Warning
   */
  Warning(4),
  
  /**
   * Error
   */
  Error(5),
  
  /**
   * Silent
   */
  Silent(999);

  private Integer value;

  LoggingLevel(Integer value) {
    this.value = value;
  }

  public Integer getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static LoggingLevel fromValue(Integer value) {
    for (LoggingLevel b : LoggingLevel.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }

  public static class Adapter extends TypeAdapter<LoggingLevel> {
    @Override
    public void write(final JsonWriter jsonWriter, final LoggingLevel enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public LoggingLevel read(final JsonReader jsonReader) throws IOException {
      Integer value = jsonReader.nextInt();
      return LoggingLevel.fromValue(value);
    }
  }
}

