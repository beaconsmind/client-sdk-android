/*
 * Client Beaconsmind API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.beaconsmind.api.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * VatItemView
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class VatItemView {
  public static final String SERIALIZED_NAME_CODE = "code";
  @SerializedName(SERIALIZED_NAME_CODE)
  private String code;

  public static final String SERIALIZED_NAME_VAT_RATE = "vatRate";
  @SerializedName(SERIALIZED_NAME_VAT_RATE)
  private Double vatRate;

  public static final String SERIALIZED_NAME_APPLICABLE_AMOUNT = "applicableAmount";
  @SerializedName(SERIALIZED_NAME_APPLICABLE_AMOUNT)
  private Double applicableAmount;

  public static final String SERIALIZED_NAME_VAT_AMOUNT = "vatAmount";
  @SerializedName(SERIALIZED_NAME_VAT_AMOUNT)
  private Double vatAmount;

  public VatItemView() { 
  }

  public VatItemView code(String code) {
    
    this.code = code;
    return this;
  }

   /**
   * Get code
   * @return code
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getCode() {
    return code;
  }


  public void setCode(String code) {
    this.code = code;
  }


  public VatItemView vatRate(Double vatRate) {
    
    this.vatRate = vatRate;
    return this;
  }

   /**
   * Get vatRate
   * @return vatRate
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public Double getVatRate() {
    return vatRate;
  }


  public void setVatRate(Double vatRate) {
    this.vatRate = vatRate;
  }


  public VatItemView applicableAmount(Double applicableAmount) {
    
    this.applicableAmount = applicableAmount;
    return this;
  }

   /**
   * Get applicableAmount
   * @return applicableAmount
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public Double getApplicableAmount() {
    return applicableAmount;
  }


  public void setApplicableAmount(Double applicableAmount) {
    this.applicableAmount = applicableAmount;
  }


  public VatItemView vatAmount(Double vatAmount) {
    
    this.vatAmount = vatAmount;
    return this;
  }

   /**
   * Get vatAmount
   * @return vatAmount
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public Double getVatAmount() {
    return vatAmount;
  }


  public void setVatAmount(Double vatAmount) {
    this.vatAmount = vatAmount;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VatItemView vatItemView = (VatItemView) o;
    return Objects.equals(this.code, vatItemView.code) &&
        Objects.equals(this.vatRate, vatItemView.vatRate) &&
        Objects.equals(this.applicableAmount, vatItemView.applicableAmount) &&
        Objects.equals(this.vatAmount, vatItemView.vatAmount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, vatRate, applicableAmount, vatAmount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VatItemView {\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    vatRate: ").append(toIndentedString(vatRate)).append("\n");
    sb.append("    applicableAmount: ").append(toIndentedString(applicableAmount)).append("\n");
    sb.append("    vatAmount: ").append(toIndentedString(vatAmount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

