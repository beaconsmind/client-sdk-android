/*
 * Client Beaconsmind API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.beaconsmind.api.models;

import java.util.Objects;
import java.util.Arrays;
import com.beaconsmind.api.models.UserMonthPurchasesView;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * UserMonthPurchasesViewDataPage
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class UserMonthPurchasesViewDataPage {
  public static final String SERIALIZED_NAME_NEXT_PAGE_LINK = "nextPageLink";
  @SerializedName(SERIALIZED_NAME_NEXT_PAGE_LINK)
  private URI nextPageLink;

  public static final String SERIALIZED_NAME_COUNT = "count";
  @SerializedName(SERIALIZED_NAME_COUNT)
  private Long count;

  public static final String SERIALIZED_NAME_ITEMS = "items";
  @SerializedName(SERIALIZED_NAME_ITEMS)
  private List<UserMonthPurchasesView> items = null;

  public UserMonthPurchasesViewDataPage() { 
  }

  
  public UserMonthPurchasesViewDataPage(
     URI nextPageLink, 
     Long count
  ) {
    this();
    this.nextPageLink = nextPageLink;
    this.count = count;
  }

   /**
   * Get nextPageLink
   * @return nextPageLink
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public URI getNextPageLink() {
    return nextPageLink;
  }




   /**
   * Get count
   * @return count
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public Long getCount() {
    return count;
  }




  public UserMonthPurchasesViewDataPage items(List<UserMonthPurchasesView> items) {
    
    this.items = items;
    return this;
  }

  public UserMonthPurchasesViewDataPage addItemsItem(UserMonthPurchasesView itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<UserMonthPurchasesView>();
    }
    this.items.add(itemsItem);
    return this;
  }

   /**
   * Get items
   * @return items
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public List<UserMonthPurchasesView> getItems() {
    return items;
  }


  public void setItems(List<UserMonthPurchasesView> items) {
    this.items = items;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserMonthPurchasesViewDataPage userMonthPurchasesViewDataPage = (UserMonthPurchasesViewDataPage) o;
    return Objects.equals(this.nextPageLink, userMonthPurchasesViewDataPage.nextPageLink) &&
        Objects.equals(this.count, userMonthPurchasesViewDataPage.count) &&
        Objects.equals(this.items, userMonthPurchasesViewDataPage.items);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nextPageLink, count, items);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserMonthPurchasesViewDataPage {\n");
    sb.append("    nextPageLink: ").append(toIndentedString(nextPageLink)).append("\n");
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

