/*
 * Client Beaconsmind API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.beaconsmind.api.models;

import java.util.Objects;
import java.util.Arrays;
import com.beaconsmind.api.models.DeviceRegistrationModel;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * RegisterViaFacebookRequest
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class RegisterViaFacebookRequest {
  public static final String SERIALIZED_NAME_USER_NAME = "userName";
  @SerializedName(SERIALIZED_NAME_USER_NAME)
  private String userName;

  public static final String SERIALIZED_NAME_FIRST_NAME = "firstName";
  @SerializedName(SERIALIZED_NAME_FIRST_NAME)
  private String firstName;

  public static final String SERIALIZED_NAME_LAST_NAME = "lastName";
  @SerializedName(SERIALIZED_NAME_LAST_NAME)
  private String lastName;

  public static final String SERIALIZED_NAME_LANGUAGE = "language";
  @SerializedName(SERIALIZED_NAME_LANGUAGE)
  private String language;

  public static final String SERIALIZED_NAME_EXTERNAL_ACCESS_TOKEN = "externalAccessToken";
  @SerializedName(SERIALIZED_NAME_EXTERNAL_ACCESS_TOKEN)
  private String externalAccessToken;

  public static final String SERIALIZED_NAME_FB_USER_ID = "fbUserId";
  @SerializedName(SERIALIZED_NAME_FB_USER_ID)
  private String fbUserId;

  public static final String SERIALIZED_NAME_PROVIDER = "provider";
  @SerializedName(SERIALIZED_NAME_PROVIDER)
  private String provider;

  public static final String SERIALIZED_NAME_DEVICE_REGISTRATION = "deviceRegistration";
  @SerializedName(SERIALIZED_NAME_DEVICE_REGISTRATION)
  private DeviceRegistrationModel deviceRegistration;

  public RegisterViaFacebookRequest() { 
  }

  public RegisterViaFacebookRequest userName(String userName) {
    
    this.userName = userName;
    return this;
  }

   /**
   * Get userName
   * @return userName
   * @deprecated
  **/
  @Deprecated
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getUserName() {
    return userName;
  }


  public void setUserName(String userName) {
    this.userName = userName;
  }


  public RegisterViaFacebookRequest firstName(String firstName) {
    
    this.firstName = firstName;
    return this;
  }

   /**
   * Get firstName
   * @return firstName
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public String getFirstName() {
    return firstName;
  }


  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }


  public RegisterViaFacebookRequest lastName(String lastName) {
    
    this.lastName = lastName;
    return this;
  }

   /**
   * Get lastName
   * @return lastName
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public String getLastName() {
    return lastName;
  }


  public void setLastName(String lastName) {
    this.lastName = lastName;
  }


  public RegisterViaFacebookRequest language(String language) {
    
    this.language = language;
    return this;
  }

   /**
   * Get language
   * @return language
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public String getLanguage() {
    return language;
  }


  public void setLanguage(String language) {
    this.language = language;
  }


  public RegisterViaFacebookRequest externalAccessToken(String externalAccessToken) {
    
    this.externalAccessToken = externalAccessToken;
    return this;
  }

   /**
   * Get externalAccessToken
   * @return externalAccessToken
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public String getExternalAccessToken() {
    return externalAccessToken;
  }


  public void setExternalAccessToken(String externalAccessToken) {
    this.externalAccessToken = externalAccessToken;
  }


  public RegisterViaFacebookRequest fbUserId(String fbUserId) {
    
    this.fbUserId = fbUserId;
    return this;
  }

   /**
   * Get fbUserId
   * @return fbUserId
   * @deprecated
  **/
  @Deprecated
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getFbUserId() {
    return fbUserId;
  }


  public void setFbUserId(String fbUserId) {
    this.fbUserId = fbUserId;
  }


  public RegisterViaFacebookRequest provider(String provider) {
    
    this.provider = provider;
    return this;
  }

   /**
   * Get provider
   * @return provider
   * @deprecated
  **/
  @Deprecated
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getProvider() {
    return provider;
  }


  public void setProvider(String provider) {
    this.provider = provider;
  }


  public RegisterViaFacebookRequest deviceRegistration(DeviceRegistrationModel deviceRegistration) {
    
    this.deviceRegistration = deviceRegistration;
    return this;
  }

   /**
   * Get deviceRegistration
   * @return deviceRegistration
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public DeviceRegistrationModel getDeviceRegistration() {
    return deviceRegistration;
  }


  public void setDeviceRegistration(DeviceRegistrationModel deviceRegistration) {
    this.deviceRegistration = deviceRegistration;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegisterViaFacebookRequest registerViaFacebookRequest = (RegisterViaFacebookRequest) o;
    return Objects.equals(this.userName, registerViaFacebookRequest.userName) &&
        Objects.equals(this.firstName, registerViaFacebookRequest.firstName) &&
        Objects.equals(this.lastName, registerViaFacebookRequest.lastName) &&
        Objects.equals(this.language, registerViaFacebookRequest.language) &&
        Objects.equals(this.externalAccessToken, registerViaFacebookRequest.externalAccessToken) &&
        Objects.equals(this.fbUserId, registerViaFacebookRequest.fbUserId) &&
        Objects.equals(this.provider, registerViaFacebookRequest.provider) &&
        Objects.equals(this.deviceRegistration, registerViaFacebookRequest.deviceRegistration);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userName, firstName, lastName, language, externalAccessToken, fbUserId, provider, deviceRegistration);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RegisterViaFacebookRequest {\n");
    sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    language: ").append(toIndentedString(language)).append("\n");
    sb.append("    externalAccessToken: ").append(toIndentedString(externalAccessToken)).append("\n");
    sb.append("    fbUserId: ").append(toIndentedString(fbUserId)).append("\n");
    sb.append("    provider: ").append(toIndentedString(provider)).append("\n");
    sb.append("    deviceRegistration: ").append(toIndentedString(deviceRegistration)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

