/*
 * Client Beaconsmind API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.beaconsmind.api.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * DeviceRegistrationModel
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class DeviceRegistrationModel {
  public static final String SERIALIZED_NAME_NATIVE_ID = "nativeId";
  @SerializedName(SERIALIZED_NAME_NATIVE_ID)
  private String nativeId;

  public static final String SERIALIZED_NAME_PLATFORM = "platform";
  @SerializedName(SERIALIZED_NAME_PLATFORM)
  private String platform;

  public DeviceRegistrationModel() { 
  }

  public DeviceRegistrationModel nativeId(String nativeId) {
    
    this.nativeId = nativeId;
    return this;
  }

   /**
   * Get nativeId
   * @return nativeId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getNativeId() {
    return nativeId;
  }


  public void setNativeId(String nativeId) {
    this.nativeId = nativeId;
  }


  public DeviceRegistrationModel platform(String platform) {
    
    this.platform = platform;
    return this;
  }

   /**
   * Get platform
   * @return platform
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPlatform() {
    return platform;
  }


  public void setPlatform(String platform) {
    this.platform = platform;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeviceRegistrationModel deviceRegistrationModel = (DeviceRegistrationModel) o;
    return Objects.equals(this.nativeId, deviceRegistrationModel.nativeId) &&
        Objects.equals(this.platform, deviceRegistrationModel.platform);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nativeId, platform);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeviceRegistrationModel {\n");
    sb.append("    nativeId: ").append(toIndentedString(nativeId)).append("\n");
    sb.append("    platform: ").append(toIndentedString(platform)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

