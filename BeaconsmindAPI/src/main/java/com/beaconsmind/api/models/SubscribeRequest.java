/*
 * Client Beaconsmind API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.beaconsmind.api.models;

import java.util.Objects;
import java.util.Arrays;
import com.beaconsmind.api.models.PhoneOS;
import com.beaconsmind.api.models.PlatformType;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * SubscribeRequest
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class SubscribeRequest {
  public static final String SERIALIZED_NAME_PLATFORM_ID = "platformId";
  @SerializedName(SERIALIZED_NAME_PLATFORM_ID)
  private String platformId;

  public static final String SERIALIZED_NAME_PLATFORM_TYPE = "platformType";
  @SerializedName(SERIALIZED_NAME_PLATFORM_TYPE)
  private PlatformType platformType;

  public static final String SERIALIZED_NAME_PHONE_O_S = "phoneOS";
  @SerializedName(SERIALIZED_NAME_PHONE_O_S)
  private PhoneOS phoneOS;

  public static final String SERIALIZED_NAME_PHONE_MANUFACTURER = "phoneManufacturer";
  @SerializedName(SERIALIZED_NAME_PHONE_MANUFACTURER)
  private String phoneManufacturer;

  public static final String SERIALIZED_NAME_PHONE_MODEL = "phoneModel";
  @SerializedName(SERIALIZED_NAME_PHONE_MODEL)
  private String phoneModel;

  public static final String SERIALIZED_NAME_PHONE_OS_VERSION = "phoneOsVersion";
  @SerializedName(SERIALIZED_NAME_PHONE_OS_VERSION)
  private String phoneOsVersion;

  public SubscribeRequest() { 
  }

  public SubscribeRequest platformId(String platformId) {
    
    this.platformId = platformId;
    return this;
  }

   /**
   * Device id for the notification platform (FCM token, APNS token)
   * @return platformId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Device id for the notification platform (FCM token, APNS token)")

  public String getPlatformId() {
    return platformId;
  }


  public void setPlatformId(String platformId) {
    this.platformId = platformId;
  }


  public SubscribeRequest platformType(PlatformType platformType) {
    
    this.platformType = platformType;
    return this;
  }

   /**
   * Get platformType
   * @return platformType
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public PlatformType getPlatformType() {
    return platformType;
  }


  public void setPlatformType(PlatformType platformType) {
    this.platformType = platformType;
  }


  public SubscribeRequest phoneOS(PhoneOS phoneOS) {
    
    this.phoneOS = phoneOS;
    return this;
  }

   /**
   * Get phoneOS
   * @return phoneOS
  **/
  @javax.annotation.Nonnull
  @ApiModelProperty(required = true, value = "")

  public PhoneOS getPhoneOS() {
    return phoneOS;
  }


  public void setPhoneOS(PhoneOS phoneOS) {
    this.phoneOS = phoneOS;
  }


  public SubscribeRequest phoneManufacturer(String phoneManufacturer) {
    
    this.phoneManufacturer = phoneManufacturer;
    return this;
  }

   /**
   * Get phoneManufacturer
   * @return phoneManufacturer
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPhoneManufacturer() {
    return phoneManufacturer;
  }


  public void setPhoneManufacturer(String phoneManufacturer) {
    this.phoneManufacturer = phoneManufacturer;
  }


  public SubscribeRequest phoneModel(String phoneModel) {
    
    this.phoneModel = phoneModel;
    return this;
  }

   /**
   * Get phoneModel
   * @return phoneModel
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPhoneModel() {
    return phoneModel;
  }


  public void setPhoneModel(String phoneModel) {
    this.phoneModel = phoneModel;
  }


  public SubscribeRequest phoneOsVersion(String phoneOsVersion) {
    
    this.phoneOsVersion = phoneOsVersion;
    return this;
  }

   /**
   * Get phoneOsVersion
   * @return phoneOsVersion
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPhoneOsVersion() {
    return phoneOsVersion;
  }


  public void setPhoneOsVersion(String phoneOsVersion) {
    this.phoneOsVersion = phoneOsVersion;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SubscribeRequest subscribeRequest = (SubscribeRequest) o;
    return Objects.equals(this.platformId, subscribeRequest.platformId) &&
        Objects.equals(this.platformType, subscribeRequest.platformType) &&
        Objects.equals(this.phoneOS, subscribeRequest.phoneOS) &&
        Objects.equals(this.phoneManufacturer, subscribeRequest.phoneManufacturer) &&
        Objects.equals(this.phoneModel, subscribeRequest.phoneModel) &&
        Objects.equals(this.phoneOsVersion, subscribeRequest.phoneOsVersion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(platformId, platformType, phoneOS, phoneManufacturer, phoneModel, phoneOsVersion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SubscribeRequest {\n");
    sb.append("    platformId: ").append(toIndentedString(platformId)).append("\n");
    sb.append("    platformType: ").append(toIndentedString(platformType)).append("\n");
    sb.append("    phoneOS: ").append(toIndentedString(phoneOS)).append("\n");
    sb.append("    phoneManufacturer: ").append(toIndentedString(phoneManufacturer)).append("\n");
    sb.append("    phoneModel: ").append(toIndentedString(phoneModel)).append("\n");
    sb.append("    phoneOsVersion: ").append(toIndentedString(phoneOsVersion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

