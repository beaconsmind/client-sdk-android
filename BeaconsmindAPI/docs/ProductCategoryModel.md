

# ProductCategoryModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [readonly]
**url** | **String** |  |  [optional] [readonly]
**imageUrl** | **String** |  |  [optional] [readonly]
**name** | **String** |  |  [optional] [readonly]
**deleted** | **Boolean** |  |  [readonly]



