# ProductsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCategories**](ProductsApi.md#getCategories) | **GET** /api/v1/products/categories | 
[**getProducts**](ProductsApi.md#getProducts) | **GET** /api/v1/products | 
[**getProductsByCategory**](ProductsApi.md#getProductsByCategory) | **GET** /api/v1/products/by-category/{categoryId} | 


<a name="getCategories"></a>
# **getCategories**
> List&lt;ProductCategoryModel&gt; getCategories(search, skip, limit)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ProductsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ProductsApi apiInstance = new ProductsApi(defaultClient);
    String search = "search_example"; // String | 
    Integer skip = 56; // Integer | 
    Integer limit = 56; // Integer | 
    try {
      List<ProductCategoryModel> result = apiInstance.getCategories(search, skip, limit);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProductsApi#getCategories");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **String**|  | [optional]
 **skip** | **Integer**|  | [optional]
 **limit** | **Integer**|  | [optional]

### Return type

[**List&lt;ProductCategoryModel&gt;**](ProductCategoryModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="getProducts"></a>
# **getProducts**
> List&lt;ProductModel&gt; getProducts(search, skip, limit)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ProductsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ProductsApi apiInstance = new ProductsApi(defaultClient);
    String search = "search_example"; // String | 
    Integer skip = 56; // Integer | 
    Integer limit = 56; // Integer | 
    try {
      List<ProductModel> result = apiInstance.getProducts(search, skip, limit);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProductsApi#getProducts");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **String**|  | [optional]
 **skip** | **Integer**|  | [optional]
 **limit** | **Integer**|  | [optional]

### Return type

[**List&lt;ProductModel&gt;**](ProductModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="getProductsByCategory"></a>
# **getProductsByCategory**
> List&lt;ProductModel&gt; getProductsByCategory(categoryId, search, skip, limit)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ProductsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ProductsApi apiInstance = new ProductsApi(defaultClient);
    Integer categoryId = 56; // Integer | 
    String search = "search_example"; // String | 
    Integer skip = 56; // Integer | 
    Integer limit = 56; // Integer | 
    try {
      List<ProductModel> result = apiInstance.getProductsByCategory(categoryId, search, skip, limit);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProductsApi#getProductsByCategory");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryId** | **Integer**|  |
 **search** | **String**|  | [optional]
 **skip** | **Integer**|  | [optional]
 **limit** | **Integer**|  | [optional]

### Return type

[**List&lt;ProductModel&gt;**](ProductModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

