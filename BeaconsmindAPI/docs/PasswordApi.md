# PasswordApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changePassword**](PasswordApi.md#changePassword) | **PUT** /api/v1/accounts/password | 
[**deprecatedChangePassword**](PasswordApi.md#deprecatedChangePassword) | **POST** /api/accounts/changePassword | 
[**deprecatedResetPassword**](PasswordApi.md#deprecatedResetPassword) | **POST** /api/accounts/resetPassword | 
[**deprecatedResetPasswordRequest**](PasswordApi.md#deprecatedResetPasswordRequest) | **GET** /api/accounts/resetPasswordRequest | 
[**forgotPassword**](PasswordApi.md#forgotPassword) | **POST** /api/v1/accounts/forgot-password | 
[**resetPassword**](PasswordApi.md#resetPassword) | **POST** /api/v1/accounts/password | 
[**verifyToken**](PasswordApi.md#verifyToken) | **POST** /api/v1/accounts/token/verify | 


<a name="changePassword"></a>
# **changePassword**
> PasswordChangedResponse changePassword(changePasswordRequest)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PasswordApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    PasswordApi apiInstance = new PasswordApi(defaultClient);
    ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest(); // ChangePasswordRequest | 
    try {
      PasswordChangedResponse result = apiInstance.changePassword(changePasswordRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PasswordApi#changePassword");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changePasswordRequest** | [**ChangePasswordRequest**](ChangePasswordRequest.md)|  | [optional]

### Return type

[**PasswordChangedResponse**](PasswordChangedResponse.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="deprecatedChangePassword"></a>
# **deprecatedChangePassword**
> deprecatedChangePassword(depreactedChangePasswordRequest)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PasswordApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    PasswordApi apiInstance = new PasswordApi(defaultClient);
    DepreactedChangePasswordRequest depreactedChangePasswordRequest = new DepreactedChangePasswordRequest(); // DepreactedChangePasswordRequest | 
    try {
      apiInstance.deprecatedChangePassword(depreactedChangePasswordRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling PasswordApi#deprecatedChangePassword");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **depreactedChangePasswordRequest** | [**DepreactedChangePasswordRequest**](DepreactedChangePasswordRequest.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="deprecatedResetPassword"></a>
# **deprecatedResetPassword**
> deprecatedResetPassword(deprecatedResetPasswordRequest)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PasswordApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PasswordApi apiInstance = new PasswordApi(defaultClient);
    DeprecatedResetPasswordRequest deprecatedResetPasswordRequest = new DeprecatedResetPasswordRequest(); // DeprecatedResetPasswordRequest | 
    try {
      apiInstance.deprecatedResetPassword(deprecatedResetPasswordRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling PasswordApi#deprecatedResetPassword");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deprecatedResetPasswordRequest** | [**DeprecatedResetPasswordRequest**](DeprecatedResetPasswordRequest.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="deprecatedResetPasswordRequest"></a>
# **deprecatedResetPasswordRequest**
> PasswordChangedResponse deprecatedResetPasswordRequest(email, skipSendingEmail)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PasswordApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PasswordApi apiInstance = new PasswordApi(defaultClient);
    String email = "email_example"; // String | 
    Boolean skipSendingEmail = false; // Boolean | Default: False
    try {
      PasswordChangedResponse result = apiInstance.deprecatedResetPasswordRequest(email, skipSendingEmail);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PasswordApi#deprecatedResetPasswordRequest");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**|  | [optional]
 **skipSendingEmail** | **Boolean**| Default: False | [optional] [default to false]

### Return type

[**PasswordChangedResponse**](PasswordChangedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="forgotPassword"></a>
# **forgotPassword**
> EmailSentResponse forgotPassword(forgotPasswordRequest)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PasswordApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PasswordApi apiInstance = new PasswordApi(defaultClient);
    ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest(); // ForgotPasswordRequest | 
    try {
      EmailSentResponse result = apiInstance.forgotPassword(forgotPasswordRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PasswordApi#forgotPassword");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **forgotPasswordRequest** | [**ForgotPasswordRequest**](ForgotPasswordRequest.md)|  | [optional]

### Return type

[**EmailSentResponse**](EmailSentResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="resetPassword"></a>
# **resetPassword**
> PasswordChangedResponse resetPassword(resetPasswordRequest)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PasswordApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PasswordApi apiInstance = new PasswordApi(defaultClient);
    ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(); // ResetPasswordRequest | 
    try {
      PasswordChangedResponse result = apiInstance.resetPassword(resetPasswordRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PasswordApi#resetPassword");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resetPasswordRequest** | [**ResetPasswordRequest**](ResetPasswordRequest.md)|  | [optional]

### Return type

[**PasswordChangedResponse**](PasswordChangedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="verifyToken"></a>
# **verifyToken**
> Boolean verifyToken(verifyTokenRequest)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PasswordApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PasswordApi apiInstance = new PasswordApi(defaultClient);
    VerifyTokenRequest verifyTokenRequest = new VerifyTokenRequest(); // VerifyTokenRequest | 
    try {
      Boolean result = apiInstance.verifyToken(verifyTokenRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PasswordApi#verifyToken");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verifyTokenRequest** | [**VerifyTokenRequest**](VerifyTokenRequest.md)|  | [optional]

### Return type

**Boolean**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

