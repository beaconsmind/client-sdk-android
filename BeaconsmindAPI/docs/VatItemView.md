

# VatItemView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  |  [optional]
**vatRate** | **Double** |  | 
**applicableAmount** | **Double** |  | 
**vatAmount** | **Double** |  | 



