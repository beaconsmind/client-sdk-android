

# BlogPostView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**imageUrl** | **String** |  |  [optional]
**authorImageUrl** | **String** |  |  [optional]
**authorName** | **String** |  |  [optional]
**authorDescription** | **String** |  |  [optional]
**date** | **Date** |  | 
**title** | **String** |  |  [optional]
**leadText** | **String** |  |  [optional]
**articleText** | **String** |  |  [optional]
**deleted** | **Boolean** |  | 



