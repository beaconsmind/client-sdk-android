

# PasswordChangedResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isAdmin** | **Boolean** |  | 
**isSuperAdmin** | **Boolean** |  | 
**isCustomer** | **Boolean** |  | 



