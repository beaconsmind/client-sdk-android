

# HomeScreenResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**newsId** | **Integer** |  |  [optional]
**newsTitle** | **String** |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**videoUrl** | **String** |  |  [optional]



