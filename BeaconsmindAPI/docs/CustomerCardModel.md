

# CustomerCardModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**barcodeType** | **BarcodeType** |  | 
**barcodeData** | **String** |  | 
**loyaltyPoints** | [**LoyaltyPoints**](LoyaltyPoints.md) |  |  [optional]



