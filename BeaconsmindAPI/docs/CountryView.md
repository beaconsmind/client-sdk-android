

# CountryView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**name** | **String** |  |  [optional]
**iso2** | **String** |  |  [optional]
**iso3** | **String** |  |  [optional]
**phoneCode** | [**PhoneCode**](PhoneCode.md) |  |  [optional]



