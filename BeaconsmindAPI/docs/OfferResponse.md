

# OfferResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offerId** | **Integer** | Id of the message (deprecated). | 
**messageId** | **Integer** | Id of the message. | 
**title** | **String** | Title (plain text) of the message. |  [optional]
**text** | **String** | Content of the message (HTML text). |  [optional]
**imageUrl** | **String** | Url of the large image. If message has an image, large version should be shown when viewing message details. |  [optional]
**thumbnailUrl** | **String** | Url of the small image. If message has an image, small version should be shown (if possible) when popup appears in status bar,  or when viewing list of messages. |  [optional]
**validity** | **String** | Plain text explaining validity of the message i.e. &#39;Valid for 2 days&#39;, &#39;Expired&#39; ... |  [optional]
**isRedeemed** | **Boolean** | True if message is redeemable and customer clicked on Redeem button. | 
**isRedeemable** | **Boolean** | If message is redeemable, this will be true. When it is, a Redeem button should be displayed that allows user to mark the offer as Redeemed.  In case it&#39;s possible to automatically detect redemption of the offer on the customer app, redeemed notification should be automated.  If both Redeemed button and CallToAction button are visible, Redeemed should be styled as a secondary button. | 
**delivered** | **Date** | Represents the delivery time of the offer message. |  [optional]
**isVoucher** | **Boolean** | Serves same purpose as &#39;IsRedeemable&#39;. | 
**tileAmount** | **Integer** | Not used. | 
**callToAction** | [**ButtonModel**](ButtonModel.md) |  |  [optional]



