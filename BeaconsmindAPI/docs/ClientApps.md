

# ClientApps


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playStoreUri** | **String** |  |  [optional]
**appStoreUri** | **String** |  |  [optional]
**privacyPolicy** | [**PrivacyPolicySettings**](PrivacyPolicySettings.md) |  |  [optional]



