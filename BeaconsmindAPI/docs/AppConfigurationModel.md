

# AppConfigurationModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configurationName** | **String** |  |  [optional]
**configurationValue** | **String** |  |  [optional]



