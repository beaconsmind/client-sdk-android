

# ImageUploadSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**generateGrayscale** | **Boolean** |  | 
**generateThumbnail** | **Boolean** |  | 
**generateThumbnailGrayscale** | **Boolean** |  | 
**skipResizing** | **Boolean** |  |  [optional]



