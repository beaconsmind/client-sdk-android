

# SaveTouchpointRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uniqueDeviceId** | **String** |  |  [optional]
**type** | **TouchpointType** |  | 
**appVersion** | **String** |  |  [optional]
**localDateTime** | **Date** |  |  [optional]



