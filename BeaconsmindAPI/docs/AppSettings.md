

# AppSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**getInTouchUrl** | **String** |  |  [optional]
**termsAndConditionsUrl** | **String** |  |  [optional]
**stores** | [**List&lt;StoreInfo&gt;**](StoreInfo.md) |  |  [optional]
**beacons** | [**List&lt;StoreBeaconInfo&gt;**](StoreBeaconInfo.md) |  |  [optional]



