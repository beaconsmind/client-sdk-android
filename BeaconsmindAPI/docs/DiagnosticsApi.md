# DiagnosticsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saveDiagnostics**](DiagnosticsApi.md#saveDiagnostics) | **POST** /api/v1/diagnostics | Save diagnostics related to the mobile application health and errors.


<a name="saveDiagnostics"></a>
# **saveDiagnostics**
> AppDiagnosticsModel saveDiagnostics(appDiagnosticsRequest)

Save diagnostics related to the mobile application health and errors.

Metrics can have a custom Key or a known Key:  osInformation, sdkVersion, permissionsGranted, batteryLevel, notificationSetting, exceptionMessage

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.DiagnosticsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    DiagnosticsApi apiInstance = new DiagnosticsApi(defaultClient);
    AppDiagnosticsRequest appDiagnosticsRequest = new AppDiagnosticsRequest(); // AppDiagnosticsRequest | 
    try {
      AppDiagnosticsModel result = apiInstance.saveDiagnostics(appDiagnosticsRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DiagnosticsApi#saveDiagnostics");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appDiagnosticsRequest** | [**AppDiagnosticsRequest**](AppDiagnosticsRequest.md)|  | [optional]

### Return type

[**AppDiagnosticsModel**](AppDiagnosticsModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Diagnostics were saved correctly. |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |

