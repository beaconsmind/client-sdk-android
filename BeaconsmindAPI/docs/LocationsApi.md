# LocationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCitiesForCountry**](LocationsApi.md#getCitiesForCountry) | **GET** /api/v1/locations/countries/{countryId}/cities | 
[**getCountries**](LocationsApi.md#getCountries) | **GET** /api/v1/locations/countries | 


<a name="getCitiesForCountry"></a>
# **getCitiesForCountry**
> List&lt;CityView&gt; getCitiesForCountry(countryId, search, limit)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.LocationsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    LocationsApi apiInstance = new LocationsApi(defaultClient);
    Integer countryId = 56; // Integer | 
    String search = "search_example"; // String | Default: null
    Integer limit = 56; // Integer | Default: null
    try {
      List<CityView> result = apiInstance.getCitiesForCountry(countryId, search, limit);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationsApi#getCitiesForCountry");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countryId** | **Integer**|  |
 **search** | **String**| Default: null | [optional]
 **limit** | **Integer**| Default: null | [optional]

### Return type

[**List&lt;CityView&gt;**](CityView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="getCountries"></a>
# **getCountries**
> List&lt;CountryView&gt; getCountries(search, limit)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.LocationsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    LocationsApi apiInstance = new LocationsApi(defaultClient);
    String search = "search_example"; // String | Default: null
    Integer limit = 56; // Integer | Default: null
    try {
      List<CountryView> result = apiInstance.getCountries(search, limit);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationsApi#getCountries");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **String**| Default: null | [optional]
 **limit** | **Integer**| Default: null | [optional]

### Return type

[**List&lt;CountryView&gt;**](CountryView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

