# AppSigningApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAndroidAssetLinks**](AppSigningApi.md#getAndroidAssetLinks) | **GET** /.well-known/assetlinks.json | 
[**getAppleAppSiteAssociation**](AppSigningApi.md#getAppleAppSiteAssociation) | **GET** /.well-known/apple-app-site-association | 


<a name="getAndroidAssetLinks"></a>
# **getAndroidAssetLinks**
> getAndroidAssetLinks()



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AppSigningApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    AppSigningApi apiInstance = new AppSigningApi(defaultClient);
    try {
      apiInstance.getAndroidAssetLinks();
    } catch (ApiException e) {
      System.err.println("Exception when calling AppSigningApi#getAndroidAssetLinks");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="getAppleAppSiteAssociation"></a>
# **getAppleAppSiteAssociation**
> getAppleAppSiteAssociation()



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AppSigningApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    AppSigningApi apiInstance = new AppSigningApi(defaultClient);
    try {
      apiInstance.getAppleAppSiteAssociation();
    } catch (ApiException e) {
      System.err.println("Exception when calling AppSigningApi#getAppleAppSiteAssociation");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

