

# ArticleView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**articleId** | **String** |  |  [optional]
**brand** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**quantity** | **Integer** |  | 
**price** | **Double** |  | 
**discount** | **Double** |  | 
**total** | **Double** |  | 
**pictureUrl** | **String** |  |  [optional]



