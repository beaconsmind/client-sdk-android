

# OfferStatusRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **String** |  |  [optional]
**offerId** | **Integer** |  | 



