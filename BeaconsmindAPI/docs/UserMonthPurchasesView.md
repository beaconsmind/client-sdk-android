

# UserMonthPurchasesView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**month** | **Integer** |  | 
**year** | **Integer** |  | 
**totalRevenue** | **Double** |  | 
**purchases** | [**List&lt;PurchaseSummaryView&gt;**](PurchaseSummaryView.md) |  |  [optional]



