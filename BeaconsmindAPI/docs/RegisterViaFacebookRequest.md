

# RegisterViaFacebookRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userName** | **String** |  |  [optional]
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**language** | **String** |  | 
**externalAccessToken** | **String** |  | 
**fbUserId** | **String** |  |  [optional]
**provider** | **String** |  |  [optional]
**deviceRegistration** | [**DeviceRegistrationModel**](DeviceRegistrationModel.md) |  |  [optional]



