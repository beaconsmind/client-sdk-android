

# ConfigurationModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enviromentKey** | **String** |  |  [optional]
**environmentKey** | **String** |  |  [optional]
**enviromentName** | **String** |  |  [optional]
**environmentName** | **String** |  |  [optional]
**appConfigurations** | [**List&lt;AppConfigurationModel&gt;**](AppConfigurationModel.md) |  |  [optional]
**tileConfigurations** | [**List&lt;AppTileConfigurationModel&gt;**](AppTileConfigurationModel.md) |  |  [optional]



