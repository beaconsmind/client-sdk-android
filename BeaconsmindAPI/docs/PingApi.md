# PingApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**pingBeacon**](PingApi.md#pingBeacon) | **POST** /api/beacons | Record beacon contact information for single beacon. Prefer to use multiple beacons endpoint.
[**pingMultipleBeacons**](PingApi.md#pingMultipleBeacons) | **POST** /api/beacons/multiple | Record beacon contact information for multiple beacons.


<a name="pingBeacon"></a>
# **pingBeacon**
> List&lt;SentMessageModel&gt; pingBeacon(pingBeaconRequest)

Record beacon contact information for single beacon. Prefer to use multiple beacons endpoint.

If the contact results in any messaged being sent at this exact moment, the response will contain a list of them.  Usually this is empty, as message are sent asynchronous. Do not rely on return value of the endpoint containing any info.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PingApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PingApi apiInstance = new PingApi(defaultClient);
    PingBeaconRequest pingBeaconRequest = new PingBeaconRequest(); // PingBeaconRequest | 
    try {
      List<SentMessageModel> result = apiInstance.pingBeacon(pingBeaconRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PingApi#pingBeacon");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pingBeaconRequest** | [**PingBeaconRequest**](PingBeaconRequest.md)|  | [optional]

### Return type

[**List&lt;SentMessageModel&gt;**](SentMessageModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Any immediate message being sent (usually empty) |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |
**401** | Authorization token is missing or not valid. |  -  |

<a name="pingMultipleBeacons"></a>
# **pingMultipleBeacons**
> List&lt;SentMessageModel&gt; pingMultipleBeacons(pingBeaconsRequest)

Record beacon contact information for multiple beacons.

If the contact results in any messaged being sent at this exact moment, the response will contain a list of them.  Usually this is empty, as message are sent asynchronous. Do not rely on return value of the endpoint containing any info.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PingApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PingApi apiInstance = new PingApi(defaultClient);
    PingBeaconsRequest pingBeaconsRequest = new PingBeaconsRequest(); // PingBeaconsRequest | 
    try {
      List<SentMessageModel> result = apiInstance.pingMultipleBeacons(pingBeaconsRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PingApi#pingMultipleBeacons");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pingBeaconsRequest** | [**PingBeaconsRequest**](PingBeaconsRequest.md)|  | [optional]

### Return type

[**List&lt;SentMessageModel&gt;**](SentMessageModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Any immediate message being sent (usually empty) |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |
**401** | Authorization token is missing or not valid. |  -  |

