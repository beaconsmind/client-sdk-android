

# SubscribeRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**platformId** | **String** | Device id for the notification platform (FCM token, APNS token) |  [optional]
**platformType** | **PlatformType** |  | 
**phoneOS** | **PhoneOS** |  | 
**phoneManufacturer** | **String** |  |  [optional]
**phoneModel** | **String** |  |  [optional]
**phoneOsVersion** | **String** |  |  [optional]



