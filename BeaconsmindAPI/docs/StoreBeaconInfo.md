

# StoreBeaconInfo


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beaconId** | **String** |  |  [optional]
**uuid** | **String** |  |  [optional]
**major** | **String** |  |  [optional]
**minor** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**beaconType** | **String** |  |  [optional]
**cashbox** | **Boolean** |  | 
**store** | **String** |  |  [optional]



