

# NewsItem


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**videoUrl** | **String** |  |  [optional]



