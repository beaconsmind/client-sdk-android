

# AppDiagnosticsRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**utcDateTime** | **Date** |  |  [optional]
**userId** | **String** |  |  [optional]
**uniqueDeviceId** | **String** |  |  [optional]
**platformDeviceId** | **String** |  |  [optional]
**metrics** | [**List&lt;Metric&gt;**](Metric.md) |  |  [optional]



