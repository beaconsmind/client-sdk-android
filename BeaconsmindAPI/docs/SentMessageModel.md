

# SentMessageModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offerId** | **Integer** |  | 
**sentOfferId** | **Integer** |  | 
**userId** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**sent** | **Date** |  |  [optional]



