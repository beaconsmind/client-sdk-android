

# ButtonModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Optional title of the button. |  [optional]
**link** | **String** | Optional link of the button. |  [optional]



