

# LoggingLevel

## Enum


* `Verbose` (value: `1`)

* `Debug` (value: `2`)

* `Info` (value: `3`)

* `Warning` (value: `4`)

* `Error` (value: `5`)

* `Silent` (value: `999`)



