

# PingBeaconsRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **String** |  |  [optional]
**deviceId** | **String** |  |  [optional]
**deviceOs** | **String** |  |  [optional]
**currentPosition** | [**LatLongModel**](LatLongModel.md) |  |  [optional]
**events** | [**List&lt;EventModel&gt;**](EventModel.md) |  |  [optional]



