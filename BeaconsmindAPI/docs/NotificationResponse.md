

# NotificationResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offerId** | **Integer** |  | 
**messageId** | **Integer** |  | 
**title** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**validity** | **String** |  |  [optional]
**isRedeemable** | **Boolean** |  | 
**isRedeemed** | **Boolean** |  | 
**delivered** | **Date** |  |  [optional]
**callToAction** | [**Button**](Button.md) |  |  [optional]



