

# DeviceInformation


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**platformId** | **String** |  |  [optional]
**platformType** | **PlatformType** |  | 
**phoneManufacturer** | **String** |  |  [optional]
**phoneModel** | **String** |  |  [optional]
**phoneOSVersion** | **String** |  |  [optional]
**phoneOS** | [**PhoneOS**](PhoneOS.md) |  |  [optional]



