

# UserMonthPurchasesViewDataPage


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nextPageLink** | **URI** |  |  [optional] [readonly]
**count** | **Long** |  |  [readonly]
**items** | [**List&lt;UserMonthPurchasesView&gt;**](UserMonthPurchasesView.md) |  |  [optional]



