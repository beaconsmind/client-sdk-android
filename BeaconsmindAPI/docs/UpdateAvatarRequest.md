

# UpdateAvatarRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]



