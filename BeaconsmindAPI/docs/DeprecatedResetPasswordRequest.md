

# DeprecatedResetPasswordRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | 
**password** | **String** |  | 
**confirmPassword** | **String** |  | 
**code** | **String** |  | 



