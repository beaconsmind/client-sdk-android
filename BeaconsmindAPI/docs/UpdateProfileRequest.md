

# UpdateProfileRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**birthDate** | **Date** |  |  [optional]
**street** | **String** |  |  [optional]
**houseNumber** | **String** |  |  [optional]
**zipCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**language** | **String** |  |  [optional]
**landlinePhone** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**favoriteStore** | **String** |  |  [optional]
**favoriteStoreId** | **Integer** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**avatarThumbnailUrl** | **String** |  |  [optional]
**newsLetterSubscription** | **String** |  |  [optional]
**disablePushNotifications** | **String** |  |  [optional]
**loggedOut** | **String** |  |  [optional]



