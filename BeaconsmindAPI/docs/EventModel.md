

# EventModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event** | **String** |  | 
**timestamp** | **Long** | Unix timestamp in milliseconds, optional. If not provided, current UTC time will be used. |  [optional]
**uuid** | **String** | UUID of the beacon being sent | 
**major** | **String** | Major of the beacon beacon sent | 
**minor** | **String** | Minor of the beacon beacon sent | 
**distance** | **Double** | Distance in meters (will be 0 if not available) |  [optional]
**rssi** | **Double** | Signal strength in dB (will be 0 if not available) |  [optional]



