

# StoreInfo


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**storeId** | **Integer** |  | 
**name** | **String** |  |  [optional]
**location** | **String** |  |  [optional]
**country** | **String** |  |  [optional]
**street** | **String** |  |  [optional]
**zipCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**email1** | **String** |  |  [optional]
**email2** | **String** |  |  [optional]
**fax** | **String** |  |  [optional]
**storeManager** | **String** |  |  [optional]
**wireless** | **Boolean** |  | 
**numberOfCameras** | **Integer** |  | 
**instagramUrl** | **String** |  |  [optional]
**facebookUrl** | **String** |  |  [optional]
**language1** | **String** |  |  [optional]
**language2** | **String** |  |  [optional]
**coordinate** | [**CoordinateView**](CoordinateView.md) |  |  [optional]
**brands** | **List&lt;Object&gt;** |  |  [optional]
**storeOpeningTimes** | [**List&lt;StoreOpeningTimeView&gt;**](StoreOpeningTimeView.md) |  |  [optional]



