# PrivacyPolicyApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPrivacyPolicy**](PrivacyPolicyApi.md#getPrivacyPolicy) | **GET** /privacy-policy | 


<a name="getPrivacyPolicy"></a>
# **getPrivacyPolicy**
> getPrivacyPolicy()



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PrivacyPolicyApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PrivacyPolicyApi apiInstance = new PrivacyPolicyApi(defaultClient);
    try {
      apiInstance.getPrivacyPolicy();
    } catch (ApiException e) {
      System.err.println("Exception when calling PrivacyPolicyApi#getPrivacyPolicy");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

