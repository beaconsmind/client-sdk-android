

# DeviceModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nativeId** | **String** |  |  [optional]
**platform** | **String** |  |  [optional]
**manufacturer** | **String** |  |  [optional]
**model** | **String** |  |  [optional]
**osVersion** | **String** |  |  [optional]



