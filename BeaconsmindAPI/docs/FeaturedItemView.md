

# FeaturedItemView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**imageUrl** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**deleted** | **Boolean** |  | 



