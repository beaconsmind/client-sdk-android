

# AppTileConfigurationModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**imageUrl** | **String** |  |  [optional]
**secondaryImgUrl** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**icon** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**link** | **String** |  |  [optional]
**enviromentKey** | **String** |  |  [optional]
**sortOrder** | **Integer** |  |  [optional]
**firstImageUrl** | **String** |  |  [optional]
**secondImageUrl** | **String** |  |  [optional]



