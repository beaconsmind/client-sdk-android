# OffersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clicked**](OffersApi.md#clicked) | **POST** /api/offers/clicked | If notification contains Call To Action button, report that user Clicked on it.
[**getOffer**](OffersApi.md#getOffer) | **GET** /api/offers/{offerId} | Return the latest live notification that was sent to the customer.  Application should report notification interactions (i.e. Received, Read, Redeemed) back to the suite.
[**getOfferPreview**](OffersApi.md#getOfferPreview) | **GET** /api/offers/preview/{offerId} | Return HTML preview of the offer to be displayed in Whitelabel app.
[**getOffers**](OffersApi.md#getOffers) | **GET** /api/offers | Return a list of current live notifications that were sent to the customer.  Application should report notification interactions (i.e. Received, Read, Redeemed) back to the suite.
[**read**](OffersApi.md#read) | **POST** /api/offers/read | Marks the notification as Read.
[**received**](OffersApi.md#received) | **POST** /api/offers/received | Marks the notification as Received.
[**redeemed**](OffersApi.md#redeemed) | **POST** /api/offers/redeemed | Marks the notification as Redeemed.


<a name="clicked"></a>
# **clicked**
> clicked(offerStatusRequest)

If notification contains Call To Action button, report that user Clicked on it.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.OffersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    OffersApi apiInstance = new OffersApi(defaultClient);
    OfferStatusRequest offerStatusRequest = new OfferStatusRequest(); // OfferStatusRequest | 
    try {
      apiInstance.clicked(offerStatusRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling OffersApi#clicked");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerStatusRequest** | [**OfferStatusRequest**](OfferStatusRequest.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Empty |  -  |

<a name="getOffer"></a>
# **getOffer**
> OfferResponse getOffer(offerId)

Return the latest live notification that was sent to the customer.  Application should report notification interactions (i.e. Received, Read, Redeemed) back to the suite.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.OffersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    OffersApi apiInstance = new OffersApi(defaultClient);
    Integer offerId = 56; // Integer | 
    try {
      OfferResponse result = apiInstance.getOffer(offerId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling OffersApi#getOffer");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerId** | **Integer**|  |

### Return type

[**OfferResponse**](OfferResponse.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns latest live notification. |  -  |

<a name="getOfferPreview"></a>
# **getOfferPreview**
> getOfferPreview(offerId)

Return HTML preview of the offer to be displayed in Whitelabel app.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.OffersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    OffersApi apiInstance = new OffersApi(defaultClient);
    Integer offerId = 56; // Integer | 
    try {
      apiInstance.getOfferPreview(offerId);
    } catch (ApiException e) {
      System.err.println("Exception when calling OffersApi#getOfferPreview");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerId** | **Integer**|  |

### Return type

null (empty response body)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns latest offer preview. |  -  |

<a name="getOffers"></a>
# **getOffers**
> List&lt;OfferResponse&gt; getOffers()

Return a list of current live notifications that were sent to the customer.  Application should report notification interactions (i.e. Received, Read, Redeemed) back to the suite.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.OffersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    OffersApi apiInstance = new OffersApi(defaultClient);
    try {
      List<OfferResponse> result = apiInstance.getOffers();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling OffersApi#getOffers");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;OfferResponse&gt;**](OfferResponse.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the list of live notifications. |  -  |

<a name="read"></a>
# **read**
> read(offerStatusRequest)

Marks the notification as Read.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.OffersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    OffersApi apiInstance = new OffersApi(defaultClient);
    OfferStatusRequest offerStatusRequest = new OfferStatusRequest(); // OfferStatusRequest | 
    try {
      apiInstance.read(offerStatusRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling OffersApi#read");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerStatusRequest** | [**OfferStatusRequest**](OfferStatusRequest.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Empty |  -  |

<a name="received"></a>
# **received**
> received(offerStatusRequest)

Marks the notification as Received.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.OffersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    OffersApi apiInstance = new OffersApi(defaultClient);
    OfferStatusRequest offerStatusRequest = new OfferStatusRequest(); // OfferStatusRequest | 
    try {
      apiInstance.received(offerStatusRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling OffersApi#received");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerStatusRequest** | [**OfferStatusRequest**](OfferStatusRequest.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Empty |  -  |

<a name="redeemed"></a>
# **redeemed**
> redeemed(offerStatusRequest)

Marks the notification as Redeemed.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.OffersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    OffersApi apiInstance = new OffersApi(defaultClient);
    OfferStatusRequest offerStatusRequest = new OfferStatusRequest(); // OfferStatusRequest | 
    try {
      apiInstance.redeemed(offerStatusRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling OffersApi#redeemed");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offerStatusRequest** | [**OfferStatusRequest**](OfferStatusRequest.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Empty |  -  |

