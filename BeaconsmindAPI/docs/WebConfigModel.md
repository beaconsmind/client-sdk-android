

# WebConfigModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | **String** |  |  [optional]
**suiteDate** | **String** |  |  [optional]
**buildDate** | **String** |  |  [optional]
**appVersion** | **String** |  |  [optional]
**runtimeVersion** | **String** |  |  [optional]
**suiteVariant** | **String** |  |  [optional]
**currentEnvironment** | **String** |  |  [optional]
**disablePurchaseHistories** | **Boolean** |  | 
**defaultCurrency** | [**CurrencyView**](CurrencyView.md) |  |  [optional]
**supportedCurrencies** | [**List&lt;CurrencyView&gt;**](CurrencyView.md) |  |  [optional]
**defaultLanguage** | **String** |  |  [optional]
**allowedLanguages** | **List&lt;String&gt;** |  |  [optional]
**enableWhitelabelFeatures** | **Boolean** |  | 
**enablePocFeatures** | **Boolean** |  | 
**enablePurchasesImport** | **Boolean** |  | 
**enableProductsImport** | **Boolean** |  | 
**enableChangeLog** | **Boolean** |  | 
**clientApps** | [**ClientApps**](ClientApps.md) |  |  [optional]
**defaultDeepLinkPayload** | **Map&lt;String, String&gt;** |  |  [optional]



