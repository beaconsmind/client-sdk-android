

# BarcodeResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clubId** | **String** |  |  [optional]
**barcode** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]



