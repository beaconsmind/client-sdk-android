

# ImportUserRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**email** | **String** |  | 
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**birthDate** | **Date** |  |  [optional]
**language** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**deviceRegistration** | [**DeviceRegistrationModel**](DeviceRegistrationModel.md) |  |  [optional]



