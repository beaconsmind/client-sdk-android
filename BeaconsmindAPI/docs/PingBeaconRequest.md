

# PingBeaconRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **String** |  |  [optional]
**major** | **String** |  |  [optional]
**minor** | **String** |  |  [optional]
**deviceId** | **String** |  |  [optional]
**deviceOS** | **String** |  |  [optional]
**userId** | **String** |  |  [optional]
**event** | **String** |  |  [optional]
**timestamp** | **Long** |  |  [optional]
**rssi** | **Double** |  |  [optional]



