# ProfileApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCustomerCard**](ProfileApi.md#getCustomerCard) | **GET** /api/v1/profile/customer-card | 
[**updateAvatar**](ProfileApi.md#updateAvatar) | **PUT** /api/v1/profile/avatar | 


<a name="getCustomerCard"></a>
# **getCustomerCard**
> CustomerCardModel getCustomerCard()



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ProfileApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    ProfileApi apiInstance = new ProfileApi(defaultClient);
    try {
      CustomerCardModel result = apiInstance.getCustomerCard();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProfileApi#getCustomerCard");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CustomerCardModel**](CustomerCardModel.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="updateAvatar"></a>
# **updateAvatar**
> updateAvatar(updateAvatarRequest)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ProfileApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    ProfileApi apiInstance = new ProfileApi(defaultClient);
    UpdateAvatarRequest updateAvatarRequest = new UpdateAvatarRequest(); // UpdateAvatarRequest | 
    try {
      apiInstance.updateAvatar(updateAvatarRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling ProfileApi#updateAvatar");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateAvatarRequest** | [**UpdateAvatarRequest**](UpdateAvatarRequest.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

