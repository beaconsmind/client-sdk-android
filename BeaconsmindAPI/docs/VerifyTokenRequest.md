

# VerifyTokenRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**tokenPurpose** | **String** |  |  [optional]



