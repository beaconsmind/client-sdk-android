

# StoreModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**name** | **String** |  | 
**address** | **String** |  | 
**coordinates** | [**Coordinates**](Coordinates.md) |  | 
**imageUrl** | **String** |  |  [optional]
**isFavorite** | **Boolean** |  | 



