

# RegisterViaSocialProviderRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**externalAccessToken** | **String** |  | 
**providerType** | **ProviderType** |  | 
**language** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**avatarThumbnailUrl** | **String** |  |  [optional]
**deviceRegistration** | [**DeviceRegistrationModel**](DeviceRegistrationModel.md) |  |  [optional]



