# TouchpointsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**saveTouchpoint**](TouchpointsApi.md#saveTouchpoint) | **POST** /api/touchpoints | Save touchpoint of the mobile application.  Touchpoints are application events reported to the suite, like &#39;Application Downloaded&#39;, &#39;Application Opened&#39;, etc.


<a name="saveTouchpoint"></a>
# **saveTouchpoint**
> saveTouchpoint(saveTouchpointRequest)

Save touchpoint of the mobile application.  Touchpoints are application events reported to the suite, like &#39;Application Downloaded&#39;, &#39;Application Opened&#39;, etc.

When tracking AppDownload, a unique device id has to be present to allow the suite to ignore future requests.  If the application developer decides to implement single AppDownload reporting on the application side, a random  UUID can be sent instead.  For other touchpoints, user needs to be authenticated and Bearer JWT needs to be passed with the request.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.TouchpointsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    TouchpointsApi apiInstance = new TouchpointsApi(defaultClient);
    SaveTouchpointRequest saveTouchpointRequest = new SaveTouchpointRequest(); // SaveTouchpointRequest | 
    try {
      apiInstance.saveTouchpoint(saveTouchpointRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling TouchpointsApi#saveTouchpoint");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **saveTouchpointRequest** | [**SaveTouchpointRequest**](SaveTouchpointRequest.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Touchpoint was saved correctly. |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |

