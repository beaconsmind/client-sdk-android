# PurchaseHistoriesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPurchaseHistoryDetails**](PurchaseHistoriesApi.md#getPurchaseHistoryDetails) | **GET** /api/v1/purchase-histories/{purchaseHistoryId} | 
[**getUserMonthlyPurchases**](PurchaseHistoriesApi.md#getUserMonthlyPurchases) | **GET** /api/v1/purchase-histories/monthly | 


<a name="getPurchaseHistoryDetails"></a>
# **getPurchaseHistoryDetails**
> PurchaseHistoryView getPurchaseHistoryDetails(purchaseHistoryId)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PurchaseHistoriesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PurchaseHistoriesApi apiInstance = new PurchaseHistoriesApi(defaultClient);
    Integer purchaseHistoryId = 56; // Integer | 
    try {
      PurchaseHistoryView result = apiInstance.getPurchaseHistoryDetails(purchaseHistoryId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PurchaseHistoriesApi#getPurchaseHistoryDetails");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchaseHistoryId** | **Integer**|  |

### Return type

[**PurchaseHistoryView**](PurchaseHistoryView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="getUserMonthlyPurchases"></a>
# **getUserMonthlyPurchases**
> UserMonthPurchasesViewDataPage getUserMonthlyPurchases(skip, limit)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.PurchaseHistoriesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    PurchaseHistoriesApi apiInstance = new PurchaseHistoriesApi(defaultClient);
    Integer skip = 0; // Integer | Default: 0
    Integer limit = 2; // Integer | Default: 2
    try {
      UserMonthPurchasesViewDataPage result = apiInstance.getUserMonthlyPurchases(skip, limit);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PurchaseHistoriesApi#getUserMonthlyPurchases");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **Integer**| Default: 0 | [optional] [default to 0]
 **limit** | **Integer**| Default: 2 | [optional] [default to 2]

### Return type

[**UserMonthPurchasesViewDataPage**](UserMonthPurchasesViewDataPage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

