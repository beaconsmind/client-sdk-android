

# InlineObject1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | [**ImageUploadSettings**](ImageUploadSettings.md) |  |  [optional]
**image** | **File** |  |  [optional]



