# DashboardApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBlogPosts**](DashboardApi.md#getBlogPosts) | **GET** /api/v1/dashboard/blog-posts | 
[**getFeaturedItems**](DashboardApi.md#getFeaturedItems) | **GET** /api/v1/dashboard/featured-items | 


<a name="getBlogPosts"></a>
# **getBlogPosts**
> List&lt;BlogPostView&gt; getBlogPosts(search, skip, limit)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.DashboardApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    DashboardApi apiInstance = new DashboardApi(defaultClient);
    String search = "search_example"; // String | 
    Integer skip = 56; // Integer | 
    Integer limit = 56; // Integer | 
    try {
      List<BlogPostView> result = apiInstance.getBlogPosts(search, skip, limit);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DashboardApi#getBlogPosts");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **String**|  | [optional]
 **skip** | **Integer**|  | [optional]
 **limit** | **Integer**|  | [optional]

### Return type

[**List&lt;BlogPostView&gt;**](BlogPostView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="getFeaturedItems"></a>
# **getFeaturedItems**
> List&lt;FeaturedItemView&gt; getFeaturedItems(search, skip, limit)



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.DashboardApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    DashboardApi apiInstance = new DashboardApi(defaultClient);
    String search = "search_example"; // String | 
    Integer skip = 56; // Integer | 
    Integer limit = 56; // Integer | 
    try {
      List<FeaturedItemView> result = apiInstance.getFeaturedItems(search, skip, limit);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling DashboardApi#getFeaturedItems");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **String**|  | [optional]
 **skip** | **Integer**|  | [optional]
 **limit** | **Integer**|  | [optional]

### Return type

[**List&lt;FeaturedItemView&gt;**](FeaturedItemView.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

