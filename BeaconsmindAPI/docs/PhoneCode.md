

# PhoneCode


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**countryCode** | **String** |  |  [optional]
**validPrefixes** | **List&lt;String&gt;** |  |  [optional]



