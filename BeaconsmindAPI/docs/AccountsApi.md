# AccountsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUser**](AccountsApi.md#createUser) | **POST** /api/accounts/create | Create customer account, authenticated with username and password.
[**deleteUser**](AccountsApi.md#deleteUser) | **DELETE** /api/accounts | Mark an account for deletion.
[**getClubId**](AccountsApi.md#getClubId) | **GET** /api/accounts/getClubId | When the suite is integrated with 3rd party loyalty system, this returns the customers loyalty number formatted as a barcode image.
[**getProfile**](AccountsApi.md#getProfile) | **GET** /api/accounts/profile | Returns customer profile info and personal data.
[**importUser**](AccountsApi.md#importUser) | **POST** /api/accounts/import | Import customer data when using 3rd party authentication.
[**postUpdateProfile**](AccountsApi.md#postUpdateProfile) | **POST** /api/accounts/profile | Update customer profile info and personal data.
[**registerViaFacebook**](AccountsApi.md#registerViaFacebook) | **POST** /api/accounts/facebook | Create customer account, authenticated with facebook.
[**registerViaSocialProviders**](AccountsApi.md#registerViaSocialProviders) | **POST** /api/accounts/externalLogin | Create customer account, authenticated with a social Provider (currently supports: Facebook, Google, and Apple).
[**updateDevice**](AccountsApi.md#updateDevice) | **POST** /api/accounts/device | Update customer device information in case it changes. Last device sent to the API will receive push notifications.
[**updateProfile**](AccountsApi.md#updateProfile) | **PUT** /api/accounts/profile | Update customer profile info and personal data.


<a name="createUser"></a>
# **createUser**
> CreateUserResponse createUser(createUserRequest)

Create customer account, authenticated with username and password.

In case customer with same username exists, and was authenticated via Facebook, this will link the account  and allow customer to log in with password.  If the customer is successfully created, and device information is available, messages triggered by user  registration will be sent.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    CreateUserRequest createUserRequest = new CreateUserRequest(); // CreateUserRequest | 
    try {
      CreateUserResponse result = apiInstance.createUser(createUserRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#createUser");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUserRequest** | [**CreateUserRequest**](CreateUserRequest.md)|  | [optional]

### Return type

[**CreateUserResponse**](CreateUserResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the newly created customer |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |

<a name="deleteUser"></a>
# **deleteUser**
> deleteUser()

Mark an account for deletion.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    try {
      apiInstance.deleteUser();
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#deleteUser");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Empty |  -  |

<a name="getClubId"></a>
# **getClubId**
> BarcodeResponse getClubId()

When the suite is integrated with 3rd party loyalty system, this returns the customers loyalty number formatted as a barcode image.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    try {
      BarcodeResponse result = apiInstance.getClubId();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#getClubId");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BarcodeResponse**](BarcodeResponse.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Loyalty information. |  -  |
**401** | Authorization token is missing or not valid. |  -  |

<a name="getProfile"></a>
# **getProfile**
> ProfileResponse getProfile()

Returns customer profile info and personal data.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    try {
      ProfileResponse result = apiInstance.getProfile();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#getProfile");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ProfileResponse**](ProfileResponse.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Profile info |  -  |
**401** | Authorization token is missing or not valid. |  -  |

<a name="importUser"></a>
# **importUser**
> TokenResponse importUser(importUserRequest)

Import customer data when using 3rd party authentication.

When customer is using 3rd party authentication and not relying on Beaconsmind to keep the customer private data,  this endpoint is used to import customer into Beaconsmind and to obtain a token with which app can send tracking data  to us. The token does NOT provide access to any personal customer info.  If personal data is present in the request, it will be updated only first time the endpoint is invoked for that customer.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    ImportUserRequest importUserRequest = new ImportUserRequest(); // ImportUserRequest | 
    try {
      TokenResponse result = apiInstance.importUser(importUserRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#importUser");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **importUserRequest** | [**ImportUserRequest**](ImportUserRequest.md)|  | [optional]

### Return type

[**TokenResponse**](TokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Tracking access token |  -  |

<a name="postUpdateProfile"></a>
# **postUpdateProfile**
> ProfileResponse postUpdateProfile(updateProfileRequest)

Update customer profile info and personal data.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest(); // UpdateProfileRequest | 
    try {
      ProfileResponse result = apiInstance.postUpdateProfile(updateProfileRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#postUpdateProfile");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateProfileRequest** | [**UpdateProfileRequest**](UpdateProfileRequest.md)|  | [optional]

### Return type

[**ProfileResponse**](ProfileResponse.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Updated profile info |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |
**401** | Authorization token is missing or not valid. |  -  |

<a name="registerViaFacebook"></a>
# **registerViaFacebook**
> TokenResponse registerViaFacebook(registerViaFacebookRequest)

Create customer account, authenticated with facebook.

In case customer with same username exists, and was authenticated via password, this will link the account  and allow customer to log in with password as well.  If the customer is successfully created, and device information is available, messages triggered by user  registration will be sent.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    RegisterViaFacebookRequest registerViaFacebookRequest = new RegisterViaFacebookRequest(); // RegisterViaFacebookRequest | 
    try {
      TokenResponse result = apiInstance.registerViaFacebook(registerViaFacebookRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#registerViaFacebook");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerViaFacebookRequest** | [**RegisterViaFacebookRequest**](RegisterViaFacebookRequest.md)|  | [optional]

### Return type

[**TokenResponse**](TokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Customers access token |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |

<a name="registerViaSocialProviders"></a>
# **registerViaSocialProviders**
> TokenResponse registerViaSocialProviders(registerViaSocialProviderRequest)

Create customer account, authenticated with a social Provider (currently supports: Facebook, Google, and Apple).

In case customer with same token exists, and was authenticated via password, this will link the account  and allow customer to log in with password as well.  If the customer is successfully created, and device information is available, messages triggered by user  registration will be sent.  External Access Token must contain email and ideally first and last name.  Note: Apple provides the first and last name in the callback (not in the token), so please provide names in the request for Apple.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    RegisterViaSocialProviderRequest registerViaSocialProviderRequest = new RegisterViaSocialProviderRequest(); // RegisterViaSocialProviderRequest | 
    try {
      TokenResponse result = apiInstance.registerViaSocialProviders(registerViaSocialProviderRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#registerViaSocialProviders");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registerViaSocialProviderRequest** | [**RegisterViaSocialProviderRequest**](RegisterViaSocialProviderRequest.md)|  | [optional]

### Return type

[**TokenResponse**](TokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Customer&#39;s access token |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |

<a name="updateDevice"></a>
# **updateDevice**
> DeviceModel updateDevice(deviceModel)

Update customer device information in case it changes. Last device sent to the API will receive push notifications.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    DeviceModel deviceModel = new DeviceModel(); // DeviceModel | 
    try {
      DeviceModel result = apiInstance.updateDevice(deviceModel);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#updateDevice");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceModel** | [**DeviceModel**](DeviceModel.md)|  | [optional]

### Return type

[**DeviceModel**](DeviceModel.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Updated device info |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |
**401** | Authorization token is missing or not valid. |  -  |

<a name="updateProfile"></a>
# **updateProfile**
> ProfileResponse updateProfile(updateProfileRequest)

Update customer profile info and personal data.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.auth.*;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.AccountsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure HTTP bearer authorization: BmsClientKey
    HttpBearerAuth BmsClientKey = (HttpBearerAuth) defaultClient.getAuthentication("BmsClientKey");
    BmsClientKey.setBearerToken("BEARER TOKEN");

    AccountsApi apiInstance = new AccountsApi(defaultClient);
    UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest(); // UpdateProfileRequest | 
    try {
      ProfileResponse result = apiInstance.updateProfile(updateProfileRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling AccountsApi#updateProfile");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **updateProfileRequest** | [**UpdateProfileRequest**](UpdateProfileRequest.md)|  | [optional]

### Return type

[**ProfileResponse**](ProfileResponse.md)

### Authorization

[BmsClientKey](../README.md#BmsClientKey)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Updated profile info |  -  |
**400** | Validation did not pass, response will contain informational message. |  -  |
**401** | Authorization token is missing or not valid. |  -  |

