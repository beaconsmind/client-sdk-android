

# CreateUserRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**password** | **String** |  | 
**confirmPassword** | **String** |  | 
**language** | **String** |  |  [optional]
**countryCode** | **String** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**favoriteStore** | **String** |  |  [optional]
**favoriteStoreId** | **Integer** |  |  [optional]
**birthday** | **Date** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**avatarThumbnailUrl** | **String** |  |  [optional]
**deviceRegistration** | [**DeviceRegistrationModel**](DeviceRegistrationModel.md) |  |  [optional]
**isAdministrator** | **Boolean** |  |  [optional]
**isSuperAdministrator** | **Boolean** |  |  [optional]



