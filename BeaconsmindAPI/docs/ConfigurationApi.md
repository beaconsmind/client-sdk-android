# ConfigurationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAppConfiguration**](ConfigurationApi.md#getAppConfiguration) | **GET** /api/v1/configuration/app | App configurations for a user, BundleId, platform, or default.
[**getBeacons**](ConfigurationApi.md#getBeacons) | **GET** /api/configuration/beacons | Return a list of configured beacons.
[**getConfiguration**](ConfigurationApi.md#getConfiguration) | **GET** /api/configuration | Return suite configuration with store and beacon information.
[**getEnvironmentConfiguration**](ConfigurationApi.md#getEnvironmentConfiguration) | **GET** /api/configuration/get | Return configuration values for given environment.
[**getWebConfiguration**](ConfigurationApi.md#getWebConfiguration) | **GET** /api/v1/configuration/web | 


<a name="getAppConfiguration"></a>
# **getAppConfiguration**
> AppConfigurationResponse getAppConfiguration(userId, phoneOS, bundleId)

App configurations for a user, BundleId, platform, or default.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ConfigurationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ConfigurationApi apiInstance = new ConfigurationApi(defaultClient);
    String userId = "userId_example"; // String | 
    PhoneOS phoneOS = PhoneOS.fromValue("0"); // PhoneOS | 
    String bundleId = "bundleId_example"; // String | 
    try {
      AppConfigurationResponse result = apiInstance.getAppConfiguration(userId, phoneOS, bundleId);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationApi#getAppConfiguration");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**|  | [optional]
 **phoneOS** | [**PhoneOS**](.md)|  | [optional] [enum: 0, 1, 2]
 **bundleId** | **String**|  | [optional]

### Return type

[**AppConfigurationResponse**](AppConfigurationResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Configuration that most specifically matches the values provided, otherwise a global default. |  -  |

<a name="getBeacons"></a>
# **getBeacons**
> List&lt;StoreBeaconInfo&gt; getBeacons()

Return a list of configured beacons.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ConfigurationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ConfigurationApi apiInstance = new ConfigurationApi(defaultClient);
    try {
      List<StoreBeaconInfo> result = apiInstance.getBeacons();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationApi#getBeacons");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;StoreBeaconInfo&gt;**](StoreBeaconInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the list of beacons |  -  |

<a name="getConfiguration"></a>
# **getConfiguration**
> AppSettings getConfiguration()

Return suite configuration with store and beacon information.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ConfigurationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ConfigurationApi apiInstance = new ConfigurationApi(defaultClient);
    try {
      AppSettings result = apiInstance.getConfiguration();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationApi#getConfiguration");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AppSettings**](AppSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Configuration containing store and beacon information |  -  |

<a name="getEnvironmentConfiguration"></a>
# **getEnvironmentConfiguration**
> ConfigurationModel getEnvironmentConfiguration(environmentKey)

Return configuration values for given environment.

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ConfigurationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ConfigurationApi apiInstance = new ConfigurationApi(defaultClient);
    String environmentKey = "environmentKey_example"; // String | Default: null
    try {
      ConfigurationModel result = apiInstance.getEnvironmentConfiguration(environmentKey);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationApi#getEnvironmentConfiguration");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **environmentKey** | **String**| Default: null | [optional]

### Return type

[**ConfigurationModel**](ConfigurationModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns configuration values |  -  |

<a name="getWebConfiguration"></a>
# **getWebConfiguration**
> WebConfigModel getWebConfiguration()



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ConfigurationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ConfigurationApi apiInstance = new ConfigurationApi(defaultClient);
    try {
      WebConfigModel result = apiInstance.getWebConfiguration();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationApi#getWebConfiguration");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**WebConfigModel**](WebConfigModel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

