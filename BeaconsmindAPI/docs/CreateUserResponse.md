

# CreateUserResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | 
**id** | **String** |  | 
**userName** | **String** |  | 
**fullName** | **String** |  | 
**favoriteStore** | **String** |  | 
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**gender** | **String** |  | 
**language** | **String** |  | 
**street** | **String** |  | 
**houseNumber** | **String** |  | 
**zipCode** | **String** |  | 
**city** | **String** |  | 
**country** | **String** |  | 
**landlinePhone** | **String** |  | 
**birthDate** | **Date** |  |  [optional]
**favoriteStoreId** | **Integer** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**avatarThumbnailUrl** | **String** |  |  [optional]
**clubId** | **String** |  |  [optional]
**disablePushNotifications** | **Boolean** |  | 
**newsLetterSubscription** | **Boolean** |  | 
**phoneNumber** | **String** |  | 
**joinDate** | **Date** |  | 
**roles** | **List&lt;String&gt;** |  |  [optional]
**claims** | **List&lt;String&gt;** |  |  [optional]
**token** | [**TokenResponse**](TokenResponse.md) |  |  [optional]



