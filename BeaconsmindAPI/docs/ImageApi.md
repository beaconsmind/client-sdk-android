# ImageApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](ImageApi.md#create) | **POST** /api/v1/images | 


<a name="create"></a>
# **create**
> ImageUploadResponse create(settings, image)



Receives multipart/form-data. Example JS code for uploading images:        var image &#x3D; $(&#39;input[name&#x3D;image]&#39;)[0];      if (image.files.length &gt; 0) {            var settings &#x3D; {              generateThumbnail: $(&#39;input[name&#x3D;generateThumbnail]&#39;)[0].checked,              generateGrayscale: $(&#39;input[name&#x3D;generateGrayscale]&#39;)[0].checked,              generateThumbnailGrayscale: $(&#39;input[name&#x3D;generateThumbnailGrayscale]&#39;)[0].checked          };            var payload &#x3D; new FormData();          payload.append(&#39;settings&#39;, new Blob([JSON.stringify(settings)], { type: &#39;application/json&#39; }));          payload.append(&#39;image&#39;, image.files[0], image.filename);            $.ajax({                  url: &#39;http://local.bms-api.com/api/v1/images&#39;,              type: &#39;POST&#39;,              data: payload,              success: function(data) {                      alert(JSON.stringify(data));                  },              cache: false,              contentType: false,              processData: false          });      }  

### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.ImageApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    ImageApi apiInstance = new ImageApi(defaultClient);
    ImageUploadSettings settings = new ImageUploadSettings(); // ImageUploadSettings | 
    File image = new File("/path/to/file"); // File | 
    try {
      ImageUploadResponse result = apiInstance.create(settings, image);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ImageApi#create");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **settings** | [**ImageUploadSettings**](ImageUploadSettings.md)|  | [optional]
 **image** | **File**|  | [optional]

### Return type

[**ImageUploadResponse**](ImageUploadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

