

# PrivacyPolicySettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientLegalName** | **String** |  |  [optional]
**clientAppName** | **String** |  |  [optional]
**beaconsmindLegalName** | **String** |  |  [optional]
**privacySupportEmail** | **String** |  |  [optional]
**requiresUserPhoneNumber** | **Boolean** |  | 



