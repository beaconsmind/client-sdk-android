

# PurchaseHistoryView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**utcDate** | **Date** |  | 
**clubId** | **String** |  |  [optional]
**salesPersonId** | **String** |  |  [optional]
**externalStoreId** | **String** |  |  [optional]
**externalPurchaseHistoryId** | **String** |  |  [optional]
**totalPrice** | **Double** |  | 
**totalDiscount** | **Double** |  | 
**quantity** | **Integer** |  | 
**totalVat** | **Double** |  |  [optional]
**vatNumber** | **String** |  |  [optional]
**card** | [**CardView**](CardView.md) |  |  [optional]
**invoice** | [**InvoiceView**](InvoiceView.md) |  |  [optional]
**store** | [**StoreView**](StoreView.md) |  |  [optional]
**articles** | [**List&lt;ArticleView&gt;**](ArticleView.md) |  |  [optional]
**vatItems** | [**List&lt;VatItemView&gt;**](VatItemView.md) |  |  [optional]



