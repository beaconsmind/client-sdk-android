

# StoreView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**country** | **String** |  |  [optional]
**street** | **String** |  |  [optional]
**zipCode** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**coordinates** | [**Coordinates**](Coordinates.md) |  |  [optional]
**isFavorite** | **Boolean** |  | 
**imageUrl** | **String** |  |  [optional]



