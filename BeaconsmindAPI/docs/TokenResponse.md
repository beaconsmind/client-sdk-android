

# TokenResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **String** |  |  [optional]
**accessToken** | **String** |  |  [optional]
**tokenType** | **String** |  |  [optional]
**expiresIn** | **String** |  |  [optional]



