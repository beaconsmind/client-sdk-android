

# ProductModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**imageUrl** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**currency** | **String** |  |  [optional]
**fromPrice** | **Double** |  |  [optional]
**toPrice** | **Double** |  |  [optional]
**categoryImageUrl** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**categoryName** | **String** |  |  [optional]
**deleted** | **Boolean** |  | 



