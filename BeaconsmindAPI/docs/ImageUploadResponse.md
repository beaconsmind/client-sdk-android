

# ImageUploadResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imagePath** | **String** |  |  [optional]
**imageGrayscalePath** | **String** |  |  [optional]
**thumbnailPath** | **String** |  |  [optional]
**thumbnailGrayscalePath** | **String** |  |  [optional]



