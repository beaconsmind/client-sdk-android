

# ForgotPasswordRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**doNotSendEmail** | **Boolean** |  |  [optional]



