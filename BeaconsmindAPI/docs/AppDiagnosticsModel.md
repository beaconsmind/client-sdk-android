

# AppDiagnosticsModel


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**utcTime** | **Date** |  | 
**suiteTime** | **Date** |  | 
**userId** | **String** |  |  [optional]
**uniqueDeviceId** | **String** |  |  [optional]
**platformDeviceId** | **String** |  |  [optional]
**metricsJson** | **String** |  |  [optional]



