

# AppConfigurationResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**loggingEnabled** | **Boolean** |  | 
**loggingLevel** | **LoggingLevel** |  | 
**bugFenderKey** | **String** |  |  [optional]
**metricsEnabled** | **Boolean** |  | 
**metricsIntervalSeconds** | **Integer** |  |  [optional]
**defaultCurrency** | [**CurrencyView**](CurrencyView.md) |  |  [optional]
**pocPublicKey** | **String** |  |  [optional]



