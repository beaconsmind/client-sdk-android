

# UnsubscribeRequest


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**platformId** | **String** |  |  [optional]
**platformType** | **PlatformType** |  | 



