

# PurchaseSummaryView


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseHistoryId** | **Integer** |  | 
**utcDate** | **Date** |  | 
**totalPrice** | **Double** |  | 
**quantity** | **Integer** |  | 
**store** | [**StoreReferenceView**](StoreReferenceView.md) |  |  [optional]



