# NewsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getHomeScreen**](NewsApi.md#getHomeScreen) | **GET** /api/news/homescreen | 
[**getNews**](NewsApi.md#getNews) | **GET** /api/news | 


<a name="getHomeScreen"></a>
# **getHomeScreen**
> HomeScreenResponse getHomeScreen()



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.NewsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    NewsApi apiInstance = new NewsApi(defaultClient);
    try {
      HomeScreenResponse result = apiInstance.getHomeScreen();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NewsApi#getHomeScreen");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**HomeScreenResponse**](HomeScreenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

<a name="getNews"></a>
# **getNews**
> List&lt;NewsItem&gt; getNews()



### Example
```java
// Import classes:
import com.beaconsmind.api.client.ApiClient;
import com.beaconsmind.api.client.ApiException;
import com.beaconsmind.api.client.Configuration;
import com.beaconsmind.api.client.models.*;
import com.beaconsmind.api.NewsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");

    NewsApi apiInstance = new NewsApi(defaultClient);
    try {
      List<NewsItem> result = apiInstance.getNews();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling NewsApi#getNews");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;NewsItem&gt;**](NewsItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Success |  -  |

