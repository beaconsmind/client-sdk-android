# client-sdk-android

[![](https://jitpack.io/v/com.beaconsmind/client-sdk-android.svg)](https://jitpack.io/#com.beaconsmind/client-sdk-android)

Welcome to the README guide for Beaconsmind SDK!

This quick guide assumes knowledge about Android development, package managers, application signing and debug/release versions of the apps.
The project is using standard Gradle setup for building and deploying Android apps.

## Requirements

- Android Studio version 2020.3.1 or later is required to build the project.
- Project's minimum SDK: 23
- Target SDK: 33
- Compile SDK: 33

## Installing the SDK

Add JitPack repository to your build file, in your root build.gradle at the end of repositories

```groowy
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

* Add a single dependency in your modules build.gradle dependencies


```groowy
dependencies {
    ...
    implementation 'com.beaconsmind:client-sdk-android:version-tag'
}
```
* Or if you are including dependencies for individual artifacts from JitPack make sure to include both API and SDK

```groowy
dependencies {
    ...
    implementation 'com.beaconsmind.client-sdk-android:sdk:version-tag'
    implementation 'com.beaconsmind.client-sdk-android:api:version-tag'
}
```

## Usage

For a complete usage guide please check our [Documentation](../Documentation)
- SDK integration is described in [Usage Guide](../Documentation/UsageGuide.md)
- About [Using Beacons](../Documentation/Beacons.md) in general
- Documentation of the [exposed UI Widgets](../Documentation/UI.md)
- [Debug and Testing](../Documentation/DebugAndTest.md)
 
### Quick start

We provide a quick initialization method for development test integration purposes.

Initialize the SDK by calling the `BeaconsmindSdk.initializeDevelop` method from any *Context* with your *BeaconsmindConfig* object. 
Preferably do this as soon as possible after your app starts, in the onCreate() method of your Application class.

When initialized with this method the sdk requests permissions on it's own. This way you don't have to worry about device prerequirements and you can focus on testing our core features.

```java
public class DemoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        String suitUri = "SUITE_URL";
        BeaconsmindConfig.Builder builder = new BeaconsmindConfig.Builder(suitUri)
                .usePassiveScanning(true) // true for scanning without persistent notification
                // notification setting is not-required for passive scanning
                .setNotificationBadge(R.drawable.ic_beacons)
                .setNotificationTitle("Beaconsmind")
                .setNotificationText("Listening for Beacons")
                .setNotificationChannelName("Beacon Listener Notification");

        // quick setup for testing the sdk 
        BeaconsmindSdk.initializeDevelop(this, builder.build(this));
        // for development and production use: 
        // BeaconsmindSdk.initialize(this, builder.build(this));
    }
}
```

## Proguard rules
If using proguard with Beaconsmind Android SDK versions prior to version 1.7.0 add a custom rule to your proguard-rules.pro:
- `-keep class com.beaconsmind.api.models** { *; }`

## Structure

### [BeaconsmindSDK](./BeaconsmindSDK)

Project containing Beaconsmind SDK for initiating and managing beacon scanning and reporting. SDK is written with minimal SDK 23 support.

Beacon scanning relies on Android foreground service to make scanning long lived and precise. On newer Android SDK, this requires setting up service notification do be displayed.

Short class descriptions located in [com.beaconsmind.sdk](./BeaconsmindSDK/src/main/java/com/beaconsmind/sdk):

- BeaconsmindSdk.java

> Main class for setting up and initializing  SDK.

- BeaconListenerService.java

> Class encapsulating broadcast functionality (fetch beacons, push events, start, stop).

- UserManager.java

> Class responsible for user authentication, uses BroadcastService to start broadcasting upon successfull authentication.

- OffersManager.java

> Class used for managing offers and push notification interactions


### [BeaconsmindAPI](./BeaconsmindAPI)

Project containing generated Java classes for accessing Beaconsmind Suite API.
Classes are generated using openapi-generator executed on [api.v1.yaml](./api.v1.yaml).
The code comes pregenerated, but if update is needed Gradle task is available:

#### Windows

```
./gradlew.bat generateApiClient
```

#### Linux

```
./gradlew generateApiClient
```

## Author

Beaconsmind AG

- Augustin Juričić (augustinjuricic@beaconsmind.com)
- Josip Lasić (josiplasic@beaconsmind.com)

## License

client-sdk-android is available under the MIT license. See the LICENSE file for more info.
