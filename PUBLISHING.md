## Steps before publishing a new SDK release

1. Add all changes with version number in [Changelog](./Documentation/Changelog.md)
2. Go to [BeaconsmindSDK/build.gradle](./BeaconsmindSDK/build.gradle) and change **versionName** field
3. Publish the SDK to JitPack by creating the release with a version number tag (see more below)

## What is JitPack

* Jitpack builds Git projects on demand and provides with artifacts (jar, aar)
* it can build any project that is publicly available on the git repositories
* it can build from specific commits, branches, PRs
* artifacts are build on private docker containers and made publicly available


### Is JitPack similar to Maven Central?

JitPack is a public maven repository and serves maven artifacts. In that sense it is similar to Maven Central. However, JitPack takes a completely different approach to how you get your artifacts in the repository. With Maven Central you build the artifacts yourself and then upload them. With JitPack you create a git tag for a release and it will build the artifacts from source.


## Publishing to JitPack

JitPack searches for releases in github repositories and builds artifacts from those releases. To publish new lib version on JitPack

1. Create a release in GitLab

How to create a release: [Releases | GitLab](https://docs.gitlab.com/ee/user/project/releases/#create-a-release). Make sure you follow proper versioning tag.

2. Go to JitPack and create a new build

Visit [https://jitpack.io/private#com.beaconsmind/client-sdk-android](https://jitpack.io/private#com.beaconsmind/client-sdk-android) and if the artifacts are not yet generated (green status log) click on **_Get It_**. JitPack will generate the artifact (.aar) and serve it in its maven repository. Then it can easily be added as dependency in other android projects. The dependency has the following format: <code>com.beaconsmind:Repo:&lt;Git Tag></code></strong>

The latest release and its associated JitPack dependency are visible to library users in form of a badge in project’s updated Readme.md on GitLab repository. The badge is updated automatically with every new release.


### Limiting releases

To limit who can create new releases on GitLab (and publish a new release on JitPack) there needs to be a limitation on who can create tags, as tags are necessary to create releases. 

To do so we need to introduce protected tags, ie. only some accounts are granted tag creation: [Protected tags | GitLab](https://docs.gitlab.com/ee/user/project/protected_tags.html#configuring-protected-tags)

Specifically, wildcard protected tag ( * ) should be added and allowed only to certain groups/users.
