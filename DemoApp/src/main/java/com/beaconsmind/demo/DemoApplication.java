package com.beaconsmind.demo;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Configuration;

import com.beaconsmind.demo.util.FileLoggingTree;
import com.beaconsmind.sdk.BeaconListenerService;
import com.beaconsmind.sdk.BeaconsmindConfig;
import com.beaconsmind.sdk.BeaconsmindSdk;
import com.beaconsmind.sdk.Constants;
import com.beaconsmind.sdk.models.LogLevel;
import com.beaconsmind.sdk.ui.BeaconsmindUI;
import com.beaconsmind.sdk.ui.LocalizationStrings;
import com.beaconsmind.sdk.ui.StyleSheet;

import timber.log.Timber;

public class DemoApplication extends Application implements Configuration.Provider {

    @Override
    public void onCreate() {
        super.onCreate();

        setupLogging();

        // SDK initialization

        final String defaultSuiteUri = "https://test-develop-suite.azurewebsites.net";
        final PreferencesManager preferencesManager = PreferencesManager.getInstance(this);
        String suiteUri = preferencesManager.getSuiteUri();
        if (suiteUri == null) {
            suiteUri = defaultSuiteUri;
            preferencesManager.setSuiteUri(defaultSuiteUri);
        }

        BeaconsmindConfig.Builder builder = new BeaconsmindConfig.Builder(suiteUri)
                .setNotificationBadge(R.drawable.ic_beacons)
                .setNotificationTitle("Beaconsmind")
                .setNotificationText("Listening for Beacons")
                .setNotificationChannelName("Beacon Listener Notification")
                .usePassiveScanning(PreferencesManager.getInstance(this).isUsingPassiveScanning());

        BeaconsmindSdk.setMinLogLevel(LogLevel.VERBOSE);
        BeaconsmindSdk.initialize(this, builder.build(this));

        // USE_SIMULATOR set in build.gradle(:DemoApp) buildConfigField
        // use "emulator" build variant to start with simulator turned on
        BeaconsmindSdk.enableSimulator(BuildConfig.USE_SIMULATOR);
        if (BuildConfig.USE_SIMULATOR) {
            Toast.makeText(this, "Using beacon simulator", Toast.LENGTH_SHORT).show();
        }

        // ble activity notification
        setupBleActivityNotification();

        // stylize beaconsmind UI components
        BeaconsmindUI.setStyleSheet(new StyleSheet.Builder().setUseToolbar(true).build());
        BeaconsmindUI.setLocalizationStrings(new LocalizationStrings.Builder().build());

        FCMServiceKt.registerChannel(this);
    }

    private void setupLogging() {
        // timber trees
        Timber.plant(new FileLoggingTree(this));
        Timber.plant(new Timber.DebugTree());

        // log crashes
        Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            Timber.tag(Constants.LOGTAG + ".DemoApp").e(e, "Unhandled exception");
            if (defaultHandler != null)
                defaultHandler.uncaughtException(t, e);
        });
    }

    private void setupBleActivityNotification() {
        // observe forever is safe since BeaconListenerService is bound to application context
        BeaconListenerService.getInstance().getBLEActivityLiveData().observeForever(bleStatus -> {

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Constants.NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(BeaconsmindSdk.getConfig().getNotificationBadge())
                    .setContentTitle(getString(R.string.ble_notification_title));

            switch (bleStatus) {
                case ENABLED:
                    notificationManager.notify(Constants.NOTIFICATION_FG_SERVICE, BeaconsmindSdk.createForegroundNotification());
                    break;
                case DISABLED:
                    builder.setContentText(getString(R.string.ble_bt_location));
                    notificationManager.notify(Constants.NOTIFICATION_FG_SERVICE, builder.build());
                    break;
                case BT_OFF:
                    builder.setContentText(getString(R.string.ble_bt));
                    notificationManager.notify(Constants.NOTIFICATION_FG_SERVICE, builder.build());
                    break;
                case LOCATION_OFF:
                    builder.setContentText(getString(R.string.ble_location));
                    notificationManager.notify(Constants.NOTIFICATION_FG_SERVICE, builder.build());
                    break;
            }

        });
    }

    @NonNull
    @Override
    public Configuration getWorkManagerConfiguration() {
        return new Configuration.Builder().build();
    }
}
