package com.beaconsmind.demo.ui.register;

enum RegisterResult {
    INVALID_FIRSTNAME,
    INVALID_LASTNAME,
    INVALID_EMAIL,
    INVALID_PASSWORD,
    PASSWORD_NOT_MATCH,
    SUCCEEDED,
    FAILED
}
