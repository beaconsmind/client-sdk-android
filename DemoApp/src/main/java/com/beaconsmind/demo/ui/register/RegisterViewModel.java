package com.beaconsmind.demo.ui.register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.beaconsmind.demo.util.TextUtils;
import com.beaconsmind.sdk.Error;
import com.beaconsmind.sdk.OnCompleteListener;
import com.beaconsmind.sdk.UserManager;

public class RegisterViewModel extends ViewModel {

    private final MutableLiveData<RegisterResult> registerResult;
    private final UserManager userManager;

    public RegisterViewModel() {
        this.registerResult = new MutableLiveData<>();
        this.userManager = UserManager.getInstance();
    }

    LiveData<RegisterResult> getRegisterResult() {
        return registerResult;
    }

    public void register(RegistrationForm form) {
        if (form.getFirstname().isEmpty()) {
            registerResult.postValue(RegisterResult.INVALID_FIRSTNAME);
            return;
        }
        if (form.getLastname().isEmpty()) {
            registerResult.postValue(RegisterResult.INVALID_LASTNAME);
            return;
        }
        if (!TextUtils.isUserNameValid(form.getEmail())) {
            registerResult.postValue(RegisterResult.INVALID_EMAIL);
            return;
        }
        if (form.getPassword().isEmpty()) {
            registerResult.postValue(RegisterResult.INVALID_PASSWORD);
            return;
        }
        if (!form.getPassword().equals(form.getConfirmPassword())) {
            registerResult.postValue(RegisterResult.PASSWORD_NOT_MATCH);
            return;
        }

        userManager.createAccount(form.getUserRequest(), new OnCompleteListener() {
            @Override
            public void onSuccess() {
                registerResult.postValue(RegisterResult.SUCCEEDED);
            }

            @Override
            public void onError(Error error) {
                registerResult.postValue(RegisterResult.FAILED);
            }
        });

    }
}
