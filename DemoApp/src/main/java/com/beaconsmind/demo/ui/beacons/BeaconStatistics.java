package com.beaconsmind.demo.ui.beacons;

import androidx.annotation.NonNull;

public class BeaconStatistics {

    public final String store;
    public final String name;
    public final String uuid;
    public final String major;
    public final String minor;
    public final int nmContacts;
    public final Long lastContactAgo;
    public final boolean isInRange;
    public final int freqValue;
    public final Double currentRssi;
    public final Double averageRssi;

    public BeaconStatistics(@NonNull String store,
                            @NonNull String name,
                            @NonNull String uuid,
                            @NonNull String major,
                            @NonNull String minor,
                            int nmContacts,
                            Long lastContactAgo,
                            boolean isInRange,
                            int freqValue,
                            Double currentRssi,
                            Double averageRssi) {
        this.store = store;
        this.name = name;
        this.uuid = uuid;
        this.major = major;
        this.minor = minor;
        this.nmContacts = nmContacts;
        this.lastContactAgo = lastContactAgo;
        this.isInRange = isInRange;
        this.freqValue = freqValue;
        this.currentRssi = currentRssi;
        this.averageRssi = averageRssi;
    }
}
