package com.beaconsmind.demo.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.beaconsmind.demo.R;
import com.beaconsmind.demo.ui.login.LoginActivity;
import com.beaconsmind.sdk.OfferPayload;
import com.beaconsmind.sdk.UserManager;
import com.beaconsmind.sdk.ui.components.OfferDetailsActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();

        UserManager userManager = UserManager.getInstance();
        Intent intent = new Intent(this, LoginActivity.class);

        if (userManager.isAuthenticated()) {
            OfferPayload offerPayload = OfferPayload.fromIntent(getIntent());
            if (offerPayload != null) {
                intent = OfferDetailsActivity.getIntent(this, offerPayload.getOfferId())
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                intent = new Intent(this, MainActivity.class);
            }
        }

        startActivity(intent);
        finish();
    }

}