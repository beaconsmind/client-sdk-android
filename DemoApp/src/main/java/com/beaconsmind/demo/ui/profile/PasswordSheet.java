package com.beaconsmind.demo.ui.profile;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.beaconsmind.api.models.ChangePasswordRequest;
import com.beaconsmind.demo.R;
import com.beaconsmind.demo.databinding.SheetPasswordBinding;
import com.beaconsmind.sdk.Error;
import com.beaconsmind.sdk.OnCompleteListener;
import com.beaconsmind.sdk.UserManager;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class PasswordSheet extends BottomSheetDialogFragment implements View.OnClickListener {

    private SheetPasswordBinding binding;
    private Handler handler;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = SheetPasswordBinding.inflate(inflater, container, false);
        handler = new Handler(Looper.getMainLooper());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.btnSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        String oldPassword = binding.inputOldPassword.getText().toString();
        String newPassword = binding.inputNewPassword.getText().toString();
        String confirmPassword = binding.inputConfirmPassword.getText().toString();
        if (oldPassword.isEmpty()) {
            toast(R.string.invalid_password);
            return;
        }
        if (newPassword.isEmpty()){
            toast(R.string.invalid_password);
            return;
        }
        if (!newPassword.equals(confirmPassword)) {
            toast(R.string.password_not_match);
            return;
        }

        ChangePasswordRequest request = new ChangePasswordRequest();
        request.oldPassword(oldPassword);
        request.newPassword(newPassword);

        UserManager.getInstance().changePassword(request, new OnCompleteListener() {
            @Override
            public void onSuccess() {
                handler.post(() -> {
                    toast(R.string.password_change_notification);
                    dismiss();
                });
            }

            @Override
            public void onError(Error error) {
                handler.post(() -> {
                    toast(R.string.error);
                });
            }
        });
    }

    private void toast(@StringRes int res) {
        Toast.makeText(requireContext(), res, Toast.LENGTH_SHORT).show();
    }
}
