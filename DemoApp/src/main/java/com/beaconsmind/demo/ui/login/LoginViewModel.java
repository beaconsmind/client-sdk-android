package com.beaconsmind.demo.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.beaconsmind.demo.util.TextUtils;
import com.beaconsmind.sdk.Error;
import com.beaconsmind.sdk.OnCompleteListener;
import com.beaconsmind.sdk.UserManager;

public class LoginViewModel extends ViewModel {

    private final MutableLiveData<LoginResult> loginResult;
    private final UserManager userManager;

    public LoginViewModel() {
        this.loginResult = new MutableLiveData<>();
        this.userManager = UserManager.getInstance();
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(String username, String password) {
        if (!TextUtils.isUserNameValid(username)) {
            loginResult.postValue(LoginResult.INVALID_USERNAME);
            return;
        }

        if (password.isEmpty()) {
            loginResult.postValue(LoginResult.INVALID_PASSWORD);
            return;
        }

        userManager.login(username, password, new OnCompleteListener() {
            @Override
            public void onSuccess() {
                loginResult.postValue(LoginResult.LOGIN_SUCCEEDED);
            }

            @Override
            public void onError(Error error) {
                loginResult.postValue(LoginResult.LOGIN_FAILED);
            }
        });
    }
}