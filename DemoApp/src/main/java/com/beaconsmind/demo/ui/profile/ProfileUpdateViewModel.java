package com.beaconsmind.demo.ui.profile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.beaconsmind.api.models.ProfileResponse;
import com.beaconsmind.api.models.UpdateProfileRequest;
import com.beaconsmind.sdk.Error;
import com.beaconsmind.sdk.OnCompleteListener;
import com.beaconsmind.sdk.OnResultListener;
import com.beaconsmind.sdk.UserManager;

public class ProfileUpdateViewModel extends ViewModel {

    private final MutableLiveData<ProfileResponse> profile;
    private final MutableLiveData<String> message;

    public ProfileUpdateViewModel() {
        profile = new MutableLiveData<>();
        message = new MutableLiveData<>();
    }

    public LiveData<ProfileResponse> getProfile() {
        return profile;
    }

    public LiveData<String> getMessage() {
        return message;
    }

    public void loadProfile() {
        UserManager.getInstance().getAccount(new OnResultListener<ProfileResponse>() {
            @Override
            public void onSuccess(ProfileResponse result) {
                profile.postValue(result);
            }

            @Override
            public void onError(Error error) {
                message.postValue("Error getting profile details");
            }
        });
    }

    public void updateProfile(UpdateProfileRequest request) {
        UserManager.getInstance().updateAccount(request, new OnCompleteListener() {
            @Override
            public void onSuccess() {
                message.postValue("Updated");
            }

            @Override
            public void onError(Error error) {
                message.postValue("Error updating profile details");
            }
        });
    }
}
