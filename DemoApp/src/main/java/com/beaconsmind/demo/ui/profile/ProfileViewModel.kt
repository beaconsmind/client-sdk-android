package com.beaconsmind.demo.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beaconsmind.sdk.Error
import com.beaconsmind.sdk.OnCompleteListener
import com.beaconsmind.sdk.UserManager

data class LogoutState(
    val isLoading: Boolean = false,
    val isSuccess: Boolean? = null,
    val error: Error? = null,
)

class ProfileViewModel : ViewModel() {

    private val _logoutLD = MutableLiveData<LogoutState>()
    val logoutState: LiveData<LogoutState> = _logoutLD

    fun logout() {
        _logoutLD.value = LogoutState(isLoading = true)

        UserManager.getInstance().logout(object : OnCompleteListener {
            override fun onSuccess() {
                _logoutLD.postValue(
                    LogoutState(isLoading = false, isSuccess = true, error = null)
                )
            }

            override fun onError(error: Error) {
                _logoutLD.postValue(LogoutState(isSuccess = false, error = error))
            }

        })
    }
}