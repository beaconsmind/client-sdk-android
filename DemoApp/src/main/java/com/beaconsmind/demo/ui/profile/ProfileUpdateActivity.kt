package com.beaconsmind.demo.ui.profile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.beaconsmind.api.models.ProfileResponse
import com.beaconsmind.api.models.UpdateProfileRequest
import com.beaconsmind.demo.R
import com.beaconsmind.demo.databinding.ActivityProfileUpdateBinding
import java.text.SimpleDateFormat
import java.util.*

class ProfileUpdateActivity : AppCompatActivity(), View.OnClickListener {

    private var _binding: ActivityProfileUpdateBinding? = null
    private val binding: ActivityProfileUpdateBinding get() = _binding!!

    private val viewModel: ProfileUpdateViewModel by viewModels()

    private val formatter
        @SuppressLint("SimpleDateFormat")
        get() = SimpleDateFormat(resources.getString(R.string.birthFormat))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityProfileUpdateBinding.inflate(layoutInflater)
        setContentView(binding.root)


        viewModel.loadProfile()

        viewModel.message.observe(this) { text: String? ->
            Toast.makeText(this,
                text,
                Toast.LENGTH_SHORT).show()
        }

        viewModel.profile.observe(this) { profile: ProfileResponse ->
            binding.inputFirstname.setText(profile.firstName)
            binding.inputLastname.setText(profile.lastName)
            binding.inputEmail.setText(profile.userName)
            binding.inputStreet.setText(profile.street)
            binding.inputCity.setText(profile.city)
            binding.inputCountry.setText(profile.country)
            binding.inputLanguage.setText(profile.language)
            binding.inputBirth.setText(profile.birthDate?.let { formatter.format(it) })
            binding.inputGender.setText(profile.gender)
            binding.btnUpdate.setOnClickListener(this)
        }
    }

    override fun onClick(v: View) {
        val profileRequest = UpdateProfileRequest()
        profileRequest.firstName = binding.inputFirstname.text.toString()
        profileRequest.lastName = binding.inputLastname.text.toString()
        profileRequest.street = binding.inputStreet.text.toString()
        profileRequest.city = binding.inputCity.text.toString()
        profileRequest.country = binding.inputCountry.text.toString()
        profileRequest.language = binding.inputLanguage.text.toString()
        profileRequest.gender = binding.inputGender.text.toString()
        formatter.parse(binding.inputBirth.text.toString())?.let {
            profileRequest.birthDate = it
        }

        viewModel.updateProfile(profileRequest)
    }
}