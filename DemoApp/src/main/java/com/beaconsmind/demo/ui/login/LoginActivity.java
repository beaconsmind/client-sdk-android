package com.beaconsmind.demo.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.beaconsmind.demo.R;
import com.beaconsmind.demo.databinding.ActivityLoginBinding;
import com.beaconsmind.demo.ui.MainActivity;
import com.beaconsmind.demo.ui.register.RegisterActivity;
import com.beaconsmind.demo.util.ExtensionsKt;
import com.beaconsmind.demo.util.KeyboardUtil;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    private ActivityLoginBinding binding;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        inputEmail = binding.inputEmail;
        inputPassword = binding.inputPassword;
        progressBar = binding.loading;

        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        loginViewModel.getLoginResult().observe(this, loginResult -> {
            progressBar.setVisibility(View.GONE);
            switch (loginResult) {
                case LOGIN_FAILED:
                    toast(R.string.login_failed);
                    break;
                case INVALID_USERNAME:
                    toast(R.string.invalid_username);
                    break;
                case INVALID_PASSWORD:
                    toast(R.string.invalid_password);
                    break;
                case LOGIN_SUCCEEDED:
                    updateUiWithUser();
                    break;

            }
        });

        inputPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                progressBar.setVisibility(View.VISIBLE);
                loginViewModel.login(
                        inputEmail.getText().toString(),
                        inputPassword.getText().toString());
            }
            return false;
        });

        binding.btnLogin.setOnClickListener(v -> {
            progressBar.setVisibility(View.VISIBLE);
            loginViewModel.login(
                    inputEmail.getText().toString(),
                    inputPassword.getText().toString());
        });

        binding.tvRegister.setOnClickListener(v -> {
            this.startActivity(new Intent(this, RegisterActivity.class));
            this.finish();
        });


        binding.btnServer.setOnClickListener(v -> {
            BackendSheet dialog = new BackendSheet();
            dialog.show(getSupportFragmentManager(), dialog.getTag());
        });

        binding.tvVersion.setText(ExtensionsKt.getVersionString(this));

    }

    private void updateUiWithUser() {
        KeyboardUtil.hideKeyboard(this);
        startActivity(new Intent(this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK)
        );
        finish();
    }

    private void toast(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}