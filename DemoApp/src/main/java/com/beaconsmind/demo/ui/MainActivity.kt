package com.beaconsmind.demo.ui

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.beaconsmind.demo.R
import com.beaconsmind.demo.databinding.ActivityMainBinding
import com.beaconsmind.sdk.BeaconListenerService
import com.beaconsmind.sdk.Constants
import com.beaconsmind.sdk.permissions.SdkPermissionsHelper
import timber.log.Timber

class MainActivity : AppCompatActivity(), SdkPermissionsHelper.ActivityResultCallback {

    private var navController: NavController? = null

    private val permissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navController = Navigation.findNavController(this, R.id.nav_host)
        NavigationUI.setupWithNavController(binding.navView, navController!!)
        SdkPermissionsHelper.requestPermissions(this, true)
    }

    // request notification permission after SdkPermissionHelper is done
    override fun onBeaconsmindPermissionsResult(startPossible: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            permissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        }
    }

    override fun onStart() {
        super.onStart()
        
        // service recovery if needed
        if (
                BeaconListenerService.shouldRestartService(this) &&
                SdkPermissionsHelper.canStartBeaconService(this)
        ) {
            Timber.tag("${Constants.LOGTAG}.DemoApp").i("Attempting BeaconListenerService restart")
            BeaconListenerService.getInstance().startListeningBeacons { e ->
                // service failed to start
                Timber.tag("${Constants.LOGTAG}.DemoApp").e(e, "Service failed to start")
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val navId = intent.getIntExtra(NAV_ID, 0)
        if (navId != 0) {
            navController!!.navigate(navId)
        }
    }

    companion object {
        const val NAV_ID = "navId"
    }
}