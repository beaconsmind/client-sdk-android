package com.beaconsmind.demo.ui.login;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.beaconsmind.demo.PreferencesManager;
import com.beaconsmind.demo.R;
import com.beaconsmind.demo.databinding.SheetBackendBinding;
import com.beaconsmind.sdk.BeaconsmindSdk;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class BackendSheet extends BottomSheetDialogFragment {

    private List<RadioButton> predefinedButtons;
    private SheetBackendBinding binding;
    private PreferencesManager preferencesManager;
    private InputMethodManager imm;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        binding = SheetBackendBinding.inflate(inflater, container, false);
        binding.selectDevelop.setTag("https://test-develop-suite.azurewebsites.net");
        binding.selectRelease.setTag("https://test-release-suite.azurewebsites.net");
        binding.selectCustom.setTag(null);
        predefinedButtons = new ArrayList<RadioButton>();
        predefinedButtons.add(binding.selectRelease);
        predefinedButtons.add(binding.selectDevelop);
        imm = (InputMethodManager) this.requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        preferencesManager = PreferencesManager.getInstance(requireContext());
        String suiteUri = preferencesManager.getSuiteUri();
        binding.inputUri.setText(suiteUri);
        initRadioSelection(suiteUri);
        binding.selectCustom.setOnClickListener(this::enableCustomField);
        binding.selectDevelop.setOnClickListener(this::handlePredefinedClick);
        binding.selectRelease.setOnClickListener(this::handlePredefinedClick);
        binding.inputUri.setOnKeyListener(this::enableButton);
        binding.btnSubmit.setOnClickListener(this::updateBackendServer);
    }

    private boolean enableButton(View view, int keyCode, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        binding.btnSubmit.setEnabled(true);
        return true;
    }

    private void handlePredefinedClick(View view) {
        binding.inputUri.setEnabled(false);
        binding.inputUri.setText(view.getTag().toString());
        binding.btnSubmit.setEnabled(true);
    }

    private void enableCustomField(View view) {
        binding.inputUri.setEnabled(true);
        binding.inputUri.setText("");
        binding.btnSubmit.setEnabled(true);
    }

    private void initRadioSelection(String suiteUri) {
        binding.btnSubmit.setEnabled(false);
        for (RadioButton button : predefinedButtons) {
            if (button.getTag().equals(suiteUri)) {
                button.setChecked(true);
                binding.inputUri.setEnabled(false);
                return;
            }
        }
        binding.selectCustom.setChecked(true);
        binding.inputUri.setEnabled(true);
    }

    private void updateBackendServer(View v) {
        String suiteUri = binding.inputUri.getText().toString();
        if (suiteUri.isEmpty()) {
            return;
        }
        preferencesManager.setSuiteUri(suiteUri);
        BeaconsmindSdk.updateSuiteUri(suiteUri);
        String message = getString(R.string.backend_change_notification, suiteUri);
        binding.btnSubmit.setEnabled(false);
        toast(message);
        dismiss();
    }

    private void toast(@NotNull String message) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
    }
}
