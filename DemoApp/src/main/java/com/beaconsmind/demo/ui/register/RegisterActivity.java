package com.beaconsmind.demo.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.beaconsmind.demo.R;
import com.beaconsmind.demo.databinding.ActivityRegisterBinding;
import com.beaconsmind.demo.ui.MainActivity;
import com.beaconsmind.demo.ui.login.LoginActivity;
import com.beaconsmind.demo.util.KeyboardUtil;

public class RegisterActivity extends AppCompatActivity {

    private ActivityRegisterBinding binding;
    private RegisterViewModel registerViewModel;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        progressBar = binding.loading;

        registerViewModel = new ViewModelProvider(this).get(RegisterViewModel.class);

        registerViewModel.getRegisterResult().observe(this, registerResult -> {
            progressBar.setVisibility(View.GONE);
            switch (registerResult) {
                case INVALID_FIRSTNAME:
                    toast(R.string.invalid_firstname);
                    break;
                case INVALID_LASTNAME:
                    toast(R.string.invalid_lastname);
                    break;
                case INVALID_EMAIL:
                    toast(R.string.invalid_email);
                    break;
                case INVALID_PASSWORD:
                    toast(R.string.invalid_password);
                    break;
                case PASSWORD_NOT_MATCH:
                    toast(R.string.password_not_match);
                    break;
                case FAILED:
                    toast(R.string.registration_failed);
                    break;
                case SUCCEEDED:
                    updateUiWithUser();
                    break;
            }
        });


        binding.inputConfirmPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                progressBar.setVisibility(View.VISIBLE);
                RegistrationForm form = buildForm();
                registerViewModel.register(form);
            }
            return false;
        });

        binding.btnRegister.setOnClickListener(v -> {
            progressBar.setVisibility(View.VISIBLE);
            RegistrationForm form = buildForm();
            registerViewModel.register(form);
        });

        binding.tvLogin.setOnClickListener(v -> {
            this.startActivity(new Intent(this, LoginActivity.class));
            this.finish();
        });

    }

    public RegistrationForm buildForm() {
        return new RegistrationForm(
                binding.inputFirstname.getText().toString(),
                binding.inputLastname.getText().toString(),
                binding.inputEmail.getText().toString(),
                binding.inputPassword.getText().toString(),
                binding.inputConfirmPassword.getText().toString()
        );
    }

    private void updateUiWithUser() {
        KeyboardUtil.hideKeyboard(this);
        startActivity(new Intent(this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }

    private void toast(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}
