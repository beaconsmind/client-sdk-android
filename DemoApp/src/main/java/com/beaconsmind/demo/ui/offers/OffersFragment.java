package com.beaconsmind.demo.ui.offers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.beaconsmind.demo.databinding.FragmentOffersDemoBinding;


public class OffersFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        FragmentOffersDemoBinding binding = FragmentOffersDemoBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }
}