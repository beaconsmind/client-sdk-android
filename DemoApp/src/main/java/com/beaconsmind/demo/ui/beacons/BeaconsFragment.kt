package com.beaconsmind.demo.ui.beacons

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.beaconsmind.demo.PreferencesManager
import com.beaconsmind.demo.databinding.FragmentBeaconsBinding
import com.beaconsmind.sdk.BeaconListenerService
import com.beaconsmind.sdk.BeaconsmindSdk.config
import com.jakewharton.processphoenix.ProcessPhoenix
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class BeaconsFragment : Fragment() {

    private var _binding: FragmentBeaconsBinding? = null
    private val binding: FragmentBeaconsBinding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBeaconsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val beaconAdapter = BeaconAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.adapter = beaconAdapter

        binding.checkboxScan.isChecked = config.usePassiveScanning
        binding.checkboxScan.setOnCheckedChangeListener { _, isChecked: Boolean ->
            PreferencesManager.getInstance(context).isUsingPassiveScanning = isChecked
            ProcessPhoenix.triggerRebirth(context)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                val service = BeaconListenerService.getInstance()
                service.flow().collect { beaconStatistics ->
                    if (service.isRunning) {
                        binding.loading.visibility = View.VISIBLE
                        binding.emptyMsg.visibility = when {
                            beaconStatistics.isEmpty() -> View.VISIBLE
                            else -> View.GONE
                        }
                        binding.loading.visibility = View.GONE
                        beaconAdapter.submitBeacons(beaconStatistics)
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun BeaconListenerService.flow() = flow {
        while (true) {
            emit(getStatistics(this@flow))
            delay(REFRESH_DELAY)
        }
    }

    private companion object {
        const val REFRESH_DELAY = 1000L

        fun getStatistics(service: BeaconListenerService): List<BeaconStatistics> {
            val stats = service.beaconsSummary.map { summary ->
                val lastContactAgo = summary.getLastContact()?.let { System.currentTimeMillis() - it }

                BeaconStatistics(
                    summary.store,
                    summary.name,
                    summary.uuid,
                    summary.major,
                    summary.minor,
                    summary.timesContacted,
                    lastContactAgo,
                    summary.isInRange,
                    summary.getFrequency(),
                    summary.currentRssi,
                    summary.averageRssi
                )
            }.toMutableList()

            stats.sortWith { o1: BeaconStatistics, o2: BeaconStatistics ->
                when {
                    o1.lastContactAgo != null && o2.lastContactAgo != null -> {
                        o1.lastContactAgo.compareTo(o2.lastContactAgo)
                    }
                    o1.lastContactAgo != null -> -1
                    o2.lastContactAgo != null -> 1
                    else -> String.CASE_INSENSITIVE_ORDER.compare(o1.name, o2.name)
                }
            }

            return stats
        }
    }
}