package com.beaconsmind.demo.ui.beacons

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.beaconsmind.demo.R
import com.beaconsmind.demo.databinding.ItemBeaconBinding
import com.beaconsmind.demo.ui.beacons.BeaconAdapter.BeaconViewHolder
import com.beaconsmind.demo.util.TimeUtils

class BeaconAdapter : RecyclerView.Adapter<BeaconViewHolder>() {

    private var prevValue = emptyList<BeaconStatistics>()

    fun submitBeacons(statistics: List<BeaconStatistics>) {
        val result = DiffUtil.calculateDiff(DiffCallback(prevValue, statistics))
        prevValue = statistics
        result.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeaconViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val binding = ItemBeaconBinding.inflate(inflater, parent, false)
        return BeaconViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BeaconViewHolder, position: Int) {
        val beaconSummary = prevValue[position]
        holder.bind(beaconSummary)
    }

    override fun getItemCount(): Int {
        return prevValue.size
    }

    class BeaconViewHolder(private val binding: ItemBeaconBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(statistic: BeaconStatistics) {
            val r = binding.root.resources
            binding.beaconName.text = statistic.name
            binding.storeName.text = statistic.store
            binding.beaconUuidMajorMinor.text = r.getString(
                R.string.beacon_uuid_major_minor,
                statistic.uuid,
                statistic.major,
                statistic.minor
            )

            if (statistic.nmContacts == 0) {
                binding.contact.setText(R.string.never)
                binding.summary.visibility = View.GONE
                binding.signalStrength.visibility = View.GONE
            } else {
                binding.contact.text =
                    statistic.lastContactAgo?.let { TimeUtils.toDuration(it) }
                        ?: r.getText(R.string.long_time_ago)

                val quantity = r.getQuantityString(
                    R.plurals.beacon_contacts,
                    statistic.nmContacts,
                    statistic.nmContacts
                )

                binding.summary.text = r.getString(
                    R.string.beacon_frequency,
                    quantity,
                    statistic.freqValue
                )

                if (statistic.currentRssi == null || statistic.averageRssi == null) {
                    binding.signalStrength.setText(R.string.signal_unavailable)
                } else {
                    binding.signalStrength.text = r.getString(
                        R.string.signal_strength,
                        statistic.currentRssi,
                        statistic.averageRssi
                    )
                }

                binding.summary.visibility = View.VISIBLE
                binding.signalStrength.visibility = View.VISIBLE
            }

            binding.clHolder.backgroundTintList = when {
                statistic.isInRange -> ColorStateList.valueOf(Color.parseColor("#80DAFFDA"))
                else -> null
            }
        }
    }

    private class DiffCallback(
        private val oldList: List<BeaconStatistics>,
        private val newList: List<BeaconStatistics>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldList.size
        }

        override fun getNewListSize(): Int {
            return newList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldS = oldList[oldItemPosition]
            val newS = newList[newItemPosition]
            return oldS.uuid == newS.uuid && oldS.major == newS.major && oldS.minor == newS.minor
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val prevValue = oldList[oldItemPosition]
            val newValue = newList[newItemPosition]
            val prevLastContact = prevValue.lastContactAgo?.let { TimeUtils.toDuration(it) } ?: ""
            val newLastContact = newValue.lastContactAgo?.let { TimeUtils.toDuration(it) } ?: ""

            return newValue.nmContacts == prevValue.nmContacts &&
                    newLastContact == prevLastContact &&
                    newValue.currentRssi == prevValue.currentRssi &&
                    newValue.averageRssi == prevValue.averageRssi &&
                    newValue.store == prevValue.store &&
                    newValue.name == prevValue.name &&
                    newValue.uuid == prevValue.uuid &&
                    newValue.major == prevValue.major &&
                    newValue.minor == prevValue.minor &&
                    newValue.freqValue == prevValue.freqValue
        }
    }
}