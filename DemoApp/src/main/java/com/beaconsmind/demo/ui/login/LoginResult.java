package com.beaconsmind.demo.ui.login;

enum LoginResult {
    LOGIN_SUCCEEDED,
    LOGIN_FAILED,
    INVALID_USERNAME,
    INVALID_PASSWORD
}