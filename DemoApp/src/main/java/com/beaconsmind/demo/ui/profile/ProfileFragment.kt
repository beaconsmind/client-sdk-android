package com.beaconsmind.demo.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.beaconsmind.demo.databinding.FragmentProfileBinding
import com.beaconsmind.demo.ui.SplashActivity
import com.beaconsmind.demo.util.LogsUtil
import com.beaconsmind.demo.util.getVersionString
import com.beaconsmind.sdk.UserManager.Companion.getInstance

class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.logoutState.observe(viewLifecycleOwner) {
            with(it) {
                binding.loader.visibility = if (isLoading) View.VISIBLE else View.GONE

                if (error != null) {
                    Toast.makeText(requireContext(), "Logout $error", Toast.LENGTH_SHORT).show()
                }

                if (isSuccess == true) {
                    startActivity(
                        Intent(requireActivity(), SplashActivity::class.java).apply {
                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        }
                    )
                    requireActivity().finish()
                }
            }
        }

        val fullName = getInstance().jwtPayload!!.displayName
        binding.name.text = fullName

        binding.btnDetails.setOnClickListener {
            val intent = Intent(requireActivity(), ProfileUpdateActivity::class.java)
            startActivity(intent)
        }

        binding.btnResetPassword.setOnClickListener {
            val dialog = PasswordSheet()
            dialog.show(childFragmentManager, dialog.tag)
        }

        binding.btnLogout.setOnClickListener { viewModel.logout() }

        binding.btnSendLogs.setOnClickListener { LogsUtil.shareLogs(requireContext()) }

        binding.tvVersion.text = context?.getVersionString()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}