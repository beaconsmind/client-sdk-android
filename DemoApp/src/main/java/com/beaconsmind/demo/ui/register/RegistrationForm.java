package com.beaconsmind.demo.ui.register;

import com.beaconsmind.api.models.CreateUserRequest;

public class RegistrationForm {

    private final String firstname;
    private final String lastname;
    private final String email;
    private final String password;
    private final String confirmPassword;

    public RegistrationForm(String firstname, String lastname,
                            String email, String password,
                            String confirmPassword) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public CreateUserRequest getUserRequest() {
        CreateUserRequest userRequest = new CreateUserRequest();
        userRequest.setUsername(email);
        userRequest.setFirstName(firstname);
        userRequest.setLastName(lastname);
        userRequest.password(password);
        userRequest.confirmPassword(confirmPassword);
        return userRequest;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }
}
