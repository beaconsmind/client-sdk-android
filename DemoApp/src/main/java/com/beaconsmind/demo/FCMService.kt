package com.beaconsmind.demo

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.beaconsmind.demo.ui.MainActivity
import com.beaconsmind.demo.ui.SplashActivity
import com.beaconsmind.sdk.OfferPayload
import com.beaconsmind.sdk.OffersManager
import com.beaconsmind.sdk.UserManager
import com.beaconsmind.sdk.ui.components.OfferDetailsActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

fun registerChannel(context: Context) {
    // Since android Oreo notification channel is needed.

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channelId = context.resources.getString(R.string.offers_notification_channel_id)
        val channelName = context.resources.getString(R.string.offers_notification_channel_name)
        val channel = NotificationChannel(
            channelId, channelName, NotificationManager.IMPORTANCE_HIGH
        )
        val notificationManager =
            context.getSystemService(FirebaseMessagingService.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }
}

class FCMService : FirebaseMessagingService() {
    private var channelId: String? = null


    override fun onNewToken(s: String) {
        UserManager.getInstance().updateDeviceInfo(s)
    }

    override fun onCreate() {
        super.onCreate()
        channelId = getString(R.string.offers_notification_channel_id)
    }

    override fun handleIntent(intent: Intent) {
        super.handleIntent(intent)

        // intercepts all incoming firebase messages
        val offer = OfferPayload.fromIntent(intent)
        if (offer != null) {
            OffersManager.getInstance().markOfferAsReceived(offer.offerId)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        // messages received while app in foreground
        val offerPayload = OfferPayload.tryParse(remoteMessage.data)

        val notificationPayload = remoteMessage.notification

        if (notificationPayload != null) {
            val intent = createIntent(offerPayload)

            val pendingIntent = PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
            )

            val notificationBuilder = NotificationCompat.Builder(this, channelId!!)
                .setSmallIcon(R.drawable.ic_offers)
                .setContentTitle(notificationPayload.title)
                .setContentText(notificationPayload.body)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(offerPayload?.offerId ?: 0, notificationBuilder.build())
        }
    }

    private fun createIntent(offerPayload: OfferPayload?): Intent {
        return if (!UserManager.getInstance().isAuthenticated) {
            Intent(this, SplashActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        } else if (offerPayload != null) {
            OfferDetailsActivity.getIntent(this, offerPayload.offerId)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        } else {
            Intent(this, MainActivity::class.java)
                .putExtra(MainActivity.NAV_ID, R.id.navigation_offers)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }
}