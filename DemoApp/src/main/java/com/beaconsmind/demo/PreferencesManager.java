package com.beaconsmind.demo;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

public class PreferencesManager {

    private static final String PREF_NAME = "com.beaconsmind.demo";
    private static final String PREF_SUITE_URI = "suiteUri";
    private static final String PREF_PASSIVE_SCAN = "passiveScan";

    private static volatile PreferencesManager instance = null;
    private static final Object lock = new Object();
    private final SharedPreferences preferences;


    @NonNull
    public static PreferencesManager getInstance(Context context) {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new PreferencesManager(context);
                }
            }
        }
        return instance;
    }

    private PreferencesManager(Context context) {
        preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public String getSuiteUri() {
        return preferences.getString(PREF_SUITE_URI, null);
    }

    public void setSuiteUri(String value) {
        preferences.edit().putString(PREF_SUITE_URI, value).apply();
    }

    public boolean isUsingPassiveScanning() {
        return preferences.getBoolean(PREF_PASSIVE_SCAN, false);
    }

    public void setUsingPassiveScanning(boolean value) {
        preferences.edit().putBoolean(PREF_PASSIVE_SCAN, value).apply();
    }

}
