package com.beaconsmind.demo.util;

import android.util.Patterns;

public final class TextUtils {

    public static boolean isUserNameValid(String username) {
        if (username.isEmpty()) return false;
        return Patterns.EMAIL_ADDRESS.matcher(username).matches();
    }
}
