package com.beaconsmind.demo.util;

import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.Arrays;
import java.util.List;

public class TimeUtils {

    public static final List<Long> times = Arrays.asList(
            DAYS.toMillis(7),
            DAYS.toMillis(1),
            HOURS.toMillis(1),
            MINUTES.toMillis(1),
            SECONDS.toMillis(1)
    );

    public static final List<String> timesString = Arrays.asList(
            "wk", "day", "hr", "min", "sec"
    );

    public static String toDuration(long durationMilliseconds) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < times.size(); i++) {
            Long current = times.get(i);
            long nmIntervals = durationMilliseconds / current;
            if (nmIntervals > 0) {
                sb.append(nmIntervals)
                        .append(" ")
                        .append(timesString.get(i))
                        .append(nmIntervals > 1 ? "s" : "")
                        .append(" ago");
                break;
            }
        }
        return sb.toString().isEmpty() ? "now" : sb.toString();
    }
}