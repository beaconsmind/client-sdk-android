package com.beaconsmind.demo.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.core.content.FileProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

public final class LogsUtil {
    public static void shareLogs(Context c) {
        File logs = new File(c.getFilesDir(), "/logs");

        ArrayList<Uri> uris = new ArrayList<>();
        for (File file : Objects.requireNonNull(logs.listFiles())) {
            Uri uri = FileProvider.getUriForFile(c, c.getApplicationContext().getPackageName() + ".provider", file);
            uris.add(uri);
        }

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "BeaconsmindSDK Demo app logs");
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        shareIntent.setType("text/html");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        c.startActivity(Intent.createChooser(shareIntent, "Share logs..."));
    }
}
