package com.beaconsmind.demo.util

import android.content.Context
import com.beaconsmind.demo.BuildConfig
import com.beaconsmind.demo.R
import com.beaconsmind.sdk.BuildConfig as SdkBuildConfig

fun Context.getVersionString(): String = resources.getString(
    R.string.version_placeholder,
    BuildConfig.BUILD_TYPE,
    BuildConfig.VERSION_NAME,
    BuildConfig.VERSION_CODE,
    SdkBuildConfig.SDK_VERSION
)
