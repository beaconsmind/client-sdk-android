package com.beaconsmind.sdk;

import com.beaconsmind.sdk.utils.EvictingQueue
import com.beaconsmind.sdk.utils.UniqueEvictingQueue
import org.junit.Assert
import org.junit.Test

class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        Assert.assertEquals(4, (2 + 2).toLong())
    }

    @Test
    fun test_evictingQueue() {
        val queue = EvictingQueue<Int>(2)
        val list: MutableList<Int> = ArrayList()
        list.add(1)
        list.add(2)
        list.add(3)
        queue.addAll(list)
        for (i in queue) println(i)
        Assert.assertEquals(2, queue.size.toLong())
    }

    @Test
    fun test_uniqueEvictingQueue() {
        val queue = UniqueEvictingQueue<Int>(2)
        val list: MutableList<Int> = ArrayList()
        list.add(1)
        list.add(2)
        list.add(3)
        list.add(3)
        queue.addAll(list)
        for (i in queue) println(i)
        Assert.assertEquals(2, queue.size.toLong())
    }
}