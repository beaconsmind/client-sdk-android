package com.beaconsmind.sdk

import com.beaconsmind.api.TouchpointsApi
import com.beaconsmind.api.client.ApiException
import com.beaconsmind.api.models.SaveTouchpointRequest
import com.beaconsmind.api.models.TouchpointType
import com.beaconsmind.sdk.BeaconsmindSdk.config
import com.beaconsmind.sdk.api.KotlinApiCallback
import com.beaconsmind.sdk.logging.TimberBMS
import java.util.*


/**
 * Helper for sending [TouchpointsApi] requests to beaconsmind suite.
 */
internal object TouchPointHelper {

    /**
     * Send [TouchpointType.Login] request.
     */
    @JvmStatic
    fun notifyUserLoggedIn() {
        sendTouchPoint(createRequest(TouchpointType.Login))
    }

    /**
     * Send [TouchpointType.Registration] request.
     */
    @JvmStatic
    fun notifyUserRegistered() {
        sendTouchPoint(createRequest(TouchpointType.Registration))
    }

    /**
     * Send [TouchpointType.AppOpen] request.
     */
    @JvmStatic
    fun notifyAppOpened() {
        sendTouchPoint(createRequest(TouchpointType.AppOpen))
    }

    /**
     * Send [TouchpointType.AppDownload] request
     */
    @JvmStatic
    fun notifyAppDownloaded(uniqueDeviceId: String) {
        val request = createRequest(TouchpointType.AppDownload)
        request.uniqueDeviceId = uniqueDeviceId
        sendTouchPoint(request)
    }


    /**
     * Helper for creating [SaveTouchpointRequest].
     *
     * All requests should have unique device ID attached to it for tracking users across logins.
     */
    private fun createRequest(type: TouchpointType): SaveTouchpointRequest {
        val request = SaveTouchpointRequest()
        request.appVersion = config.appVersion
        request.localDateTime = Calendar.getInstance().time
        request.type = type
        request.uniqueDeviceId = PersistenceManager.getInstance().deviceUUID
        return request
    }

    /**
     * [TouchpointsApi] call
     */
    private fun sendTouchPoint(request: SaveTouchpointRequest) {
        val nameForLogs = request.type.name
        val touchpointsApi = TouchpointsApi(BeaconsmindSdk.getApiClient())
        try {
            touchpointsApi.saveTouchpointAsync(request, KotlinApiCallback(
                _onSuccess = { _, _, _ ->
                    TimberBMS.d("Sent %s touchpoint.", nameForLogs)
                },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "Error sending %s touchpoint", nameForLogs)
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Error sending %s touchpoint", nameForLogs)
        }
    }
}