package com.beaconsmind.sdk

import android.app.*
import android.content.Context
import android.os.Build
import androidx.activity.result.ActivityResultCaller
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ProcessLifecycleOwner
import com.beaconsmind.api.client.ApiClient
import com.beaconsmind.api.models.TouchpointType
import com.beaconsmind.sdk.api.BeaconsmindApiClient
import com.beaconsmind.sdk.internal.ForegroundObserver
import com.beaconsmind.sdk.internal.RemoteDebuggingManager
import com.beaconsmind.sdk.logging.RequestDurationInterceptor
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.logging.TimberToLoggerBridge
import com.beaconsmind.sdk.models.LogLevel
import com.beaconsmind.sdk.utils.UniqueDeviceID
import com.beaconsmind.sdk.utils.startExplicitMode
import io.gsonfire.util.RFC3339DateFormat
import okhttp3.OkHttpClient
import org.altbeacon.beacon.logging.LogManager
import org.jetbrains.annotations.ApiStatus
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

/**
 * Main SDK initialization object.
 * Before using the SDK call [BeaconsmindSdk.initialize] in your Application#onCreate.
 * Set log level using [BeaconsmindSdk.minLogLevel]
 *
 * For development kickstart use [BeaconsmindSdk.initializeDevelop]
 */
object BeaconsmindSdk {

    @Volatile
    private var application: Application? = null

    /**
     * Config set after [BeaconsmindSdk.initialize] call. Use [config] as public getter
     *
     * @see BeaconsmindSdk.config
     */
    private lateinit var sdkConfig: BeaconsmindConfig

    /**
     * Api client is set on [BeaconsmindSdk.initialize] call and can be changed with [BeaconsmindSdk.updateSuiteUri]
     *
     * @see BeaconsmindSdk.getApiClient
     */
    private lateinit var apiClient: ApiClient


    /* Public fields. Always accessible. */

    /**
     * Toggle for SDK logging. Can be called when SDK is not initialized and changed while running.
     */
    @JvmStatic
    var minLogLevel: LogLevel = LogLevel.SILENT


    /**
     * Field indicating if it is safe to use SDK manager classes such as [BeaconListenerService], [UserManager], etc..
     */
    @JvmStatic
    val isInitialized
        get() = application != null


    /**
     * Initializes the SDK. Call this method as soon as your app starts, preferably in your [Application.onCreate]
     * so the sdk can track all touchpoints and beacon events correctly.
     *
     * Ignores multiple calls if initializing with the same [config].
     * It will persist config changes for [BeaconsmindSdkInitializer] use case
     *
     * @param app application context
     * @param config config to setup beaconsmindSdk
     *
     * @return true if beaconsmind sdk initialized on this call, false if it was already initialized
     */
    @JvmStatic
    fun initialize(app: Application, config: BeaconsmindConfig): Boolean {

        // setup logging
        LogManager.setLogger(TimberToLoggerBridge())
        if (Timber.treeCount == 0) {
            // plant tree only if host app has none
            Timber.plant(Timber.DebugTree())
        }

        RemoteDebuggingManager.restoreConfig(app)

        if (isInitialized) {
            TimberBMS.w("BeaconsmindSdk was already initialized. You should call #initialize it in your Application#onCreate")
            val persistedConfig = PersistenceManager.getInstance().beaconsmindConfig
            if (persistedConfig == config) {
                TimberBMS.i("Ignoring new initialization. BeaconsmindConfig didn't change")
            } else {
                TimberBMS.w("BeaconsmindConfig changed. Persisting it for future initialization.")
                PersistenceManager.getInstance().beaconsmindConfig = config
            }
            return false
        }

        // initialize the sdk
        TimberBMS.i("Initializing sdk")

        // Pre init:
        application = app
        sdkConfig = config
        apiClient = createApiClientInternal()

        // Post init:
        PersistenceManager.initialize(app)
        registerNotificationChannel()
        setupTouchPoint()

        // initialize BeaconListenerService
        BeaconListenerService.getInstance().initialize()

        // persist beaconsmind config
        PersistenceManager.getInstance().beaconsmindConfig = config

        TimberBMS.d("Initialization completed")

        // fetch config after initialization, don't force it since we already restored
        // persisted config at the start of initialization
        RemoteDebuggingManager.fetchConfig(appContext, false)

        return true
    }

    /**
     * **For development purposes only.** Alternative initialization method.
     *
     * Kickstart sdk testing and integration by a single method call from your [Application] or [Activity] #onCreate.
     * Invokes [initialize] and sets the sdk in development mode.
     *
     * When initialized with this method the sdk requests permissions on it own by [BeaconsmindSdk.startExplicitMode].
     * It will ask for permissions on every Activity#onCreate until they are granted.
     *
     * > *For this to work*: Your app **must** use activity which implements [ActivityResultCaller].
     *
     * @param context any context available.
     * @param config beaconsmindSdk initialization config
     */
    @JvmStatic
    fun initializeDevelop(context: Context, config: BeaconsmindConfig): Boolean {

        // set debug log level
        minLogLevel = LogLevel.VERBOSE

        // initialize the SDK
        val app = context.applicationContext as Application
        val isFirstInitialization = initialize(app, config)

        TimberBMS.i("Initialized using explicit permissions mode. Use this for development builds only.")

        if (isFirstInitialization) {
            // explicit permissions mode
            startExplicitMode(context)
        }

        return isFirstInitialization
    }

    /**
     * Global application context, used by the sdk.
     *
     * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
     */
    @JvmStatic
    val appContext: Context
        @Throws(SdkNotInitializedException::class)
        get() = guardInitialization { application!! }

    /**
     * Beaconsmind config passed on initialization.
     *
     * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
     */
    @JvmStatic
    val config: BeaconsmindConfig
        @Throws(SdkNotInitializedException::class)
        get() = guardInitialization { sdkConfig }

    /**
     * Updates base url the SDK is connected to. Changes will not be immediately propagated through all SDK endpoints.
     * For it to take full effect call it before user login.
     *
     * *Keep in mind that your initialization config on new app startup will dismiss these changes.*
     *
     * Intended for internal use
     *
     * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
     */
    @ApiStatus.Internal
    @Throws(SdkNotInitializedException::class)
    @JvmStatic
    fun updateSuiteUri(suiteUri: String) = guardInitialization {
        sdkConfig = sdkConfig.copy(suiteUri = suiteUri)
        apiClient = createApiClientInternal()

        // persist beaconsmind config change
        PersistenceManager.getInstance().beaconsmindConfig =
            PersistenceManager.getInstance().beaconsmindConfig?.copy(suiteUri = suiteUri)
    }

    /**
     * Enable simulated beacon events.
     * Use on android emulators or if bluetooth is not available for testing.
     *
     * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
     *
     * @see BeaconListenerService.enableSimulator
     */
    @Throws(SdkNotInitializedException::class)
    @JvmStatic
    fun enableSimulator(enabled: Boolean) = guardInitialization {
        BeaconListenerService.getInstance().enableSimulator(enabled)
    }

    /**
     * ApiClient with Authorization headers added.
     * Use getter for each request since headers and base url can change.
     *
     * Use [BeaconsmindApiClient] for interaction with beaconsmind API.
     *
     * Avoid creating new ApiClients or initialising OpenApi endpoint apis without passing
     * this client in constructor call.
     *
     * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
     */
    @Throws(SdkNotInitializedException::class)
    @JvmStatic
    fun getApiClient(): ApiClient = guardInitialization {
        val accessToken = UserManager.getInstance().accessToken
        apiClient.setBearerToken(accessToken)
        apiClient.addDefaultHeader("Authorization", "Bearer $accessToken")
        return@guardInitialization apiClient
    }

    /**
     * Creates new api client. Not recommended since Authorization header is not added.
     * Use [getApiClient] instead.
     *
     * **Will be removed**
     *
     * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
     */
    @Throws(SdkNotInitializedException::class)
    @Deprecated("Will be removed",
        replaceWith = ReplaceWith("getApiClient()"),
        level = DeprecationLevel.ERROR)
    @JvmStatic
    fun createApiClient(): ApiClient = createApiClientInternal().also {
        TimberBMS.w("Creating new OkHttpClient is Deprecated. Use getApiClient instead")
    }

    /**
     * Creates notification for foreground service scanning.
     * Make sure to pass all the params to [BeaconsmindConfig.Builder] on initialization or notification will be invalid.
     *
     * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
     */
    @Throws(SdkNotInitializedException::class)
    @ApiStatus.Internal
    @JvmStatic
    fun createForegroundNotification(): Notification = guardInitialization {
        NotificationCompat.Builder(application!!, Constants.NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(sdkConfig.notificationBadge)
            .setContentTitle(sdkConfig.notificationTitle)
            .setContentText(sdkConfig.notificationText)
            .build()
    }

    /**
     * Creates base api client with suite url. Use [getApiClient] getter for all beaconsmind api requests.
     */
    private fun createApiClientInternal() = guardInitialization {
        return@guardInitialization BeaconsmindApiClient().apply {
            basePath = sdkConfig.suiteUri
            dateFormat = RFC3339DateFormat()//SimpleDateFormat(Constants.SUITE_DATE_FORMAT, Locale.ROOT)
            httpClient = OkHttpClient.Builder().addInterceptor(RequestDurationInterceptor()).build()
        }
    }


    /**
     * Tracks if the app is currently running in foreground or background. Returns *null* if not initialized.
     *
     * Tracking is done by [foregroundObserver]
     */
    internal val inBackground
        get() = foregroundObserver?.inBackground

    /**
     * App lifecycle observer
     */
    private var foregroundObserver: ForegroundObserver? = null

    /**
     * Setup for [TouchpointType.AppDownload] and [TouchpointType.AppOpen] touchpoints.
     */
    private fun setupTouchPoint() = guardInitialization {
        if (foregroundObserver == null) {
            val observer = ForegroundObserver()
            foregroundObserver = observer

            // Observing when the app is in foreground and sent AppOpen TouchPoint
            // ProcessLifecycleOwner provides a lifecycle for the whole application process
            ProcessLifecycleOwner.get().lifecycle.addObserver(observer)

            // Create unique identifier for AppDownload TouchPoint
            var uniqueID = PersistenceManager.getInstance().deviceUUID
            if (uniqueID == null) {
                uniqueID = UniqueDeviceID.getDeviceUUID()
                PersistenceManager.getInstance().deviceUUID = uniqueID
                TouchPointHelper.notifyAppDownloaded(uniqueID)
            }
        }
    }

    /**
     * Registers new [NotificationChannel] for persistent notification on API 26+
     * defined by [BeaconsmindConfig.notificationChannelName] and [Constants.NOTIFICATION_CHANNEL_ID]
     */
    private fun registerNotificationChannel() = guardInitialization {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
            sdkConfig.notificationBadge != BeaconsmindConfig.NO_NOTIFICATION_BADGE
        ) {
            // Beacon Listener Channel
            val beaconChannel = NotificationChannel(
                Constants.NOTIFICATION_CHANNEL_ID,
                sdkConfig.notificationChannelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )

            // Register the channel with the system
            val notificationManager =
                application!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(beaconChannel)
        }
    }
}


/**
 * Ensures that [BeaconsmindSdk] was initialized and executes code block passed to it.
 *
 * @param afterGuard code that will be executed after initialization check.
 *
 * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
 */
@Throws(SdkNotInitializedException::class)
internal fun <T : Any> guardInitialization(afterGuard: (() -> T)): T {
    return if (!BeaconsmindSdk.isInitialized) {
        throw SdkNotInitializedException()
    } else afterGuard.invoke()
}
