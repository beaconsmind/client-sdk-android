package com.beaconsmind.sdk

import com.beaconsmind.api.OffersApi
import com.beaconsmind.api.client.ApiException
import com.beaconsmind.api.models.OfferResponse
import com.beaconsmind.api.models.OfferStatusRequest
import com.beaconsmind.sdk.api.KotlinApiCallback
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.utils.UniqueEvictingQueue

/**
 * Main class for fetching offers and managing offer interaction.
 *
 * To obtain an instance call [OffersManager.getInstance] when BeaconsmindSdk was initialized.
 */
class OffersManager private constructor() {

    private val offersApi get() = OffersApi(BeaconsmindSdk.getApiClient())

    // Caching sent signals to avoid spamming the backend
    private val last100ReceivedOffers: UniqueEvictingQueue<Int> = UniqueEvictingQueue(100)
    private val last100ReadOffers: UniqueEvictingQueue<Int> = UniqueEvictingQueue(100)
    private val last100RedeemedOffers: UniqueEvictingQueue<Int> = UniqueEvictingQueue(100)

    /**
     * Loads all offers for currently logged-in user.
     */
    fun loadOffers(listener: OnResultListener<List<OfferResponse>>) {
        val userManager = UserManager.getInstance()
        if (!userManager.isAuthenticated) {
            listener.onError(Error.Unauthorized)
            return
        }

        try {
            offersApi.getOffersAsync(KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    listener.onSuccess(result)
                },
                _onFailure = { e, statusCode, responseHeaders ->
                    TimberBMS.e(e, "Error loading offers")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Error loading offers")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Loads offer details.
     *
     * The offer details can change after it is successfully redeemed so you would need to reload it afterwards.
     */
    fun loadOffer(offerId: Int, listener: OnResultListener<OfferResponse>) {
        val userManager = UserManager.getInstance()
        if (!userManager.isAuthenticated) {
            listener.onError(Error.Unauthorized)
            return
        }

        try {
            offersApi.getOfferAsync(offerId, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    listener.onSuccess(result)
                },
                _onFailure = { e, statusCode, responseHeaders ->
                    TimberBMS.e(e, "Error loading offer %d", offerId)
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Error loading offer %d", offerId)
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Mark offer as received overload.
     */
    fun markOfferAsReceived(offer: OfferPayload) {
        markOfferAsReceived(offer.offerId)
    }

    /**
     * Marks offer as received by the user.
     * Should be called on incoming notifications immediately after it was received.
     *
     * If using FirebaseMessaging it would be in your `FirebaseMessagingService#handleIntent()`
     *
     * ```kotlin
     *  override fun handleIntent(intent: Intent) {
     *      super.handleIntent(intent)
     *      // intercepts all incoming firebase messages
     *      val offer = OfferPayload.fromIntent(intent)
     *      if (offer != null) {
     *          OffersManager.getInstance().markOfferAsReceived(offer.offerId)
     *      }
     *  }
     * ```
     */
    fun markOfferAsReceived(offerId: Int) {
        // Avoid spamming
        if (last100ReceivedOffers.contains(offerId)) {
            TimberBMS.d("Skipping Offer $offerId received signal because already sent.")
            return
        }

        val userManager = UserManager.getInstance()
        if (!userManager.isAuthenticated) {
            TimberBMS.d("Skipping Offer $offerId received signal because not authenticated.")
            return
        }

        val request = OfferStatusRequest().apply {
            userId = userManager.jwtPayload!!.userId
            setOfferId(offerId)
        }

        try {
            offersApi.receivedAsync(request, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    last100ReceivedOffers.add(offerId)
                    TimberBMS.d("Sent Offer %d received signal.", offerId)
                },
                _onFailure = { e, statusCode, responseHeaders ->
                    TimberBMS.e(e, "Error sending Offer %d received signal", offerId)
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Error sending Offer %d received signal", offerId)
        }
    }

    /**
     * Mark offer as read overload
     */
    fun markOfferAsRead(offer: OfferPayload) {
        markOfferAsRead(offer.offerId)
    }

    /**
     * Marks offer as read by the user.
     * Should be called when offer details are opened
     * 
     * @param listener optional completion listener
     */
    @JvmOverloads
    fun markOfferAsRead(offerId: Int, listener: OnCompleteListener? = null) {
        // Avoid spamming
        if (last100ReadOffers.contains(offerId)) {
            TimberBMS.d("Skipping Offer $offerId read signal because already sent.")
            listener?.onSuccess()
            return
        }

        val userManager = UserManager.getInstance()
        if (!userManager.isAuthenticated) {
            TimberBMS.d("Skipping Offer $offerId read signal because not authenticated.")
            listener?.onError(Error.Unauthorized)
            return
        }

        val request = OfferStatusRequest().apply {
            userManager.jwtPayload!!.userId
            setOfferId(offerId)
        }

        try {
            offersApi.readAsync(request, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    last100ReadOffers.add(offerId)
                    TimberBMS.d("Sent Offer %d read signal.", offerId)
                    listener?.onSuccess()
                },
                _onFailure = { e, statusCode, responseHeaders ->
                    TimberBMS.e(e, "Error sending Offer %d read signal", offerId)
                    listener?.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Error sending Offer %d read signal", offerId)
            listener?.onError(Error.NetworkError(e))
        }
    }

    /**
     * Marks offer as clicked. 
     * Should be called when user clicks on Call-To-Action button
     * 
     * @param listener optional completion listener
     */
    @JvmOverloads
    fun markOfferClicked(offerId: Int, listener: OnCompleteListener? = null) {
        val userManager = UserManager.getInstance()
        if (!userManager.isAuthenticated) {
            TimberBMS.d("Skipping Offer $offerId clicked signal because not authenticated.")
            listener?.onError(Error.Unauthorized)
            return
        }

        val request = OfferStatusRequest()
            .userId(userManager.jwtPayload!!.userId)
            .offerId(offerId)

        try {
            offersApi.clickedAsync(request, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    TimberBMS.d("Sent Offer %d clicked signal.", offerId)
                    listener?.onSuccess()
                },
                _onFailure = { e, statusCode, responseHeaders ->
                    TimberBMS.e(e, "Failure sending Offer %d clicked signal", offerId)
                    listener?.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Exception sending Offer %d clicked signal", offerId)
            listener?.onError(Error.NetworkError(e))
        }
    }

    /**
     * Marks offer as redeemed.
     * Should be called when user explicitly redeems an offer using *redeem* button
     * (only for offers with `isRedeemable` flag set)
     *
     * When the offer is successfully redeemed you should re-[loadOffer], because it's details might have changed.
     *
     * @param listener callback invoked when server successfully redeems the offer.
     */
    fun markOfferRedeemed(offerId: Int, listener: OnCompleteListener) {
        // Avoid spamming
        if (last100RedeemedOffers.contains(offerId)) {
            TimberBMS.d("Skipping Offer $offerId redeemed signal because already sent.")
            listener.onSuccess()
            return
        }

        val userManager = UserManager.getInstance()
        if (!userManager.isAuthenticated) {
            TimberBMS.d("Skipping Offer $offerId redeemed signal because not authenticated.")
            listener.onError(Error.Unauthorized)
            return
        }

        val request = OfferStatusRequest().apply {
            setOfferId(offerId)
            userId = userManager.jwtPayload!!.userId
        }

        try {
            offersApi.redeemedAsync(request, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    last100RedeemedOffers.add(offerId)
                    TimberBMS.d("Sent Offer %d redeemed signal.", offerId)
                    listener.onSuccess()
                },
                _onFailure = { e, statusCode, responseHeaders ->
                    TimberBMS.e(e, "Error sending Offer %d redeemed signal", offerId)
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Error sending Offer %d redeemed signal", offerId)
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * [OffersManager] singleton holder.
     */
    companion object {
        @Volatile
        private var INSTANCE: OffersManager? = null

        /**
         * Returns [OffersManager] singleton instance.
         *
         * Ensure that you called [BeaconsmindSdk.initialize] before obtaining an instance
         *
         * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
         */
        @JvmStatic
        fun getInstance(): OffersManager = synchronized(this) {
            val checkInstanceAgain = INSTANCE
            if (checkInstanceAgain != null) {
                checkInstanceAgain
            } else {
                val created = OffersManager()
                INSTANCE = created
                created
            }
        }
    }
}
