package com.beaconsmind.sdk

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.RemoteException
import androidx.lifecycle.LiveData
import com.beaconsmind.api.PingApi
import com.beaconsmind.api.client.ApiClient
import com.beaconsmind.api.client.ApiException
import com.beaconsmind.api.models.EventModel
import com.beaconsmind.api.models.PingBeaconsRequest
import com.beaconsmind.api.models.StoreBeaconInfo
import com.beaconsmind.sdk.api.KotlinApiCallback
import com.beaconsmind.sdk.internal.*
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.models.BLEActivityState
import com.beaconsmind.sdk.models.BeaconContactsSummary
import com.beaconsmind.sdk.permissions.SdkPermissionsHelper
import com.beaconsmind.sdk.utils.Utils
import org.altbeacon.beacon.*
import org.altbeacon.bluetooth.BluetoothMedic
import org.jetbrains.annotations.ApiStatus
import java.util.*


/**
 * Main class for managing beacon BLE scanning. Used to explicitly start/stop beacon scanning.
 * Once beacon scanning starts successfully, BeaconsmindSdk will keep the service running.
 *
 * To obtain an instance call [BeaconListenerService.getInstance] when BeaconsmindSdk was initialized.
 */
class BeaconListenerService private constructor() {

    private val appContext get() = BeaconsmindSdk.appContext

    /**
     * Indicator of first time initialization.
     */
    private var isInitialized: Boolean = false

    /**
     * [org.altbeacon] beacon manager used for beacon scanning under the hood by our SDK
     */
    private val beaconManager: BeaconManager = BeaconManager.getInstanceForApplication(appContext)

    /**
     * Handler used for periodic beacon contacts push to the server
     *
     * @see pushEventsTask
     */
    private val handler: Handler = Handler(appContext.mainLooper)

    /**
     * Runnable which executes periodic beacon contacts push to the server.
     *
     * @see handler
     */
    private lateinit var pushEventsTask: Runnable

    /**
     * Manages beacons & beacon contacts list
     */
    private val beaconsmindBeaconManager = BeaconsmindBeaconManager()

    /**
     * Broadcast receiver which keeps track of BLE adapters state.
     */
    private val bleActivityReceiver: BLEActivityReceiver =
        BLEActivityReceiver()

    /**
     * Simulated BLE adapters state used with [enableSimulator]
     */
    private val simulatedBLEState: SimulatedBLEState = SimulatedBLEState()

    /**
     * Getter for current beacons summary. Iterable with all store beacons and it's contacts
     */
    val beaconsSummary: Iterable<BeaconContactsSummary> get() = beaconsmindBeaconManager.getBeaconsSummary()


    /* variable & getter fields */


    /**
     * In memory beacon listening state.
     * Indicates weather current app process successfully (re)started beacon ranging.
     *
     * For persisted state across process restarts see [PersistenceManager.isServiceActive]
     */
    var isRunning = false
        private set

    /**
     * Indicator if simulator was enabled. Set it via [enableSimulator].
     */
    val isSimulatorEnabled: Boolean
        get() = BeaconManager.getBeaconSimulator() != null

    /**
     * Exposed BLE adapter state via LiveData
     *
     * @see BLEActivityState
     */
    val bLEActivityLiveData: LiveData<BLEActivityState>
        get() = if (isSimulatorEnabled) {
            simulatedBLEState
        } else {
            bleActivityReceiver.activityLD
        }

    /**
     * Helper used to restart beacon scanning when passive mode is enabled
     */
    private var passiveBleScanHelper: PassiveBleScanHelper? = null

    /**
     * Internal BeaconListenerService initialization. Sets up tasks and starts beacon scanning if the scanning was persisted.
     *
     * Call from [BeaconsmindSdk.initialize] exactly once.
     */
    internal fun initialize() {
        if (isInitialized) {
            TimberBMS.e("BeaconListenerService: Multiple initializations not possible.")
            return
        }

        // setup tasks
        setupTasks()

        // background scanning for Android O+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            passiveBleScanHelper = PassiveBleScanHelper { restartForPassiveScan() }
            beaconManager.setIntentScanningStrategyEnabled(BeaconsmindSdk.config.usePassiveScanning)
        }

        // restore context after process death or restart
        if (UserManager.getInstance().isAuthenticated && PersistenceManager.getInstance().isServiceActive) {
            startListeningBeacons(null)
        }

        isInitialized = true
    }

    /**
     * Sets up periodic beacon contact pings to beaconsmind server.
     */
    private fun setupTasks() {
        pushEventsTask = Runnable {
            try {
                pushEvents()
            } catch (e: Exception) {
                TimberBMS.e(e, "Push Events Error")
            } finally {
                handler.postDelayed(pushEventsTask, Constants.BEACON_CONTACTS_PUSH_INTERVAL)
            }
        }
    }


    /**
     * Sets up notification and starts foreground service scanning.
     */
    private fun startForeground() {
        if (!beaconManager.isAnyConsumerBound) {
            val notification = BeaconsmindSdk.createForegroundNotification()
            beaconManager.enableForegroundServiceScanning(
                notification, Constants.NOTIFICATION_FG_SERVICE
            )
        } else {
            TimberBMS.w("Starting foreground service failed. Consumer is already bound")
        }
    }

    /**
     * Starts listening for beacons.
     * Make sure to invoke after the user is authenticated and has granted the required permissions.
     *
     * Started automagically if using [SdkPermissionsHelper] or [PermissionChecker] and user grants all permissions.
     *
     * Best practice is to call [startListeningBeacons] in your first screen's [Activity.onStart].
     *
     * Safe to be invoked multiple times.
     *
     * Called as a consequence of sdk initialization if the user is logged-in and scanning was already started
     *
     * @param onException Synchronous callback when starting fails. If null it will just log the error
     *
     * @see startListeningBeaconsUncaught for full implementation
     */
    fun startListeningBeacons(onException: ((StartListeningBeaconsException) -> Unit)?) {
        try {
            startListeningBeaconsUncaught()
        } catch (e: StartListeningBeaconsException) {
            onException?.invoke(e) ?: TimberBMS.e(e)
        }
    }

    /**
     * @throws StartListeningBeaconsException
     * @see startListeningBeacons(StartListeningBeaconsException)
     */
    @Deprecated(
        "Throws runtime exception since SDK version 1.11.0",
        ReplaceWith("startListeningBeacons {  } or startListeningBeaconsUncaught()")
    )
    @Throws(StartListeningBeaconsException::class)
    fun startListeningBeacons() = startListeningBeaconsUncaught()

    /**
     * Starts beacon monitoring and ranging. Persists beacon scanning across restarts.
     *
     * @throws StartListeningBeaconsException
     * @see startListeningBeacons
     */
    @Throws(StartListeningBeaconsException::class)
    fun startListeningBeaconsUncaught() {
        if (!UserManager.getInstance().isAuthenticated) {
            throw UserNotAuthenticatedException()
        }

        // check if scanning already started
        if (isRunning) {
            TimberBMS.i("Ignoring startListeningBeacons command, beacon scanning already started.")
            return
        }

        // persist beacon listening started.
        PersistenceManager.getInstance().isServiceActive = true

        // check permissions before start
        val missingPermissions = SdkPermissionsHelper.getMissingPermissions(appContext)
        if (missingPermissions.isNotEmpty()) {
            if (!SdkPermissionsHelper.canStartBeaconService(appContext)) {
                throw MissingPermissionsException(missingPermissions)
            } else {
                TimberBMS.i("Starting service with missing permissions: %s",
                    missingPermissions.joinToString())
            }
        }

        // enable bluetooth medic for failure recovery
        // works only on versions prior to Android 13 (API 33)
        if (Build.VERSION.SDK_INT < 33) {
            BluetoothMedic.getInstance().legacyEnablePowerCycleOnFailures(appContext)
        }

        // scanning mode setup
        if (!BeaconsmindSdk.config.usePassiveScanning) {
            TimberBMS.i("Using foreground scanning")
            startForeground()
        } else {
            TimberBMS.i("Using passive scanning")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                passiveBleScanHelper?.onStartListening(bLEActivityLiveData)
            }
        }

        // start listening for beacons
        try {
            // setup beacon manager
            val iBeaconParser = BeaconParser().setBeaconLayout(Constants.BEACON_IBEACON_LAYOUT)
            beaconManager.beaconParsers.clear()
            beaconManager.beaconParsers.add(iBeaconParser)

            beaconManager.isRegionStatePersistenceEnabled = false
            beaconManager.removeAllMonitorNotifiers()
            beaconManager.removeAllRangeNotifiers()
            beaconManager.addMonitorNotifier(BMSMonitorNotifier())
            beaconManager.addRangeNotifier(BMSRangeNotifier())

            // start ranging with stored beacons
            setupMonitoring()

            // setup tasks
            FetchBeaconsJobScheduler.scheduleNext(appContext)
            handler.removeCallbacks(pushEventsTask)
            pushEventsTask.run()

            updateFrequency()

            // set running flags
            isRunning = true

            // register BLE activity state broadcast receiver
            bleActivityReceiver.register(BeaconsmindSdk.appContext)
        } catch (ex: Exception) {
            passiveBleScanHelper?.onStopListening()
            throw StartListeningBeaconsException("Unknown exception while starting beacon service", ex)
        }
    }


    /**
     * Stops beacon monitoring and ranging.
     *
     * Calling this the BeaconListenerService will not auto restart and will have to be enabled programmatically again.
     */
    fun stopListeningBeacons() {
        TimberBMS.i("Stopping broadcasting")

        // remove callbacks and shutdown
        FetchBeaconsJobScheduler.cancelJobs(appContext)
        handler.removeCallbacks(pushEventsTask)

        // stop the foreground notification service
        for (r in beaconManager.monitoredRegions) beaconManager.stopMonitoring(r)
        for (r in beaconManager.rangedRegions) beaconManager.stopRangingBeacons(r)

        beaconManager.removeAllMonitorNotifiers()
        beaconManager.removeAllRangeNotifiers()

        if (!beaconManager.isAnyConsumerBound) {
            beaconManager.disableForegroundServiceScanning()
            beaconManager.setIntentScanningStrategyEnabled(false)
        }

        beaconManager.shutdownIfIdle()

        // set running flags to false
        isRunning = false
        PersistenceManager.getInstance().isServiceActive = false

        // unregister broadcast receiver
        bleActivityReceiver.unregister(BeaconsmindSdk.appContext)
        passiveBleScanHelper?.onStopListening()
    }

    /**
     * Stops beacon listening and clears cached beacons in memory.
     */
    @ApiStatus.Internal
    internal fun logout() {
        stopListeningBeacons()
        beaconsmindBeaconManager.clearSessionInMemory()
    }

    /**
     *  Workaround for a know bug in altbeacon-lib v2.19.4.
     *  Restarts beacon scanning.
     *
     *  @see [PassiveBleScanHelper]
     * */
    private fun restartForPassiveScan() {
        TimberBMS.d("Restarting passive beacon scan")

        // remove all beacons
        for (r in beaconManager.monitoredRegions) beaconManager.stopMonitoring(r)
        for (r in beaconManager.rangedRegions) beaconManager.stopRangingBeacons(r)

        // start monitoring again
        setupMonitoring()
    }


    /**
     *  Sets new beacon list and starts monitoring them. Removes stale beacons.
     * */
    // TODO: lookup cleaner way to set received beacons without exposing
    @ApiStatus.Internal
    @ApiStatus.Experimental
    internal fun setBeacons(receivedBeacons: List<StoreBeaconInfo>) {
        beaconsmindBeaconManager.setBeacons(receivedBeacons)
        setupMonitoring()
    }

    /**
     * Push beacon contacts to server request.
     */
    private fun pushEvents() {
        val eventsToSend = beaconsmindBeaconManager.popEvents() ?: return

        val request = PingBeaconsRequest()
            .deviceOs(Utils.deviceInfo)
            .userId(UserManager.getInstance().jwtPayload!!.userId)
            .events(eventsToSend)

        try {
            val pingApi = PingApi(getUserApiClient())
            pingApi.pingMultipleBeaconsAsync(request, KotlinApiCallback(
                _onSuccess = { _, _, _ ->
                    TimberBMS.d("Sent %d beacons to suite", eventsToSend.size)
                },
                _onFailure = { e, _, _ -> TimberBMS.e(e, "Error while sending to suite") }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Error while sending to suite")
        }
    }

    private fun getUserApiClient(): ApiClient {
        val userManager = UserManager.getInstance()
        if (!userManager.isAuthenticated) {
            throw RuntimeException("Cannot broadcast when user is not authenticated.")
        }
        return BeaconsmindSdk.getApiClient()
    }

    /**
     * Starts monitoring new beacons and stops monitoring stale beacons.
     */
    private fun setupMonitoring() {
        // beacon uuids received from the suite (to be monitored)
        val receivedBeacons = beaconsmindBeaconManager.mapNotNull { it.uuid?.lowercase() }.toSet()

        TimberBMS.d("Setting up monitoring for %d regions", receivedBeacons.size)

        // get already monitored beacon's uuids
        val oldMonitoredBeacons =
            beaconManager.monitoredRegions.map { it.uniqueId.lowercase() }.toMutableSet()

        // start monitoring new beacons from the suite
        for (uuid in receivedBeacons) {
            if (!oldMonitoredBeacons.contains(uuid)) {
                TimberBMS.d("Starting to monitor %s", uuid)
                beaconManager.startMonitoring(Region(uuid, Identifier.parse(uuid), null, null))
            } else {
                TimberBMS.d("%s is already being monitored", uuid)
            }

            // remove receivedBeacon from old monitored set
            oldMonitoredBeacons.remove(uuid)
        }

        // stop monitoring beacons not received from suite
        for (uuid in oldMonitoredBeacons) {
            TimberBMS.d("Stopping monitoring for %s", uuid)
            beaconManager.stopMonitoring(Region(uuid, Identifier.parse(uuid), null, null))
        }
    }

    /**
     * Updates BLE beacon scanning frequency.
     */
    private fun updateFrequency() {
        if (beaconManager.rangedRegions.isNotEmpty()) {
            TimberBMS.d("Increasing frequency for ranging")
            beaconManager.foregroundBetweenScanPeriod = Constants.BEACON_RANGING_FREQUENCY
            beaconManager.backgroundBetweenScanPeriod =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && BeaconsmindSdk.config.usePassiveScanning) {
                    /* 15 min background limit */ 15 * 60 * 1000
                } else Constants.BEACON_RANGING_FREQUENCY
        } else {
            TimberBMS.d("Decreasing frequency for monitoring")
            beaconManager.foregroundBetweenScanPeriod = Constants.BEACON_MONITORING_FREQUENCY
            beaconManager.backgroundBetweenScanPeriod =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && BeaconsmindSdk.config.usePassiveScanning) {
                    /* 15 min background limit*/ 15 * 60 * 1000
                } else Constants.BEACON_MONITORING_FREQUENCY
        }
        beaconManager.foregroundScanPeriod = Constants.BEACON_SCAN_PERIOD
        beaconManager.backgroundScanPeriod = Constants.BEACON_SCAN_PERIOD
        try {
            beaconManager.updateScanPeriods()
        } catch (ex: RemoteException) {
            TimberBMS.e(ex, "Error updating scan periods")
        }
    }

    /**
     * Enables simulated beacon contacts. Used for debugging when bluetooth functionality is not available.
     *
     * Simulates beacon contacts with beacons downloaded from beaconsmind backend.
     */
    fun enableSimulator(enabled: Boolean) {
        if (enabled != isSimulatorEnabled) {
            if (enabled) {
                TimberBMS.i("Enabling beacon simulator!")
                BeaconManager.setBeaconSimulator(beaconsmindBeaconManager.beaconSimulator)
            } else {
                TimberBMS.i("Disabling beacon simulator!")
                BeaconManager.setBeaconSimulator(null)
            }
        }
    }

    /**
     * Tracks beacon contacts and updates them to [beaconsmindBeaconManager].
     */
    private inner class BMSRangeNotifier : RangeNotifier {

        /**
         * Callback which returns [rangedBeacons] contacts. Sets beacon contacts to [beaconsmindBeaconManager]
         */
        override fun didRangeBeaconsInRegion(rangedBeacons: Collection<Beacon>, region: Region) {
            val currentEvents: MutableList<EventModel> = ArrayList()
            for (rangedBeacon in rangedBeacons) {
                val uuid = rangedBeacon.id1?.toString() ?: ""
                val major = rangedBeacon.id2?.toString() ?: ""
                val minor = rangedBeacon.id3?.toString() ?: ""

                val rssi = rangedBeacon.getValidRssiOrNull()
                val beaconIsKnown = isKnownBeacon(uuid, major, minor)

                if (beaconIsKnown && rssi != null) {
                    TimberBMS.d("Ranging beacon: %s:%s:%s", uuid, major, minor)
                    currentEvents.add(
                        EventModel()
                            .event(null)
                            .rssi(rssi)
                            .distance(rangedBeacon.getValidDistanceOrNull())
                            .timestamp(System.currentTimeMillis())
                            .uuid(uuid)
                            .major(major)
                            .minor(minor)
                    )
                } else if (!beaconIsKnown) {
                    // we don't care about unknown beacons
                } else {
                    TimberBMS
                        .d("Fantom beacon (RSSI invalid): %s:%s:%s", uuid, major, minor)
                }
            }

            beaconsmindBeaconManager.addBeaconEvents(currentEvents)
        }

        /**
         * Determines if we are tracking this beacon to filter false positives.
         */
        private fun isKnownBeacon(uuid: String, major: String, minor: String): Boolean {
            return beaconsmindBeaconManager.any { b -> uuid == b.uuid && major == b.major && minor == b.minor }
        }

        /**
         * Extension function on [Beacon] receiver. Returns rssi if it's valid
         */
        private fun Beacon.getValidRssiOrNull(): Double? {
            return if (rssi >= -200 && rssi < 0) {
                rssi.toDouble()
            } else null
        }

        /**
         * Extension function on [Beacon] receiver. Returns distance from beacon if possible.
         *
         * @receiver [Beacon]
         */
        private fun Beacon.getValidDistanceOrNull(): Double? {
            var distance: Double? = distance
            if (distance!! <= 0 || distance > 10000) {
                distance = Utils.calcDistance(rssi.toDouble())
            }
            if (distance <= 0 || distance > 10000) {
                distance = null
            }
            return distance
        }
    }

    /**
     * Monitors regions we enter and stars ranging them if we are tracking any beacon from that region.
     */
    private inner class BMSMonitorNotifier : MonitorNotifier {
        override fun didEnterRegion(region: Region) {
            TimberBMS.d("Got a didEnterRegion call %s", region.uniqueId)
            beaconManager.startRangingBeacons(region)
            updateFrequency()
        }

        override fun didExitRegion(region: Region) {
            TimberBMS.d("Got a didExitRegion call %s", region.uniqueId)
            beaconManager.stopRangingBeacons(region)
            updateFrequency()
        }

        override fun didDetermineStateForRegion(state: Int, region: Region) {
            TimberBMS.d(
                "Got a didDetermineStateForRegion call for %s (state:%d)",
                region.uniqueId, state
            )
        }
    }

    /**
     * [BeaconListenerService] singleton holder.
     */
    companion object {
        @Volatile
        private var INSTANCE: BeaconListenerService? = null

        /**
         * Returns [BeaconListenerService] singleton instance.
         *
         * Ensure that you called [BeaconsmindSdk.initialize] before obtaining an instance
         *
         * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
         */
        @Throws(SdkNotInitializedException::class)
        @JvmStatic
        fun getInstance(): BeaconListenerService = guardInitialization {
            synchronized(this) {
                val checkInstanceAgain = INSTANCE
                if (checkInstanceAgain != null) {
                    checkInstanceAgain
                } else {
                    val created = BeaconListenerService()
                    INSTANCE = created
                    created
                }
            }
        }

        /**
         * Returns true if [BeaconListenerService] already got start command but couldn't successfully start.
         *
         * Safe for callers without sdk initialization.
         */
        @JvmStatic
        fun shouldRestartService(context: Context): Boolean {
            return BeaconsmindSdk.isInitialized &&
                    !BeaconListenerService.getInstance().isRunning && // service not active,
                    PersistenceManager.getInstanceWithContext(context).isServiceActive // but was started sometime
        }
    }
}

/**
 *  Helper class for managing current beacons and beacon events
 *
 *  Keeps track of beacons downloaded from the suite, current events and  beacon summary data.
 *
 * @constructor Restores beacons persisted from previous store beacons fetch to keep it after process
 * death. This enables monitoring if started without internet connection.
 * */
private class BeaconsmindBeaconManager /*keep for documentation*/ constructor() :
    Iterable<StoreBeaconInfo> {

    /**
     * Delegated beacons list.
     */
    private val _internalBeaconsList = Collections.synchronizedList(ArrayList<StoreBeaconInfo>())

    /**
     * Unconsumed events list.
     */
    private val _internalEventsList: MutableList<EventModel> =
        Collections.synchronizedList(ArrayList())

    private val beaconsSummary = BeaconContactsSummaryList()

    val beaconSimulator = RandomBeaconSimulator()

    init {
        // restore persisted beacons
        val cachedBeacons = PersistenceManager.getInstance().storeBeacons

        if (cachedBeacons != null) {
            _setBeaconsInternal(cachedBeacons)
            TimberBMS.d("Restoring ${cachedBeacons.size} beacons from PersistenceManager")
        }
    }

    /**
     * Sets beacons to delegated beacons list, beacon summary and beacon simulator.
     */
    private fun _setBeaconsInternal(beacons: List<StoreBeaconInfo>) {
        _internalBeaconsList.clear()
        _internalBeaconsList.addAll(beacons)
        beaconSimulator.setBeacons(beacons)
        for (beacon in beacons) beaconsSummary.addBeacon(beacon)
    }

    /**
     * Sets store beacons for tracking and persists them.
     * */
    fun setBeacons(beacons: List<StoreBeaconInfo>) {
        PersistenceManager.getInstance().storeBeacons = beacons
        _setBeaconsInternal(beacons)
    }

    /**
     * Keeps track of beacon events.
     * */
    fun addBeaconEvents(currentEvents: List<EventModel>) {
        _internalEventsList.addAll(currentEvents)
        beaconsSummary.addBeaconContacts(currentEvents)
    }

    /**
     * Consumes internal events list. Make sure to use it or else it's gone.
     * */
    fun popEvents(): List<EventModel>? {
        if (_internalEventsList.isEmpty()) return null

        val currentEvents = ArrayList(_internalEventsList)
        _internalEventsList.clear()

        return currentEvents
    }

    /**
     *  Exposes beacon summary as a collection.
     * */
    fun getBeaconsSummary() = beaconsSummary.asIterable()

    /**
     * Clears all beacon collections from cached memory.
     * */
    fun clearSessionInMemory() {
        _internalBeaconsList.clear()
        beaconSimulator.setBeacons(emptyList())
        beaconsSummary.clear()
    }

    /**
     * Returns Iterable store beacon list from delegate [_internalBeaconsList]
     * */
    override fun iterator(): Iterator<StoreBeaconInfo> = _internalBeaconsList.iterator()
}

/**
 * Simulates BLE adapters enabled state.
 * Used for debugging with [org.altbeacon.beacon.BeaconManager.setBeaconSimulator]
 */
private class SimulatedBLEState : LiveData<BLEActivityState>(BLEActivityState.ENABLED)