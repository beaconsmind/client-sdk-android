package com.beaconsmind.sdk;

/**
 * Request callbacks used for interaction with Beaconsmind API via SDK managers.
 *
 * Used with requests that don't return data. For requests with data check {@link OnResultListener}
 *
 * @see UserManager
 * @see AvatarManager
 * @see OffersManager
 */
public interface OnCompleteListener {
    /**
     * Invoked when request completes successfully.
     */
    void onSuccess();

    /**
     * Invoked when request error occurs.
     *
     * @param error wrapped network error
     */
    void onError(Error error);
}
