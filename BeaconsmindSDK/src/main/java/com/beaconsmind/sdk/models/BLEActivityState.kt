package com.beaconsmind.sdk.models

/**
 * Enumerates different states device Bluetooth and Location adapters can be in. These adapters are necessary for BLE scanning to work.
 */
enum class BLEActivityState {
    /**
     * Device Location adapter is turned off.
     */
    LOCATION_OFF,

    /**
     * Device Bluetooth adapter is turned off.
     */
    BT_OFF,

    /**
     * Both Bluetooth and Location adapters are off.
     */
    DISABLED,

    /**
     * Bluetooth and Location adapters are turned on.
     */
    ENABLED
}