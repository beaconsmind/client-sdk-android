package com.beaconsmind.sdk.models

import androidx.annotation.Nullable
import com.beaconsmind.api.models.EventModel
import com.beaconsmind.api.models.StoreBeaconInfo
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.utils.EvictingQueue
import org.jetbrains.annotations.ApiStatus

/**
 * Interval in which we calculate frequency.
 */
private const val FREQUENCY_PERIOD = 30

/**
 * Helper model used to describe beacon's contacts summary.
 * */
class BeaconContactsSummary(beacon: StoreBeaconInfo) {

    /**
     * Store's name
     */
    val store: String = beacon.store!!

    /**
     * Beacon's name
     * */
    val name: String = beacon.name!!

    /**
     * Beacon's UUID value
     */
    val uuid: String = beacon.uuid!!

    /**
     * Beacon's major UUID value
     */
    val major: String = beacon.major!!

    /**
     * Beacon minor UUID value
     */
    val minor: String = beacon.minor!!

    /**
     * Helper queue for keeping track of beacon contacts
     */
    private val last100Contacts: EvictingQueue<EventModel> = EvictingQueue(100)

    /**
     * Beacon's average signal strength (based on last contacts)
     */
    var averageRssi: Double? = null
        private set

    /**
     * Beacon's current signal strength (based on single last contact)
     */
    var currentRssi: Double? = null
        private set

    /**
     * Contact count
     */
    var timesContacted: Int = 0
        private set

    /**
     * Indicator if beacon is in range
     */
    val isInRange: Boolean
        get() = System.currentTimeMillis() - lastContact < 20 * 1000

    /**
     * Last beacon contact timestamp.
     * @return null if no beacon contacts
     */
    @Nullable
    fun getLastContact(): Long? = if (last100Contacts.isNotEmpty()) {
        last100Contacts.last?.timestamp
    } else null

    /**
     * Contact frequency per minute
     */
    fun getFrequency(): Int {
        val someTimeAgo = lastContact - FREQUENCY_PERIOD * 1000
        if (someTimeAgo < 0) return 0

        val contactsInPeriod = last100Contacts.filter { it.timestamp!! >=  someTimeAgo }.size
        return ((contactsInPeriod.toFloat() / FREQUENCY_PERIOD) * 60f).toInt()
    }

    /**
     * Add beacon contact internally. Used by SDK only.
     */
    @ApiStatus.Internal
    internal fun addNewBeaconContact(event: EventModel) {
        if (event.timestamp!! < lastContact) {
            TimberBMS.e("Contacted beacon is older than the last one")
            return
        }
        if (uuid != event.uuid || major != event.major || minor != event.minor) {
            TimberBMS.e("Contacted beacon does not belong to this bacon summary")
            return
        }

        val beaconRssi = event.rssi!!
        currentRssi = beaconRssi
        if (last100Contacts.size == 0 || averageRssi == null) {
            averageRssi = currentRssi
        } else {
            averageRssi =
                (averageRssi!! * last100Contacts.size + beaconRssi) / (last100Contacts.size + 1)
        }

        last100Contacts.add(event)
        timesContacted++
    }

    /**
    * Extension util for non-null lastContact
    */
    private val BeaconContactsSummary.lastContact: Long
        get() = getLastContact() ?: 0
}