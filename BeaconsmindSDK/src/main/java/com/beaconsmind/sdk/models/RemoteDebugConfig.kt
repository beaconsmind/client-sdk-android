package com.beaconsmind.sdk.models

import com.beaconsmind.api.models.AppConfigurationResponse
import com.beaconsmind.api.models.LoggingLevel

/**
 * Internal model for persisting remote config response [AppConfigurationResponse]
 *
 * @property loggingKey BugFender remote logging API key. null if disabled.
 * @property logLevel LogLevel as represented by beaconsmindSDK / android OS log priority. Use [getLogLevel] getter.
 * @property metricsIntervalSeconds Interval in seconds between diagnostics request. null if disabled
 */
internal data class RemoteDebugConfig(
    val loggingKey: String?,
    private val logLevel: LogLevel?,
    val metricsIntervalSeconds: Int?,
) {
    /**
     * Safe getter for [logLevel] in case of nullable response/persisted cache.
     */
    fun getLogLevel() = logLevel ?: LogLevel.SILENT

    /**
     * Helper constructor for wrapping API response.
     */
    constructor(response: AppConfigurationResponse) : this(
        response.bugFenderKey,
        response.loggingLevel.toLogLevel(),
        response.metricsIntervalSeconds
    )
}

/**
 * Translates remote logging level to [LogLevel] used by the SDK.
 *
 * @receiver [LoggingLevel]
 */
private fun LoggingLevel?.toLogLevel(): LogLevel {
    return when (this) {
        LoggingLevel.Verbose -> LogLevel.VERBOSE
        LoggingLevel.Debug -> LogLevel.DEBUG
        LoggingLevel.Info -> LogLevel.INFO
        LoggingLevel.Warning -> LogLevel.WARNING
        LoggingLevel.Error -> LogLevel.ERROR
        else -> LogLevel.SILENT
    }
}