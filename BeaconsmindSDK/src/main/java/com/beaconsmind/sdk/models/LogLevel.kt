package com.beaconsmind.sdk.models

import android.util.Log
import com.beaconsmind.sdk.BeaconsmindSdk


/**
 * Global SDK log level definitions. Use [BeaconsmindSdk.minLogLevel] to toggle SDK logging.
 */
enum class LogLevel(val priority: Int, internal val value: String) {
    /**
     * Altbeacon debug logs go here
     */
    VERBOSE(Log.VERBOSE, "verbose"),

    /**
     *  Any developer-level debug info, repeating events, e.g.:
     *  - monitoring/location callbacks,
     *  - networking success results,
     * */
    DEBUG(Log.DEBUG, "debug"),

    /**
     * Bigger events/steps in the SDK lifecycle, e.g.
     * - starting/stopping services,
     * - initializing SDK,
     * - one-time events.
     */
    INFO(Log.INFO, "info"),

    /** Allowed, but not-optimal, inconsistent state, e.g.:
     * - trying to start monitoring without permissions.
     * */
    WARNING(Log.WARN, "warning"),

    /**
     * Not allowed state (any error).
     */
    ERROR(Log.ERROR, "error"),

    /**
     * No logging at all.
     * */
    SILENT(Int.MAX_VALUE, "silent");

    companion object {
        @JvmStatic
        fun fromValue(value: String) = when (value) {
            "verbose" -> LogLevel.VERBOSE
            "debug" -> LogLevel.DEBUG
            "info" -> LogLevel.INFO
            "warning" -> LogLevel.WARNING
            "error" -> LogLevel.ERROR
            else -> LogLevel.SILENT
        }
    }
}