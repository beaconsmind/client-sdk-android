package com.beaconsmind.sdk.api

import com.beaconsmind.api.ImageApi
import com.beaconsmind.api.client.ApiClient
import com.beaconsmind.api.models.ImageUploadSettings
import okhttp3.Headers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File


/**
 * Used for all requests with beaconsmind api. Contains fix for openApi generator issue
 * affecting our [ImageApi.createAsync] endpoint.
 */
internal class BeaconsmindApiClient : ApiClient() {

    /**
     * [ImageUploadSettings] issue workaround
     *
     * See [issue](https://github.com/OpenAPITools/openapi-generator/issues/12036)
     */
    override fun buildRequestBodyMultipart(formParams: MutableMap<String, Any>?): RequestBody {
        val mpBuilder: MultipartBody.Builder = MultipartBody.Builder().setType(MultipartBody.FORM)
        for ((key, value) in formParams!!) {
            when (value) {
                is File -> {
                    val file = value
                    val mediaType = guessContentTypeFromFile(file).toMediaTypeOrNull()
                    mpBuilder.addPart(
                        Headers.headersOf(
                            "Content-Disposition",
                            "form-data; name=\"" + key + "\"; filename=\"" + file.name + "\""
                        ),
                        file.asRequestBody(mediaType)
                    )
                }
                is ImageUploadSettings -> {
                    mpBuilder.addPart(
                        Headers.headersOf(
                            "Content-Disposition",
                            "form-data; name=\"$key\"; filename=\"blob\""
                        ),
                        json.serialize(value).toRequestBody("application/json".toMediaTypeOrNull())
                    )
                }
                else -> {
                    val partHeaders: Headers = Headers.headersOf(
                        "Content-Disposition",
                        "form-data; name=\"$key\""
                    )
                    mpBuilder.addPart(partHeaders, parameterToString(value).toRequestBody())
                }
            }
        }
        return mpBuilder.build()
    }
}