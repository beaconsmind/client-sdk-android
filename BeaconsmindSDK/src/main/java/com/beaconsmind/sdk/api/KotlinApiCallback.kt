package com.beaconsmind.sdk.api

import com.beaconsmind.api.client.ApiCallback
import com.beaconsmind.api.client.ApiException

/**
 * Adapter for Beaconsmind API network request callbacks.
 *
 * @see ApiCallback
 *
 * @constructor
 * [ApiCallback] adapter for kotlin.
 *
 * @param _onSuccess invoked when call was successful
 * @param _onFailure invoked when call fails
 */
internal open class KotlinApiCallback<T : Any?>(
    private val _onSuccess: (result: T, statusCode: Int, responseHeaders: MutableMap<String, MutableList<String>>) -> Unit,
    private val _onFailure: (e: ApiException, statusCode: Int, responseHeaders: MutableMap<String, MutableList<String>>) -> Unit,
    private val _onUploadProgress: (bytesWritten: Long, contentLength: Long, done: Boolean) -> Unit = { _, _, _ -> Unit },
    private val _onDownloadProgress: (bytesRead: Long, contentLength: Long, done: Boolean) -> Unit = { _, _, _ -> Unit },
) : ApiCallback<T> {

    /**
     * Dispatch failure
     */
    override fun onFailure(
        e: ApiException,
        statusCode: Int,
        responseHeaders: MutableMap<String, MutableList<String>>?,
    ) = _onFailure(e, statusCode, responseHeaders ?: mutableMapOf())

    /**
     * Dispatch success
     */
    override fun onSuccess(
        result: T,
        statusCode: Int,
        responseHeaders: MutableMap<String, MutableList<String>>?,
    ) = _onSuccess(result, statusCode, responseHeaders ?: mutableMapOf())

    /**
     * Dispatch uploadProgress
     */
    override fun onUploadProgress(bytesWritten: Long, contentLength: Long, done: Boolean) =
        _onUploadProgress(bytesWritten, contentLength, done)


    /**
     * Dispatch downloadProgress
     */
    override fun onDownloadProgress(bytesRead: Long, contentLength: Long, done: Boolean) =
        _onDownloadProgress(bytesRead, contentLength, done)
}