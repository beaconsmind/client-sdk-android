package com.beaconsmind.sdk

import com.beaconsmind.sdk.permissions.SdkPermissionsHelper

/**
 * *Base* SDK related runtime exceptions class
 */
sealed class BeaconsmindException : RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable) : super(cause)
}


/**
 * Runtime exception thrown if [BeaconsmindSdk.initialize] was not called before accessing the SDK.
 *
 * @see BeaconsmindSdk.initialize
 */
class SdkNotInitializedException :
    BeaconsmindException("The Beaconsmind Sdk is not initialized, try: BeaconsmindSdk.initialize(..)")


/**
 * Runtime exception thrown if [BeaconsmindConfig.Builder] was not properly set
 */
class MissingBuilderParamException(param: String) :
    BeaconsmindException("Required parameter $param is not set.")


/**
 * Runtime exception *base* class when [BeaconListenerService.startListeningBeacons] call fails
 */
open class StartListeningBeaconsException(reason: String, cause: Throwable? = null) :
    BeaconsmindException("StartListeningBeacons failed. $reason", cause)

/**
 * Runtime exception thrown when [BeaconListenerService.startListeningBeacons] is called without proper BLE permissions.
 *
 * @see SdkPermissionsHelper.getMissingPermissions
 */
class MissingPermissionsException(
    val missingPermissions: Collection<String>,
    cause: Throwable? = null,
) : StartListeningBeaconsException(
    reason = "Missing permissions: ${missingPermissions.joinToString()}",
    cause = cause
)

/**
 * Runtime exception thrown when [BeaconListenerService.startListeningBeacons] is called without user authentication
 *
 * @see UserManager.isAuthenticated
 */
class UserNotAuthenticatedException :
    StartListeningBeaconsException(reason = "User is not authenticated.")