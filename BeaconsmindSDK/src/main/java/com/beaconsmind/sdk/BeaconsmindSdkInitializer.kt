package com.beaconsmind.sdk

import android.app.Application
import android.content.Context
import androidx.annotation.CallSuper
import androidx.startup.Initializer
import androidx.work.WorkManagerInitializer
import com.beaconsmind.sdk.logging.TimberBMS
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield

/**
 * Used for initializing [BeaconsmindSdk] from persisted BeaconsmindConfig using [androidx.startup].
 * **Disabled** by default. For android native SDK usage call [BeaconsmindSdk.initialize] in your [Application.onCreate].
 *
 * If initialization from Application#onCreate is not possible (non-native development; eg. Flutter, ReactNative..),
 * use [BeaconsmindSdkInitializer] and add the lines below to your application's manifest for continuous BeaconListenerService startup
 *
 * ```xml
 * <!-- Example AndroidManifest.xml -->
 * <application>
 *      <!-- add this provider to your manifest -->
 *      <provider
 *          android:name="androidx.startup.InitializationProvider"
 *          android:authorities="${applicationId}.androidx-startup"
 *          android:exported="false"
 *          tools:node="merge">
 *          <meta-data
 *              android:name="com.beaconsmind.sdk.BeaconsmindSdkInitializer"
 *              android:value="androidx.startup" />
 *      </provider>
 * </application>
 * ```
 *
 * Also, add androidx-startup to your *build.gradle* dependencies
 * ```gradle
 * // build.gradle dependencies
 * dependencies {
 *      // add androidx.startup dependency
 *      implementation "androidx.startup:startup-runtime:1.1.1
 * }
 * ```
 */
open class BeaconsmindSdkInitializer : Initializer<BeaconsmindSdk> {

    /**
     * Initializes BeaconsmindSdk with context that was persisted from last initialization.
     * You still need, at some point in your app, initialize the SDK for the first time using [BeaconsmindConfig]
     *
     * Note that this is called before your Application#onCreate so beaconsmind sdk logs might not
     * be visible before you set [BeaconsmindSdk.minLogLevel].
     *
     * If you want to enable logger on time, override this class and do it before calling `super.create`
     */
    @CallSuper
    override fun create(context: Context): BeaconsmindSdk {
        MainScope().launch {
            yield()
            // yield on Main thread and restore the sdk when thread is freed
            // this gives way for the app to initialize the instance before restoring it from storage.
            restoreBeaconsmindInstance(context)
        }

        return BeaconsmindSdk
    }

    /**
     * Initializes BeaconsmindSdk from persisted [BeaconsmindConfig].
     */
    protected fun restoreBeaconsmindInstance(context: Context): BeaconsmindSdk {
        val config = PersistenceManager.getInstanceWithContext(context).beaconsmindConfig
        if (config != null && !BeaconsmindSdk.isInitialized) {
            BeaconsmindSdk.initialize(context.applicationContext as Application, config)
            TimberBMS.i("BeaconsmindSdk instance was initialized by BeaconsmindSdkInitializer")
        }
        return BeaconsmindSdk
    }

    /**
     * No dependencies except for [WorkManager] for jobs used by the SDK.
     */
    override fun dependencies(): List<Class<out Initializer<*>>> =
        listOf(WorkManagerInitializer::class.java)
}