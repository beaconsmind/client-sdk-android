package com.beaconsmind.sdk

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.beaconsmind.sdk.logging.TimberBMS

/**
 * This is **not** necessary for beacon scanning startup.
 *
 * BeaconsmindSdk has to be initialized for beacon scanning to work.
 * For proper beacon scanning startup make sure to call [BeaconsmindSdk.initialize] in you [Application.onCreate].
 *
 * Application startup on [Intent.ACTION_BOOT_COMPLETED] is ensured by [org.altbeacon.beacon] lib.
 */
@Deprecated("Useless. Will be removed", level = DeprecationLevel.ERROR)
class BootCompleteReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        TimberBMS.d("BootCompleteReceiver#onReceive")
    }
}