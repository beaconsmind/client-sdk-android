package com.beaconsmind.sdk

import org.jetbrains.annotations.ApiStatus.Internal

/**
 * Constants used by the SDK.
 */
@Internal
object Constants {
    /**
     * BeaconsmindSDK log tag
     */
    const val LOGTAG = "BMS"

    /**
     * AltBeacon log tag prefix
     */
    const val LOGTAG_ALTBEACON = "BMS.ab"

    /* Preferences constants */

    const val PREFERENCES_NAME = "preferences.beaconsmind."
    const val PREFERENCES_ACCESS_TOKEN = "preferences.accessToken"
    const val PREFERENCES_UNIQUE_ID = "preferences.uniqueID"
    const val PREFERENCES_PLATFORM_DEVICE_ID = "preferences.platformId"
    const val PREFERENCES_IMPORTED_USERNAME = "preferences.importedUsername"
    const val PREFERENCES_SERVICE_ACTIVE = "preferences.serviceActive"
    const val PREFERENCES_STORE_BEACONS = "preferences.storeBeacons"
    const val PREFERENCES_PERMISSION_PROMPTED = "preferences.permissionsPrompted"
    const val PREFERENCES_BEACONSMIND_CONFIG = "preferences.beaconsmindConfig"
    const val PREFERENCES_REMOTE_CONFIG = "preferences.remoteConfig"

    /* Beacon scan settings */

    /**
     * Interval for fetching store beacons from beaconsmind server
     */
    const val BEACON_REFRESH_INTERVAL: Long = 60 * 60 * 1000 // 60min

    /**
     * Interval between beacon contact pings to beaconsmind server.
     */
    const val BEACON_CONTACTS_PUSH_INTERVAL: Long = 10 * 1000 // 10s

    /**
     * Time between BLE scans when we are monitoring beacons (not in range)
     */
    const val BEACON_MONITORING_FREQUENCY: Long = 10 * 1000 // 10s

    /**
     * Time between BLE scans when we are in beacon range.
     */
    const val BEACON_RANGING_FREQUENCY: Long = 1000 // 1s

    /**
     * Duration of a single BLE scan
     */
    const val BEACON_SCAN_PERIOD: Long = 2100 // 2.1s

    /**
     * Beaconsmind iBeacon layout
     */
    const val BEACON_IBEACON_LAYOUT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"
    /* Notification */
    /**
     * Active scanning notification ID.
     */
    const val NOTIFICATION_FG_SERVICE = 24

    /**
     * Channel ID of active scanning permanent notification.
     */
    const val NOTIFICATION_CHANNEL_ID = "notificaton.beacons"

    /* Misc */

    /**
     * Simple Date Format of beaconsmind backend
     */
    const val SUITE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
}