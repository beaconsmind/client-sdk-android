package com.beaconsmind.sdk

import com.beaconsmind.api.models.PlatformType
import com.beaconsmind.sdk.logging.TimberBMS
import com.google.firebase.messaging.FirebaseMessaging

/**
 * Device registration token provider for implementing custom push notifications. 
 * For custom push notifications set it in [BeaconsmindConfig] upon initialization.
 *
 * Make sure your class implementation doesn't have constructor parameters since they will not be serialized.
 * Deserialization is done via reflection so provide your implementation [className].
 *
 * @see [FcmTokenProvider]
 */
abstract class NativeIdProvider{
    /**
     * Callback for [NativeIdProvider.provideId] function call.
     */
    interface ProvideIdCallback {
        /**
         * Invoked when there is an exception getting the native id
         */
        fun error(e: Exception?)

        /**
         * Invoked when successfully getting the native id
         */
        fun success(providedId: String)
    }

    /**
     * Classname used to (de)serialize the object via reflection when persisting beaconsmind config.
     */
    abstract val className: String

    /**
     * Notification platform the device is using
     */
    abstract val platformType: PlatformType

    /**
     * Fetches the token and returns the result via [callback]
     */
    abstract fun provideId(callback: ProvideIdCallback)
}

/**
 * Default [NativeIdProvider] implementation used by the SDK. Provides Firebase FCM token.
 */
internal class FcmTokenProvider : NativeIdProvider() {
    override val className: String = FcmTokenProvider::class.java.name

    override val platformType: PlatformType = PlatformType.Fcm

    /**
     * Provides FCM token
     * @see [FirebaseMessaging.getToken]
     */
    override fun provideId(callback: ProvideIdCallback) {
        FirebaseMessaging.getInstance().token
            .addOnSuccessListener { token ->
                TimberBMS.d("FCMToken success: %s", token)
                callback.success(token)
            }
            .addOnFailureListener { exception ->
                TimberBMS.w(exception, "Fetching FCM registration token failed")
                callback.error(exception)
            }
    }
}