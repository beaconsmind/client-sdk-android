package com.beaconsmind.sdk

import android.content.Context
import androidx.annotation.DrawableRes
import com.beaconsmind.sdk.utils.getAppVersion

/**
 * BeaconsmindSdk initialization object.
 * Use [BeaconsmindConfig.Builder] for creating an instance.
 */
data class BeaconsmindConfig private constructor(
    /**
     * Base suite uri
     *
     * eg. https://example-suite.com
     */
    val suiteUri: String,

    /**
     * Host app version.
     *
     * *Optional* If not set appVersion is fetched from PackageManager when using Builder#build(Context)
     */
    val appVersion: String,

    /**
     * Enables fully background periodic scans, without persistent notification
     * and foreground service.
     *
     * - If **`true`**, notification related config is not required.
     * - If **`false`**, foreground service is used for scanning and requires active notification
     * to be displayed
     *
     * Starting from Android O uses intent scanning. Prior versions use background service and alarms for the same functionality.
     */
    val usePassiveScanning: Boolean = false,

    /**
     * Persistent notification icon resource id
     *
     * *Optional* if using `usePassiveScanning = true`
     */
    @DrawableRes val notificationBadge: Int,

    /**
     * Persistent notification title
     *
     *  *Optional* if using `usePassiveScanning = true`
     */
    val notificationTitle: String,

    /**
     * Persistent notification text. Gives context to the user about app service, to let him know that app is
     * scanning in background
     *
     *  *Optional* if using `usePassiveScanning = true`
     */
    val notificationText: String,

    /**
     * Persistent notification channel name.
     *
     * *Optional* if using `usePassiveScanning = true`
     */
    val notificationChannelName: String,

    /**
     * Device registration token provider for implementing custom push notifications.
     *
     * *Optional.* Defaults to [com.google.firebase.messaging.FirebaseMessaging]
     */
    val nativeIdProvider: NativeIdProvider,
) {

    /**
     * Overridden equality check. Checks all fields for equality. For [nativeIdProvider] property compares `className` field
     */
    override fun equals(other: Any?): Boolean {
        if (other !is BeaconsmindConfig) {
            return false
        }
        return appVersion == other.appVersion &&
                usePassiveScanning == other.usePassiveScanning &&
                notificationBadge == other.notificationBadge &&
                notificationTitle == other.notificationTitle &&
                notificationText == other.notificationText &&
                notificationChannelName == other.notificationChannelName &&
                nativeIdProvider.className == other.nativeIdProvider.className
    }

    /**
     * [BeaconsmindConfig] constants holder
     */
    internal companion object {
        /**
         * Default notification badge which means we are **not** using notification (when passive scanning)
         */
        internal val NO_NOTIFICATION_BADGE = 0
    }

    /**
     *  [BeaconsmindConfig] builder.
     *
     *  @constructor
     *  @param suiteUri required base url
     */
    class Builder(private val suiteUri: String) {
        /** If not set appVersion is fetched from PackageManager when using Builder#build(Context) */
        private var appVersion: String? = null

        private var usePassiveScanning = false

        /** *Optional* if using passiveScanning = true */
        private var notificationBadge: Int? = null

        /** *Optional* if using passiveScanning = true */
        private var notificationTitle: String? = null

        /** *Optional* if using passiveScanning = true */
        private var notificationText: String? = null

        /** *Optional* if using passiveScanning = true */
        private var notificationChannelName: String? = null

        private var nativeIdProvider: NativeIdProvider = FcmTokenProvider()

        /** **Optional** - If not set appVersion is fetched from PackageManager when using Builder#build(Context) */
        @Deprecated("If not set appVersion is fetched from PackageManager when using Builder#build(Context)")
        fun setAppVersion(appVersion: String): Builder {
            this.appVersion = appVersion
            return this
        }

        /**
         * Setter for [BeaconsmindConfig.usePassiveScanning]
         *
         * @see BeaconsmindConfig.usePassiveScanning
         */
        fun usePassiveScanning(usePassiveScanning: Boolean): Builder {
            this.usePassiveScanning = usePassiveScanning
            return this
        }

        /**
         * Setter for [BeaconsmindConfig.notificationBadge].
         * *Optional* if using `usePassiveScanning = true`
         *
         * @see BeaconsmindConfig.notificationBadge
         */
        fun setNotificationBadge(@DrawableRes notificationBadge: Int): Builder {
            this.notificationBadge = notificationBadge
            return this
        }

        /**
         * Setter for [BeaconsmindConfig.notificationTitle].
         * *Optional* if using `usePassiveScanning = true`
         *
         * @see BeaconsmindConfig.notificationTitle
         */
        fun setNotificationTitle(notificationTitle: String): Builder {
            this.notificationTitle = notificationTitle
            return this
        }

        /**
         * Setter for [BeaconsmindConfig.notificationText].
         * *Optional* if using `usePassiveScanning = true`
         *
         * @see BeaconsmindConfig.notificationText
         */
        fun setNotificationText(notificationText: String): Builder {
            this.notificationText = notificationText
            return this
        }

        /**
         * Setter for [BeaconsmindConfig.notificationChannelName].
         * *Optional* if using `usePassiveScanning = true`
         *
         * @see BeaconsmindConfig.notificationChannelName
         */
        fun setNotificationChannelName(notificationChannelName: String): Builder {
            this.notificationChannelName = notificationChannelName
            return this
        }

        /**
         * Setter for [BeaconsmindConfig.nativeIdProvider].
         * *Optional.* Defaults to FCM token provider.
         *
         * @see BeaconsmindConfig.nativeIdProvider
         */
        fun setNativeIdProvider(provider: NativeIdProvider): Builder {
            nativeIdProvider = provider
            return this
        }

        /**
         * This builder requires appVersion to be set.
         *
         * If you want the sdk to fetch your app version from PackageManager (build.config versionName + versionCode)
         * use Builder#build(Context)
         *
         * @throws MissingBuilderParamException if setup incorrectly
         */
        @Deprecated(
            message = "This builder requires appVersion to be set.",
            replaceWith = ReplaceWith("Builder.Build(Context)")
        )
        fun build(): BeaconsmindConfig {
            return BeaconsmindConfig(
                suiteUri = suiteUri,
                appVersion = appVersion ?: throw MissingBuilderParamException("appVersion"),
                usePassiveScanning = usePassiveScanning,
                notificationBadge = notificationBadge ?: if (usePassiveScanning) NO_NOTIFICATION_BADGE else throw MissingBuilderParamException("notificationBadge"),
                notificationTitle = notificationTitle ?: "Beaconsmind",
                notificationText = notificationText ?: "Listening for Beacons",
                notificationChannelName = notificationChannelName ?: "Beacon Listener Notification",
                nativeIdProvider = nativeIdProvider,
            )
        }

        /**
         * Builds [BeaconsmindConfig] and validates all required fields were set.
         *
         * Gets [appVersion] from PackageManager if not set.
         *
         * When using passive scanning you don't have to set notification fields.
         *
         * @throws MissingBuilderParamException if setup incorrectly
         */
        fun build(context: Context): BeaconsmindConfig {
            // assert notification fields are set when using active scanning
            if (!usePassiveScanning) {
                notificationBadge ?: throw MissingBuilderParamException("notificationBadge")
                if (notificationBadge == NO_NOTIFICATION_BADGE) throw MissingBuilderParamException("notificationBadge can't be 0")
                notificationTitle ?: throw MissingBuilderParamException("notificationTitle")
                notificationText ?: throw MissingBuilderParamException("notificationText")
                notificationChannelName ?: throw MissingBuilderParamException("notificationChannelName")
            }

            return BeaconsmindConfig(
                suiteUri = suiteUri,
                appVersion = appVersion ?: getAppVersion(context),
                usePassiveScanning = usePassiveScanning,
                notificationBadge = notificationBadge ?: NO_NOTIFICATION_BADGE,
                notificationTitle = notificationTitle ?: "",
                notificationText = notificationText ?: "",
                notificationChannelName = notificationChannelName ?: "",
                nativeIdProvider = nativeIdProvider,
            )
        }
    }
}