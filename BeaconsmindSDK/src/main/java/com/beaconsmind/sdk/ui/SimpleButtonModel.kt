package com.beaconsmind.sdk.ui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


/**
 * Parcelable button wrapper used by UI components.
 *
 * @see [com.beaconsmind.api.models.ButtonModel]
 * @see [com.beaconsmind.api.models.Button]
 */
@Parcelize
class SimpleButtonModel(val title: String?, val link: String?) : Parcelable