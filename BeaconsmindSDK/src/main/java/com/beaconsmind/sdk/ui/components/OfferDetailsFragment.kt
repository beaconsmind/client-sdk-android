package com.beaconsmind.sdk.ui.components

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.beaconsmind.api.models.NotificationResponse
import com.beaconsmind.api.models.OfferResponse
import com.beaconsmind.sdk.BeaconsmindConfig
import com.beaconsmind.sdk.PersistenceManager
import com.beaconsmind.sdk.R
import com.beaconsmind.sdk.UserManager
import com.beaconsmind.sdk.databinding.FragmentOfferDetailsBinding
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.ui.BeaconsmindUI
import com.beaconsmind.sdk.ui.SimpleOfferModel
import com.beaconsmind.sdk.ui.toPx
import com.beaconsmind.sdk.utils.TextUtils
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat


/**
 * A simple [Fragment] subclass that holds beaconsmind offer details UI.
 *
 * Use the [OfferDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 * Implement [IOfferRedeemed] in the holding activity to receive the offer redeemed callback
 *
 * The fragment is styled by [BeaconsmindUI] configuration
 *
 * @see IOfferRedeemed
 * @see BeaconsmindUI
 */
class OfferDetailsFragment : Fragment() {

    private val offerId: Int? by lazy { arguments?.getInt(ARG_OFFER_ID) }
    private val offer: SimpleOfferModel? by lazy { arguments?.getParcelable(ARG_OFFER_MODEL) }

    private lateinit var binding: FragmentOfferDetailsBinding

    private var redeemCallback: IOfferRedeemed? = null

    private val viewModel: OfferDetailsViewModel by lazy {
        ViewModelProvider(this).get(OfferDetailsViewModel::class.java)
    }

    /**
     * Tries to attach activity [context] as [IOfferRedeemed] listener
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        redeemCallback = context as? IOfferRedeemed
        if (redeemCallback == null) {
            TimberBMS.w("Didn't attach offer redeem listener")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val root = inflater.inflate(R.layout.fragment_offer_details, container, false)
        binding = FragmentOfferDetailsBinding.bind(root)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when {
            offer != null -> viewModel.setOfferModel(offer!!)
            offerId != null -> viewModel.setOfferId(offerId!!)
            else -> throw IllegalArgumentException("No arguments provided")
        }

        // observe the offer changes
        viewModel.getOfferLiveData().observe(viewLifecycleOwner) { offerResponse ->
            offerResponse?.data?.let { setup(it) }

            offerResponse?.error?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        }

        // redeem offer callback
        viewModel.offerRedeemedLD.observe(viewLifecycleOwner) {
            if (it != null) {
                viewModel.offerRedeemedLD.value = null
                Toast.makeText(
                    requireContext(),
                    BeaconsmindUI.localizationStrings.redeemSuccessMessage,
                    Toast.LENGTH_SHORT
                ).show()
                redeemCallback?.onOfferSuccessfullyRedeemed(it)
            }
        }
    }

    /**
     * Displays [offer] in fragment UI
     */
    @SuppressLint("SetJavaScriptEnabled")
    private fun setup(simpleOffer: SimpleOfferModel) {
        val persistance = context?.let { PersistenceManager.getInstanceWithContext(it) } ?: return
        val suiteUri = persistance.beaconsmindConfig?.suiteUri ?: return
        val accessToken = persistance.accessToken ?: return

        binding.webview.apply {
            webViewClient = object : WebViewClient() {
                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    binding.progressBar.visibility = View.GONE
                }
            }
            val headers = mapOf("Authorization" to "Bearer $accessToken")
            loadUrl("$suiteUri/api/offers/preview/${simpleOffer.offerId}", headers)
            webChromeClient = WebChromeClient()
            addJavascriptInterface(
                OfferJSInterface(
                    onRedeem = {
                        activity?.runOnUiThread {
                            if (simpleOffer.isRedeemable && !simpleOffer.isRedeemed) {
                                AlertDialog.Builder(requireContext())
                                    .setMessage(BeaconsmindUI.localizationStrings.redeemQuestionMessage)
                                    .setPositiveButton(BeaconsmindUI.localizationStrings.alertOKTitle) { _, _ -> viewModel.redeemOffer() }
                                    .setNegativeButton(BeaconsmindUI.localizationStrings.alertCancelTitle) { _, _ -> }
                                    .create()
                                    .show()
                            }
                        }
                    }, onCtaClick = {
                        activity?.runOnUiThread {
                            simpleOffer.callToAction?.let { cta ->
                                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(cta.link))
                                try {
                                    startActivity(intent)
                                    viewModel.onClicked()
                                } catch (e: ActivityNotFoundException) {
                                    TimberBMS.e("Couldn't start call to action link '${cta.link}'")
                                }
                            }
                        }
                    }), "Android"
            )
            settings.javaScriptEnabled = true
        }

        binding.toolbar.title = simpleOffer.title
        binding.toolbar.visibility = when (BeaconsmindUI.styleSheet.useToolbar) {
            true -> View.VISIBLE
            false -> View.GONE
        }
    }

    /**
     * Holds [OfferDetailsFragment]'s factory methods
     */
    companion object {
        const val ARG_OFFER_ID = "OfferDetailsFragment.offer_id"
        const val ARG_OFFER_MODEL = "OfferDetailsFragment.offer_model"

        /**
         * Creates the fragment instance with Offer's [offerId]. The fragment will fetch the offer upon launching.
         */
        @JvmStatic
        fun newInstance(offerId: Int) = OfferDetailsFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_OFFER_ID, offerId)
            }
        }

        /**
         * Creates an intent with [SimpleOfferModel]. The fragment will display the offer immediately.
         */
        @JvmStatic
        fun newInstance(simpleOfferModel: SimpleOfferModel) = OfferDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_OFFER_MODEL, simpleOfferModel)
            }
        }


        /**
         * Creates an intent with [OfferResponse]. The fragment will display the offer immediately.
         */
        @JvmStatic
        fun newInstance(offerResponse: OfferResponse) = newInstance(SimpleOfferModel(offerResponse))

        /**
         * Creates an intent with [NotificationResponse]. The fragment will display the offer immediately.
         */
        @JvmStatic
        fun newInstance(offer: NotificationResponse) = newInstance(SimpleOfferModel(offer))
    }
}