package com.beaconsmind.sdk.ui

import android.content.res.Resources
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import com.beaconsmind.sdk.ui.components.OfferDetailsFragment

/**
 * Components style configuration. Use it to specify colors and component dimensions.
 *
 * Build it with kotlin parameterized constructor or use [StyleSheet.Builder] for java callers
 */
class StyleSheet(
    /**
     * Sets the background color of the screen. Defaults to white.
     */
    @ColorInt val backgroundColor: Int = BUILDER.backgroundColor,

    /**
     * Sets redeem button's title color. Defaults to black.
     */
    @ColorInt val redeemButtonTitleColor: Int = BUILDER.redeemButtonTitleColor,

    /**
     * Sets redeem button's background color. Defaults to black.
     */
    @ColorInt val redeemButtonBackgroundColor: Int = BUILDER.redeemButtonBackgroundColor,

    /**
     * Sets primary action button's title color. Defaults to white.
     */
    @ColorInt val primaryButtonTitleColor: Int = BUILDER.primaryButtonTitleColor,

    /**
     * Sets primary action button's background color. Defaults to black.
     */
    @ColorInt val primaryButtonBackgroundColor: Int = BUILDER.primaryButtonBackgroundColor,

    /**
     * Sets action buttons height in DP.
     */
    @Dimension(unit = Dimension.DP)
    val buttonHeight: Int = BUILDER.buttonHeight,

    /**
     * Sets action buttons borders in DP.
     */
    @Dimension(unit = Dimension.DP)
    val buttonCorners: Int = BUILDER.buttonCorners,

    /**
     * Toggle to use toolbar in [OfferDetailsFragment]
     */
    val useToolbar: Boolean = BUILDER.useToolbar,
) {

    private companion object {
        /**
         * Default values provider
         */
        private val BUILDER by lazy { Builder() }
    }

    /**
     * [StyleSheet] builder with default values
     */
    class Builder {
        /**
         * Sets the background color of the screen. Defaults to white.
         */
        @ColorInt
        var backgroundColor: Int = Color.WHITE
            private set

        fun setBackgroundColor(backgroundColor: Int) =
            apply { this.backgroundColor = backgroundColor }


        /**
         * Sets redeem button's title color. Defaults to black.
         */
        @ColorInt
        var redeemButtonTitleColor: Int = Color.BLACK
            private set

        fun setRedeemButtonTitleColor(redeemButtonTitleColor: Int) =
            apply { this.redeemButtonTitleColor = redeemButtonTitleColor }


        /**
         * Sets redeem button's background color. Defaults to black.
         */
        @ColorInt
        var redeemButtonBackgroundColor: Int = Color.BLACK
            private set

        fun setRedeemButtonBackgroundColor(redeemButtonBackgroundColor: Int) =
            apply { this.redeemButtonBackgroundColor = redeemButtonBackgroundColor }


        /**
         * Sets primary action button's title color. Defaults to white.
         */
        @ColorInt
        var primaryButtonTitleColor: Int = Color.WHITE
            private set

        fun setPrimaryButtonTitleColor(primaryButtonTitleColor: Int) =
            apply { this.primaryButtonTitleColor = primaryButtonTitleColor }


        /**
         * Sets primary action button's background color. Defaults to black.
         */
        @ColorInt
        var primaryButtonBackgroundColor: Int = Color.BLACK
            private set

        fun setPrimaryButtonBackgroundColor(primaryButtonBackgroundColor: Int) =
            apply { this.primaryButtonBackgroundColor = primaryButtonBackgroundColor }


        /**
         * Set action buttons height in Dp
         */
        @Dimension(unit = Dimension.DP)
        var buttonHeight: Int = 48
            private set

        fun setButtonHeight(buttonSize: Int) = apply {
            this.buttonHeight = buttonSize;
        }

        /**
         * Set action buttons corner radius in Dp
         */
        @Dimension(unit = Dimension.DP)
        var buttonCorners: Int = 2
            private set

        fun setButtonCorners(buttonCorners: Int) = apply {
            this.buttonCorners = buttonCorners;
        }

        /**
         * Set this if you want to use toolbar on the details screen. Defaults to false
         */
        var useToolbar: Boolean = false
            private set

        fun setUseToolbar(useToolbar: Boolean) = apply { this.useToolbar = useToolbar }

        /**
         * Constructs [StyleSheet] instance with configured fields.
         */
        fun build(): StyleSheet = StyleSheet(
            backgroundColor = backgroundColor,
            redeemButtonTitleColor = redeemButtonTitleColor,
            redeemButtonBackgroundColor = redeemButtonBackgroundColor,
            primaryButtonTitleColor = primaryButtonTitleColor,
            primaryButtonBackgroundColor = primaryButtonBackgroundColor,
            buttonHeight = buttonHeight,
            buttonCorners = buttonCorners,
            useToolbar = useToolbar
        )
    }

}


/**
 * Extension function on [Int] receiver. Converts Dp value to Pixels
 */
val Int.toPx: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()