package com.beaconsmind.sdk.ui.components

import com.beaconsmind.sdk.ui.SimpleOfferModel


/**
 * Offers list callbacks
 *
 * Implement it to get notified about the offer click, called by [OffersListFragment]
 *
 * @see OffersListFragment
 */
interface IOfferClicked {
    fun onOfferClicked(offer: SimpleOfferModel)
}