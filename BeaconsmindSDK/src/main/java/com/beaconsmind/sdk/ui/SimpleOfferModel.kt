package com.beaconsmind.sdk.ui

import android.os.Parcelable
import com.beaconsmind.api.models.Button
import com.beaconsmind.api.models.ButtonModel
import com.beaconsmind.api.models.NotificationResponse
import com.beaconsmind.api.models.OfferResponse
import kotlinx.parcelize.Parcelize
import java.util.Date

/**
 * Parcelable offer wrapper used used by UI components.
 *
 * @see NotificationResponse
 * @see OfferResponse
 */
@Parcelize
class SimpleOfferModel(
    val messageId: Int = 0,
    val offerId: Int = 0,
    val title: String? = null,
    val text: String? = null,
    val imageUrl: String? = null,
    val validity: String? = null,
    @Deprecated("Use: isRedeemable")
    val isVoucher: Boolean = false,
    val isRedeemable: Boolean = false,
    val isRedeemed: Boolean = false,
    val delivered: Date? = null,
    val callToAction: SimpleButtonModel? = null,
) : Parcelable {

    /**
     * [NotificationResponse] constructor
     */
    constructor(notificationModel: NotificationResponse) : this(
        messageId = notificationModel.messageId,
        offerId = notificationModel.offerId,
        title = notificationModel.title,
        text = notificationModel.text,
        imageUrl = notificationModel.imageUrl,
        validity = notificationModel.validity,
        isVoucher = notificationModel.isRedeemable,
        isRedeemable = notificationModel.isRedeemable,
        isRedeemed = notificationModel.isRedeemed,
        delivered = notificationModel.delivered,
        callToAction = notificationModel.callToAction?.getAsParcelable()
    )

    /**
     * [OfferResponse] constructor
     */
    constructor(offerModel: OfferResponse) : this(
        messageId = offerModel.messageId,
        offerId = offerModel.offerId,
        title = offerModel.title,
        text = offerModel.text,
        imageUrl = offerModel.imageUrl,
        validity = offerModel.validity,
        isVoucher = offerModel.isVoucher,
        isRedeemable = offerModel.isRedeemable,
        isRedeemed = offerModel.isRedeemed,
        delivered = offerModel.delivered,
        callToAction = offerModel.callToAction?.getAsParcelable()
    )

    private companion object {
        /**
         * Helper extension function on [ButtonModel] receiver. Returns wrapped [SimpleButtonModel]
         *
         * @receiver [ButtonModel]
         */
        fun ButtonModel?.getAsParcelable(): SimpleButtonModel? = this?.run {
            SimpleButtonModel(title, link)
        }

        /**
         * Helper ectension function on [Button] receiver. Returns wrapperd [SimpleButtonModel]
         *
         * @receiver [Button]
         */
        fun Button?.getAsParcelable(): SimpleButtonModel? = this?.run {
            SimpleButtonModel(title, link)
        }
    }
}