package com.beaconsmind.sdk.ui.components

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.beaconsmind.api.models.NotificationResponse
import com.beaconsmind.api.models.OfferResponse
import com.beaconsmind.sdk.databinding.ActivityOfferDetailsBinding
import com.beaconsmind.sdk.ui.SimpleOfferModel
import com.beaconsmind.sdk.ui.components.OfferDetailsActivity.Companion


/**
 * Activity which holds [OfferDetailsFragment]. Displays offer details.
 *
 * Use one of [OfferDetailsActivity.Companion.getIntent] overloads to get the launcher intent.
 *
 * @see OfferDetailsFragment
 */
class OfferDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityOfferDetailsBinding =
            ActivityOfferDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            val simpleOffer: SimpleOfferModel? = intent.getParcelableExtra(OFFER)

            val instance: Fragment = if (simpleOffer != null) {
                OfferDetailsFragment.newInstance(simpleOffer)
            } else {
                val offerId = intent.getIntExtra(OFFER_ID, -1)
                OfferDetailsFragment.newInstance(offerId)
            }

            supportFragmentManager.beginTransaction()
                .add(binding.fragmentContainer.getId(), instance, "TAG").commit()
        }
    }

    companion object {
        const val OFFER = "offer"
        const val OFFER_ID = "offerId"

        /**
         * Creates an intent with Offer's [offerId]. The fragment will fetch the offer upon launching.
         */
        @JvmStatic
        fun getIntent(context: Context, offerId: Int) =
            Intent(context, OfferDetailsActivity::class.java).apply {
                putExtra(OFFER_ID, offerId)
            }

        /**
         * Creates an intent with [SimpleOfferModel]. The fragment will display the offer immediately.
         */
        @JvmStatic
        fun getIntent(context: Context, offer: SimpleOfferModel) =
            Intent(context, OfferDetailsActivity::class.java).apply {
                putExtra(OFFER, offer)
            }

        /**
         * Creates an intent with [OfferResponse]. The fragment will display the offer immediately. Wraps the offer in [SimpleOfferModel]
         */
        @JvmStatic
        fun getIntent(context: Context, offer: OfferResponse) =
            Companion.getIntent(context, SimpleOfferModel(offer))

        /**
         * Creates an intent with [NotificationResponse]. The fragment will display the offer immediately. Wraps the offer in [SimpleOfferModel]
         */
        @JvmStatic
        fun getIntent(context: Context, notificationResponse: NotificationResponse) =
            Companion.getIntent(context, SimpleOfferModel(notificationResponse))
    }
}