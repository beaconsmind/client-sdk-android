package com.beaconsmind.sdk.ui.components


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.beaconsmind.api.models.OfferResponse
import com.beaconsmind.sdk.databinding.FragmentOffersBinding
import com.beaconsmind.sdk.ui.BeaconsmindUI
import com.beaconsmind.sdk.ui.SimpleOfferModel
import com.beaconsmind.sdk.ui.components.OfferDetailsActivity.Companion.getIntent


/**
 * A simple [Fragment] subclass that holds beaconsmind offers list UI.
 * Fetches the user's offers when inflated and handles user interaction.
 *
 * Implement [IOfferClicked] in your host activity to receiv the offer click callback.
 * Otherwise the default [OfferDetailsActivity] is opened.
 */
class OffersListFragment : Fragment() {

    private lateinit var binding: FragmentOffersBinding

    private var offerClickedCallback: IOfferClicked? = null

    private val viewModel: OffersListViewModel by lazy {
        ViewModelProvider(this).get(OffersListViewModel::class.java)
    }

    /**
     * Tries to attach activity [context] as [IOfferClicked] listener
     */
    override fun onAttach(context: Context) {
        super.onAttach(context)
        offerClickedCallback = context as? IOfferClicked
    }

    override fun onDetach() {
        super.onDetach()
        offerClickedCallback = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        binding = FragmentOffersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeComponent()

        val offerAdapter = OffersListAdapter(::onRead)

        binding.recyclerView.adapter = offerAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireActivity())

        viewModel.loadOffers()
        viewModel.offersLD.observe(viewLifecycleOwner) { offersResponse ->
            binding.loading.visibility = if (offersResponse.loading) View.VISIBLE else View.GONE

            offersResponse.data?.let {
                binding.emptyMsg.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
                offerAdapter.submitOffers(it)
            }

            offersResponse.error?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        }

    }

    /**
     * Initializes the componend styled by [BeaconsmindUI]
     */
    private fun initializeComponent() = with(binding) {
        emptyMsg.text = BeaconsmindUI.localizationStrings.noOffersMessage
    }

    /**
     * Tries to dispatch the click to host activity via [IOfferClicked].
     * If the host activity doesn't implement it, a new [OfferDetailsActivity] will be started.
     */
    private fun onRead(offer: OfferResponse) {
        offerClickedCallback?.onOfferClicked(SimpleOfferModel(offer)) ?: run {
            val intent = getIntent(requireActivity(), offer)
            requireActivity().startActivity(intent)
        }
    }
}