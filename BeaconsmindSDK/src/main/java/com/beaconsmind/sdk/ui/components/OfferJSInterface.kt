package com.beaconsmind.sdk.ui.components

import android.webkit.JavascriptInterface

class OfferJSInterface(val onRedeem: () -> Unit, val onCtaClick: () -> Unit) {
    @JavascriptInterface
    fun ctaClick(e: Any?) {
        onCtaClick()
    }

    @JavascriptInterface
    fun redeemClick(e: Any?) {
        onRedeem()
    }
}