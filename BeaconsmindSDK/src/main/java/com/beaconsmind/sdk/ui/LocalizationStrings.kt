package com.beaconsmind.sdk.ui

/**
 * Components string configuration. Use it to specify localized messages.
 *
 * Build it with kotlin parameterized constructor or use [LocalizationStrings.Builder] for java callers
 */
class LocalizationStrings(
    /**
     * Sets redeem button's title. Defaults to "Redeem".
     */
    val redeemButtonTitle: String = BUILDER.redeemButtonTitle,

    /**
     * Sets redeem question message. Defaults to "Do you want to redeem this Offer?".
     */
    val redeemQuestionMessage: String = BUILDER.redeemQuestionMessage,

    /**
     * Sets redeem success message. Defaults to "Offer redeemed!".
     */
    val redeemSuccessMessage: String = BUILDER.redeemSuccessMessage,

    /**
     * Sets alerts' ok button text. Defaults to "OK".
     */
    val alertOKTitle: String = BUILDER.alertOKTitle,

    /**
     * Sets alerts' cancel button text. Defaults to "Cancel".
     */
    val alertCancelTitle: String = BUILDER.alertCancelTitle,

    /**
     * Sets optional error message when reading an offer fails. Defaults to "Couldn't read offer, please try again".
     */
    val readingOfferErrorMessage: String = BUILDER.readingOfferErrorMessage,

    /**
     * Sets optional error message when reading an offer fails. Defaults to "Couldn't redeem offer, please try again".
     */
    val redeemingOfferErrorMessage: String = BUILDER.redeemingOfferErrorMessage,

    /**
     * Sets noOffersMessage on offers list screen
     */
    val noOffersMessage: String = BUILDER.noOffersMessage,

    /**
     * Sets offers list read more button text
     */
    val readMoreMessage: String = BUILDER.readMoreMessage,
) {

    private companion object {
        /**
         * Default values provider
         */
        private val BUILDER by lazy { Builder() }
    }

    class Builder {

        /**
         * Sets redeem button's title. Defaults to "Redeem".
         */
        var redeemButtonTitle: String = "Redeem"
            private set

        fun setRedeemButtonTitle(redeemButtonTitle: String) =
            apply { this.redeemButtonTitle = redeemButtonTitle }


        /**
         * Sets redeem question message. Defaults to "Do you want to redeem this Offer?".
         */
        var redeemQuestionMessage: String = "Do you want to redeem this Offer?"
            private set

        fun setRedeemQuestionMessage(redeemQuestionMessage: String) = apply {
            this.redeemQuestionMessage = redeemQuestionMessage
        }


        /**
         * Sets redeem success message. Defaults to "Offer redeemed!".
         */
        var redeemSuccessMessage: String = "Offer redeemed!"
            private set

        fun setRedeemSuccessMessage(redeemSuccessMessage: String) = apply {
            this.redeemSuccessMessage = redeemSuccessMessage
        }


        /**
         * Sets alerts' ok button text. Defaults to "OK".
         */
        var alertOKTitle: String = "OK"
            private set

        fun setAlertOKTitle(alertOKTitle: String) = apply { this.alertOKTitle = alertOKTitle }


        /**
         * Sets alerts' cancel button text. Defaults to "Cancel".
         */
        var alertCancelTitle: String = "Cancel"
            private set

        fun setAlertCancelTitle(alertCancelTitle: String) =
            apply { this.alertCancelTitle = alertCancelTitle }


        /**
         * Sets optional error message when reading an offer fails. Defaults to "Couldn't read offer, please try again".
         */
        var readingOfferErrorMessage: String = "Couldn't read offer, please try again"
            private set

        fun setReadingOfferErrorMessage(readingOfferErrorMessage: String) =
            apply { this.readingOfferErrorMessage = readingOfferErrorMessage }


        /**
         * Sets optional error message when reading an offer fails. Defaults to "Couldn't redeem offer, please try again".
         */
        var redeemingOfferErrorMessage: String = "Couldn't redeem offer, please try again"
            private set

        fun setRedeemingOfferErrorMessage(redeemingOfferErrorMessage: String) =
            apply { this.redeemingOfferErrorMessage = redeemingOfferErrorMessage }


        /**
         * Sets noOffersMessage on offers list screen. Defaults to "No offers"
         */
        var noOffersMessage: String = "No offers"
            private set

        fun setNoOffersMessage(noOffersMessage: String) =
            apply { this.noOffersMessage = noOffersMessage }


        /**
         * Sets offers list read more button text. Defaults to "Read more"
         */
        var readMoreMessage: String = "Read more"

        fun setReadMoreMessage(readMoreMessage: String) =
            apply { this.readMoreMessage = readMoreMessage }


        /**
         * Constructs [LocalizationStrings] instance with configured fields.
         */
        fun build(): LocalizationStrings = LocalizationStrings(
            redeemButtonTitle = redeemButtonTitle,
            redeemQuestionMessage = redeemQuestionMessage,
            redeemSuccessMessage = redeemSuccessMessage,
            alertOKTitle = alertOKTitle,
            alertCancelTitle = alertCancelTitle,
            readingOfferErrorMessage = readingOfferErrorMessage,
            redeemingOfferErrorMessage = redeemingOfferErrorMessage,
            noOffersMessage = noOffersMessage,
            readMoreMessage = readMoreMessage,
        )
    }
}