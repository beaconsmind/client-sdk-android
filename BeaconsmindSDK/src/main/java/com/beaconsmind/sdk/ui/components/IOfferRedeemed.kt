package com.beaconsmind.sdk.ui.components

/**
 * Offer details callbacks.
 *
 * Implement it in the host activity to get notified about the offer status. Called by [OfferDetailsFragment]
 *
 * @see OfferDetailsFragment
 */
interface IOfferRedeemed {
    /**
     * Invoked when the user clicked redeem and server returned success.
     *
     * @param offerId offer which was redeemed
     */
    fun onOfferSuccessfullyRedeemed(offerId: Int)
}