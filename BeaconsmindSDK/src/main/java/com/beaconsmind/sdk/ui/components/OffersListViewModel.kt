package com.beaconsmind.sdk.ui.components

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beaconsmind.api.models.OfferResponse
import com.beaconsmind.sdk.Error
import com.beaconsmind.sdk.OffersManager
import com.beaconsmind.sdk.OnResultListener
import com.beaconsmind.sdk.ui.BeaconsmindUI
import com.beaconsmind.sdk.ui.NetworkLoader

/**
 * [OffersListFragment]'s ViewModel
 */
class OffersListViewModel : ViewModel() {

    private val _offersLD: MutableLiveData<NetworkLoader<List<OfferResponse>>> =
        MutableLiveData(NetworkLoader.loading())

    val offersLD: LiveData<NetworkLoader<List<OfferResponse>>> get() = _offersLD

    /**
     * Get offers network request
     */
    fun loadOffers() {
        OffersManager.getInstance().loadOffers(object : OnResultListener<List<OfferResponse>> {

            override fun onError(error: Error) {
                _offersLD.postValue(NetworkLoader.error(BeaconsmindUI.localizationStrings.readingOfferErrorMessage))
            }

            override fun onSuccess(result: List<OfferResponse>) {
                _offersLD.postValue(NetworkLoader.success(result))
            }

        })
    }

}