package com.beaconsmind.sdk.ui.components


import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beaconsmind.api.models.OfferResponse
import com.beaconsmind.sdk.databinding.ItemOfferBinding
import com.beaconsmind.sdk.ui.BeaconsmindUI
import com.beaconsmind.sdk.ui.components.OffersListAdapter.OfferViewHolder
import com.beaconsmind.sdk.ui.toPx
import com.beaconsmind.sdk.utils.TextUtils.fromHtml
import com.bumptech.glide.Glide
import java.util.*

/**
 * RecyclerView.Adapter for [OffersListFragment]. Displays the list of offers.
 */
class OffersListAdapter(private val onReadListener: (OfferResponse) -> Unit) :
    RecyclerView.Adapter<OfferViewHolder>() {

    private var offers: List<OfferResponse> = ArrayList()
    fun submitOffers(offers: List<OfferResponse>) {
        this.offers = offers
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemOfferBinding.inflate(inflater, parent, false)
        return OfferViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) {
        val offer = offers[position]
        holder.bind(offer)
    }

    override fun getItemCount(): Int = offers.size

    inner class OfferViewHolder(private val binding: ItemOfferBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            with(binding.btnRead) {
                backgroundTintList =
                    ColorStateList.valueOf(BeaconsmindUI.styleSheet.primaryButtonBackgroundColor)
                setTextColor(BeaconsmindUI.styleSheet.primaryButtonTitleColor)
                text = BeaconsmindUI.localizationStrings.readMoreMessage
                height = BeaconsmindUI.styleSheet.buttonHeight.toPx
                cornerRadius = BeaconsmindUI.styleSheet.buttonCorners.toPx
            }
        }

        fun bind(offer: OfferResponse) {
            Glide.with(binding.image)
                .load(offer.imageUrl)
                .into(binding.image)

            binding.title.text = offer.title
            binding.description.text = fromHtml(offer.text)?.trim()
            binding.btnRead.setOnClickListener { onReadListener(offer) }
        }
    }
}