package com.beaconsmind.sdk.ui


/**
 * Wraps network request data.
 *
 * @constructor
 * @property data object of class [T] that is being fetched from the network
 * @property error request string error
 * @property loading indicates a new request is still running
 */
class NetworkLoader<T>(val data: T?, val error: String?, val loading: Boolean) {
    /**
     * Holds [NetworkLoader] general builder functions
     */
    companion object {
        /**
         * Helper function that constructs a [NetworkLoader] that is executing a new request.
         *
         * @param data data that was previously fetched. null if none
         */
        fun <T> loading(data: T? = null): NetworkLoader<T> = NetworkLoader(data, null, true)

        /**
         * Helper function that constructs a [NetworkLoader] that got a successful server response
         *
         * @param data resource which the server returned
         */
        fun <T> success(data: T) = NetworkLoader(data, null, false)

        /**
         * Helper function that constructs a [NetworkLoader] that got a failed server response
         *
         * @param error string error message
         */
        fun <T> error(error: String) = NetworkLoader(null as T?, error, false)
    }
}