package com.beaconsmind.sdk.ui


/**
 * Beaconsmind UI components configuration object.
 *
 * If your app supports dark theme and multiple languages remember to update it
 * in your Activity or Application #onConfigurationChanged callback
 *
 * @see StyleSheet
 * @see LocalizationStrings
 */
object BeaconsmindUI {

    /**
     * Components style definition.
     */
    @JvmStatic
    var styleSheet: StyleSheet = StyleSheet.Builder().build()

    /**
     * Components strings.
     */
    @JvmStatic
    var localizationStrings: LocalizationStrings = LocalizationStrings()
}