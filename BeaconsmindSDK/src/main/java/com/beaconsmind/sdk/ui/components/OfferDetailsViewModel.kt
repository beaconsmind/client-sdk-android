package com.beaconsmind.sdk.ui.components

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.beaconsmind.api.models.OfferResponse
import com.beaconsmind.sdk.Error
import com.beaconsmind.sdk.OffersManager
import com.beaconsmind.sdk.OnCompleteListener
import com.beaconsmind.sdk.OnResultListener
import com.beaconsmind.sdk.ui.BeaconsmindUI
import com.beaconsmind.sdk.ui.NetworkLoader
import com.beaconsmind.sdk.ui.SimpleOfferModel

/**
 * [OfferDetailsFragment]'s ViewModel.
 */
class OfferDetailsViewModel : ViewModel() {

    /**
     * LiveData that holds the current offer model.
     */
    private val offerLD: MutableLiveData<NetworkLoader<SimpleOfferModel>> = MutableLiveData()

    /**
     * [offerLD] observer that marks the offer as read when it is set/loaded from network
     */
    private val readOfferObserver = Observer<NetworkLoader<SimpleOfferModel>> {
        val offer = it.data
        if (offer != null) {
            OffersManager.getInstance().markOfferAsRead(offer.offerId)
        }
    }

    /**
     * LiveData that holds offer which was redeemed successfully. Null if not redeemed
     */
    val offerRedeemedLD: MutableLiveData<Int?> = MutableLiveData()

    init {
        offerLD.observeForever(readOfferObserver)
    }

    override fun onCleared() {
        offerLD.removeObserver(readOfferObserver)
    }

    /**
     * Exposes offer model.
     */
    fun getOfferLiveData(): LiveData<NetworkLoader<SimpleOfferModel>?> {
        return offerLD
    }

    /**
     * Sets the initial offer model. Doesn't reload it.
     */
    fun setOfferModel(offerModel: SimpleOfferModel) {
        if (offerLD.value?.data != offerModel) {
            offerLD.postValue(NetworkLoader.success(offerModel))
        }
    }

    /**
     * Loads the offer from server.
     */
    fun setOfferId(offerId: Int) {
        if (offerLD.value?.data?.offerId != offerId) {
            offerLD.postValue(NetworkLoader.loading())
            findOffer(offerId)
        }
    }

    /**
     * Get offer network call
     */
    private fun findOffer(offerId: Int) {
        OffersManager.getInstance().loadOffer(offerId, object : OnResultListener<OfferResponse> {
            override fun onSuccess(result: OfferResponse?) {
                result?.let {
                    offerLD.postValue(NetworkLoader.success(SimpleOfferModel(it)))
                } ?: onError(Error.BadResponse)
            }

            override fun onError(error: Error) {
                offerLD.postValue(NetworkLoader.error(BeaconsmindUI.localizationStrings.readingOfferErrorMessage))
            }
        })
    }

    /**
     * Action button clicked. Marks it as clicked.
     */
    fun onClicked() {
        val offer = offerLD.value!!.data!!
        OffersManager.getInstance().markOfferClicked(offerId = offer.offerId)
    }

    /**
     * Redeem offer button clicked. Marks the offer redeemed and reloads it in case it has a different message when redeemed.
     */
    fun redeemOffer() {
        val offer = offerLD.value!!.data!!

        offerLD.postValue(NetworkLoader.loading(offerLD.value?.data))

        OffersManager.getInstance().markOfferRedeemed(offer.offerId, object : OnCompleteListener {
            override fun onSuccess() {
                offerRedeemedLD.postValue(offer.offerId)
                offerLD.postValue(NetworkLoader(offer, null, true))
                findOffer(offer.offerId)
            }

            override fun onError(error: Error?) {
                offerLD.postValue(NetworkLoader(offer,
                    BeaconsmindUI.localizationStrings.redeemingOfferErrorMessage,
                    false))
            }

        })
    }

}