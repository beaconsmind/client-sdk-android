package com.beaconsmind.sdk;


import androidx.annotation.Nullable;

import com.auth0.android.jwt.DecodeException;
import com.auth0.android.jwt.JWT;
import com.beaconsmind.sdk.logging.Logger;

/**
 * Holds data contained in beaconsmind suite access token.
 *
 * Use {@link JwtPayload#decode(String)} on accessToken to obtain an instance.
 *
 * @see JwtPayload#decode(String)
 * @see UserManager#accessToken
 */
public class JwtPayload {

    private String nameid;
    private String unique_name;
    private String role;
    private String given_name;
    private String family_name;
    private String email;
    private String gender;
    private String thumbnail;
    private String iss;
    private String aud;

    /**
     * Decodes payload from beaconsmind suite access token.
     */
    @Nullable
    public static JwtPayload decode(@Nullable String token) {
        if (token == null) return null;
        try {
            JWT jwt = new JWT(token);
            JwtPayload model = new JwtPayload();
            model.nameid = jwt.getClaim("nameid").asString();
            model.unique_name = jwt.getClaim("unique_name").asString();
            model.role = jwt.getClaim("role").asString();
            model.given_name = jwt.getClaim("given_name").asString();
            model.family_name = jwt.getClaim("family_name").asString();
            model.email = jwt.getClaim("email").asString();
            model.gender = jwt.getClaim("gender").asString();
            model.thumbnail = jwt.getClaim("thumbnail").asString();
            model.iss = jwt.getClaim("iss").asString();
            model.aud = jwt.getClaim("aud").asString();
            return model;
        } catch (DecodeException ex) {
            Logger.TimberBMS().e(ex);
            return null;
        }
    }

    public String getDisplayName() {
        return this.given_name + " " + this.family_name;
    }


    /**
     * Returns token owner user id.
     */
    public String getUserId() {
        return this.nameid;
    }
}