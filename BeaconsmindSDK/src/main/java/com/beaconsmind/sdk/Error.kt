package com.beaconsmind.sdk

import com.beaconsmind.api.client.ApiException

/**
 * Beaconsmind API network error wrapper
 */
sealed class Error {
    /**
     * Unexpected network error. Might be response code 4xx, 5xx or device network error.
     *
     * @property exception Internal exception cause
     * @property apiException Getter for beaconsmind [ApiException]
     */
    open class NetworkError internal constructor(val exception: Exception) : Error() {
        val apiException: ApiException? get() = exception as? ApiException
    }

    /**
     * Error with server response
     */
    object BadResponse : Error()

    /**
     * Accessing restricted API without valid user authorization.
     */
    object Unauthorized : Error()

    /**
     * Couldn't decode JWT token returned from server.
     */
    object InvalidToken : Error()


    /**
     * Deprecated enums holder.
     *
     * TODO: 19/06/2023 should be removed in near future
     */
    companion object {
        @JvmStatic
        @Deprecated("Syntax changed",
            replaceWith = ReplaceWith("NetworkError"))
        val NETWORK_ERROR = NetworkError(Exception("Dummy exception"))

        @JvmStatic
        @Deprecated("Syntax changed",
            replaceWith = ReplaceWith("BadResponse"))
        val BAD_RESPONSE = BadResponse

        @JvmStatic
        @Deprecated("Syntax changed",
            replaceWith = ReplaceWith("Unauthorized"))
        val UNAUTHORIZED = Unauthorized

        @JvmStatic
        @Deprecated("Syntax changed",
            replaceWith = ReplaceWith("InvalidToken"))
        val INVALID_TOKEN = InvalidToken
    }
}