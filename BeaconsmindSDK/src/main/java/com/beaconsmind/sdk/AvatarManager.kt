package com.beaconsmind.sdk

import com.beaconsmind.api.ImageApi
import com.beaconsmind.api.ProfileApi
import com.beaconsmind.api.models.ImageUploadResponse
import com.beaconsmind.api.models.ImageUploadSettings
import com.beaconsmind.api.models.UpdateAvatarRequest
import com.beaconsmind.sdk.api.KotlinApiCallback
import com.beaconsmind.sdk.logging.TimberBMS
import java.io.File


/**
 * Main class for managing images and user avatar uploaded to beaconsmind server.
 *
 * To obtain an instance call [AvatarManager.getInstance] when BeaconsmindSdk was initialized.
 */
class AvatarManager private constructor() {

    private val imageApi: ImageApi get() = ImageApi(BeaconsmindSdk.getApiClient())
    private val profileApi: ProfileApi get() = ProfileApi(BeaconsmindSdk.getApiClient())

    /**
     * Uploads image file to beaconsmind server.
     *
     * @param imageFile valid local image file
     * @param onResultListener callback invoked when upload image request finishes
     */
    fun uploadImage(imageFile: File, onResultListener: OnResultListener<ImageUploadResponse>) {
        val uploadSettings = ImageUploadSettings().apply {
            generateGrayscale = false
            generateThumbnail = true
            generateThumbnailGrayscale = false
        }

        try {
            imageApi.createAsync(uploadSettings, imageFile, KotlinApiCallback(
                _onSuccess = { result, _, _ ->
                    TimberBMS.d("Image upload success `%s`", result.imagePath)
                    onResultListener.onSuccess(result)
                },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, " Upload image failure")
                    onResultListener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: Exception) {
            TimberBMS.e(e, " Upload image exception")
            onResultListener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Uploads an image file and sets it for current user's avatar.
     *
     * @param imageFile valid local image file
     * @param onResultListener callback invoked when image upload and set avatar requests finish.
     */
    fun updateAvatar(imageFile: File, onResultListener: OnCompleteListener) {
        uploadImage(imageFile, object : OnResultListener<ImageUploadResponse> {
            override fun onSuccess(result: ImageUploadResponse) {
                val request = UpdateAvatarRequest()
                    .thumbnailUrl(result.thumbnailPath)
                    .url(result.imagePath)

                profileApi.updateAvatarAsync(request, KotlinApiCallback(
                    _onSuccess = { profile, _, _ ->
                        onResultListener.onSuccess()
                    },
                    _onFailure = { e, _, _ ->
                        TimberBMS.e(e, " Update avatar failure")
                        onResultListener.onError(Error.NetworkError(e))
                    }
                ))
            }

            override fun onError(error: Error) {
                onResultListener.onError(error)
            }
        })
    }

    /**
     * [AvatarManager] singleton holder.
     */
    companion object {
        @Volatile
        private var INSTANCE: AvatarManager? = null

        /**
         * Returns [AvatarManager] singleton instance.
         *
         * Ensure that you called [BeaconsmindSdk.initialize] before obtaining an instance
         *
         * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
         */
        @Throws(SdkNotInitializedException::class)
        @JvmStatic
        fun getInstance(): AvatarManager = guardInitialization {
            synchronized(this) {
                val checkInstanceAgain = INSTANCE
                if (checkInstanceAgain != null) {
                    checkInstanceAgain
                } else {
                    val created = AvatarManager()
                    INSTANCE = created
                    created
                }
            }
        }
    }
}