package com.beaconsmind.sdk.logging

import org.altbeacon.beacon.logging.Logger


/**
 * Bridges Altbeacon logs to be used by our [TimberBMS] logging setup.
 *
 * Altbeacon logs have different TAGs than sdk: "**BMS.ab.*altbeacon_class_tag***"
 */
internal class TimberToLoggerBridge : Logger {
    override fun v(tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).v(message, *args)
    }

    override fun v(t: Throwable, tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).v(t, message, *args)
    }

    /**
     * Logs AltBeacon debug logs to verbose tree
     */
    override fun d(tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).v(message, *args)
    }

    /**
     * Logs AltBeacon debug logs to verbose tree
     */
    override fun d(t: Throwable, tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).v(t, message, *args)
    }

    override fun i(tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).i(message, *args)
    }

    override fun i(t: Throwable, tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).i(t, message, *args)
    }

    override fun w(tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).w(message, *args)
    }

    override fun w(t: Throwable, tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).w(t, message, *args)
    }

    override fun e(tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).e(message, *args)
    }

    override fun e(t: Throwable, tag: String, message: String, vararg args: Any) {
        TimberAltBeaconBMS.altbeaconTag(tag).e(t, message, *args)
    }
}