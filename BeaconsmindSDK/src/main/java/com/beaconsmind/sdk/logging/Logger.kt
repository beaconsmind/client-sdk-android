package com.beaconsmind.sdk.logging

import com.beaconsmind.sdk.BeaconsmindSdk
import com.beaconsmind.sdk.Constants.LOGTAG
import com.beaconsmind.sdk.Constants.LOGTAG_ALTBEACON
import com.beaconsmind.sdk.logging.Logger.TimberAltBeaconBMS.altbeaconClass
import org.jetbrains.annotations.ApiStatus
import timber.log.Timber


/**
 *   Internal class used for dispatching loggable calls to global Timber object and remote logger.
 *
 *   Doesn't do logging by itself, it requires Timber.Tree to be planted in host app or [RemoteLogger] to be enabled.
 *   Defaults to [Timber.DebugTree] if none other tree is present when initialized in [BeaconsmindSdk.initialize]
 *
 *   @see BeaconsmindSdk.initialize
 *   @see RemoteLogger
 * */
internal sealed class Logger : Timber.Tree() {
    /**
     * Tag associated with all logs dispatched by this class' .[log]
     */
    abstract val TAG: String

    /**
     * Returns whether we should log this [priority] before constructing a message.
     *
     * @param [tag] should always be null since we are not calling [Timber.Tree.tag] when logging
     *
     * @return true if this priority should be logged locally and/or remotely
     */
    final override fun isLoggable(tag: String?, priority: Int): Boolean {
        return shouldLogLocally(priority) || RemoteLogger.shouldLog(priority)
    }

    /**
     * Dispatches loggable logs with a custom [TAG] to a global Timber object and/or remote logging solution.
     *
     * @param tag ignored parameter. We are overriding [TAG] to a log call.
     */
    final override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        // we need to check again if we should log this locally
        if (shouldLogLocally(priority)) {
            Timber.tag(TAG).log(priority, message, t)
        }

        // we need to check again if we should log this remotely
        if (RemoteLogger.shouldLog(priority)) {
            RemoteLogger.log(priority, TAG, message)
        }
    }


    /* Implementation */


    /**
     * Dispatches loggable calls with [LOGTAG] tag
     */
    object TimberBMS : Logger() {
        override val TAG: String = LOGTAG
    }


    /**
     * Dispatches loggable altbeacon calls with [LOGTAG_ALTBEACON].[altbeaconClass] tag
     */
    object TimberAltBeaconBMS : Logger() {
        /**
         * Temporary altbeacon class name holder
         */
        private var altbeaconClass: String = ""

        /**
         * Constructs a tag with altbeacon tag prefix and altbeacon class name
         */
        override val TAG: String
            get() = "${LOGTAG_ALTBEACON}.${altbeaconClass}"

        /**
         * Sets an altbeacon class for next logging call.
         */
        fun altbeaconTag(tag: String): Logger {
            altbeaconClass = tag
            return this
        }
    }

    /**
     *  Internal logging helper
     * */
    companion object {
        /**
         * Returns whether this log [priority] should be logged to a local logger
         */
        private fun shouldLogLocally(priority: Int): Boolean {
            return priority >= BeaconsmindSdk.minLogLevel.priority
        }

        /**
         * Java getter for [Logger.TimberBMS]
         *
         * @see Logger
         */
        @JvmStatic
        @ApiStatus.Internal
        fun TimberBMS() = Logger.TimberBMS
    }
}


/**
 * Internal getter for [Logger.TimberBMS]
 *
 * @see Logger
 */
internal val TimberBMS = Logger.TimberBMS


/**
 * Internal getter for [Logger.TimberAltBeaconBMS] object
 *
 * @see Logger
 */
internal val TimberAltBeaconBMS = Logger.TimberAltBeaconBMS
