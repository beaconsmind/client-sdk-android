package com.beaconsmind.sdk.logging

import android.content.Context
import android.util.Log
import com.beaconsmind.sdk.BeaconsmindSdk
import com.beaconsmind.sdk.PersistenceManager
import com.beaconsmind.sdk.UserManager
import com.beaconsmind.sdk.models.LogLevel
import com.bugfender.sdk.Bugfender

/**
 * Remote logging solution helper.
 *
 * We can't add it as a Timber tree since local [LogLevel] and remote logging don't share the same configuration.
 *
 * Local [BeaconsmindSdk.minLogLevel] is set by the app developer at build/run time while
 * [RemoteLogger._isEnabled] is set by the configuration fetched from beaconsmind suite at runtime.
 */
internal object RemoteLogger {

    /**
     * Indicator if logger was initialized.
     */
    private var _isEnabled: Boolean = false

    private var _logLevel: LogLevel = LogLevel.SILENT

    /**
     * Disables all remote logging
     */
    fun setDisabled() {
        _isEnabled = false
    }

    /**
     * Initializes and enables remote logging.
     *
     * *note:* In production apps this might not log if the client is using Proguard rules to remove log expressions.
     */
    fun enableRemoteLogging(context: Context, apiKey: String, logLevel: LogLevel) {
        if (!_isEnabled) {
            Bugfender.init(context, apiKey, false)
            _isEnabled = true
        }

        _logLevel = logLevel

        // set device identifiers

        Bugfender.setDeviceString("user_id",
            UserManager.getUserIdOrNull()
        )

        Bugfender.setDeviceString("device_uuid",
            PersistenceManager.getInstanceWithContext(context).deviceUUID
        )

        Bugfender.setDeviceString("platform_id",
            PersistenceManager.getInstanceWithContext(context).platformDeviceId
        )
    }

    /**
     * Return whether a message at priority should be logged.
     * @return true if BugFender was initiated and priority is equal/higher than configured priority
     */
    fun shouldLog(priority: Int): Boolean {
        return _isEnabled && priority >= _logLevel.priority
    }


    /**
     * Logs a message to remote logging solution.
     * Doesn't check if logger is enabled.
     * Use [shouldLog] before calling.
     */
    fun log(priority: Int, tag: String, message: String?) {
        if (message == null) return
        when (priority) {
            Log.VERBOSE -> /* filter chatty AltBeacon logs */ when (tag) {
                "BMS.ab.CycledLeScannerForLollipop" -> {
                    if (!message.startsWith("Waiting to start full Bluetooth")) {
                        Bugfender.d(tag, message)
                    }
                }
                "BMS.ab.MonitoringStatus" -> {
                    if (!message.contains("does not match beacon")) {
                        Bugfender.d(tag, message)
                    }
                }
                "BMS.ab.IntentScanCoord" -> {
                    if (!message.contains("Waiting for beacon detection") &&
                        !message.contains("End of scan cycle")
                    )
                        Bugfender.d(tag, message)
                }
                "BMS.ab.StartupBroadcastReceiver" -> {
                    /*
                    * Filter results from BLE intent scan strategy.
                    * In this case BMS.ab.StartupBroadcastReceiver "..does not match beacon:.." might be logged sometimes.
                    * */
                    if (!message.contains("onReceive called in startup broadcast receiver") &&
                        !message.contains("Passive background scan callback type: 1") &&
                        !message.contains("got Android background scan via intent")
                    )
                        Bugfender.d(tag, message)
                }
                "BMS.ab.RangedBeacon" -> return
                "BMS.ab.BeaconParser" -> return
                "BMS.ab.ScanHelper" -> return
                "BMS.ab.ScanState" -> return
                "BMS.ab.AndroidModel" -> return
                "BMS.ab.CurveFittedDistanceCalculator" -> return
                "BMS.ab.RunningAverageRssiFilter" -> return
                "BMS.ab.ModelSpecificDistanceCalculator" -> return
                else -> Bugfender.d(tag, message)
            }
            Log.DEBUG -> Bugfender.d(tag, message)
            Log.INFO -> Bugfender.i(tag, message)
            Log.WARN -> Bugfender.w(tag, message)
            Log.ERROR -> Bugfender.e(tag, message)
            Log.ASSERT -> Bugfender.i(tag, message)
        }
    }
}