package com.beaconsmind.sdk.logging

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import kotlin.time.DurationUnit
import kotlin.time.toDuration


/**
 * Measures & logs network request duration.
 */
internal class RequestDurationInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val t1 = System.nanoTime()

        val response: Response = chain.proceed(request)
        val t2 = System.nanoTime()

        TimberBMS.d("API Request %s %s took %s.",
            request.method,
            response.request.url,
            (t2 - t1).toDuration(DurationUnit.NANOSECONDS)
        )
        return response
    }
}