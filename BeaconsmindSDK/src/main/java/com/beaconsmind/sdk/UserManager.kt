package com.beaconsmind.sdk

import android.os.Build
import com.beaconsmind.api.AccountsApi
import com.beaconsmind.api.AuthApi
import com.beaconsmind.api.NotificationsApi
import com.beaconsmind.api.PasswordApi
import com.beaconsmind.api.client.ApiException
import com.beaconsmind.api.models.*
import com.beaconsmind.sdk.api.KotlinApiCallback
import com.beaconsmind.sdk.internal.RemoteDebuggingManager
import com.beaconsmind.sdk.logging.TimberBMS

/**
 * Main class for authenticating user.
 * Users can be authenticated via beaconsmind backend, using your own authentication mechanism or via 3rd party auth providers (Facebook, Google and Apple):
 *  1. [login] via beaconsmind authentication
 *  2. [createAccount] via beaconsmind authentication
 *  3. [importAccount] via your own authentication mechanism
 *  4. [externalLogin] via 3rd party auth providers (Facebook, Google and Apple)
 *
 * To obtain an instance call [UserManager.getInstance] when BeaconsmindSdk was initialized.
 *
 * @constructor Loads user data from persisted storage.
 */
class UserManager private constructor() {

    init {
        setAccessToken(PersistenceManager.getInstance().accessToken)
    }

    private val accountsApi get() = AccountsApi(BeaconsmindSdk.getApiClient())
    private val authApi get() = AuthApi(BeaconsmindSdk.getApiClient())
    private val passwordApi get() = PasswordApi(BeaconsmindSdk.getApiClient())
    private val notificationsApi get() = NotificationsApi(BeaconsmindSdk.getApiClient())

    /**
     * Access token associated with current user session. *Null* if not authenticated.
     *
     * Setting it persists the token.
     */
    var accessToken: String? = null
        private set(value) {
            field = value
            PersistenceManager.getInstance().accessToken = value
        }

    /**
     * Payload associated with current user session. *Null* if not authenticated.
     */
    var jwtPayload: JwtPayload? = null
        private set

    /**
     * Getter for user auth status. Only authenticated users can access restricted API endpoints.
     */
    val isAuthenticated: Boolean get() = accessToken != null

    /**
     * Current authenticated user's beaconsmind user ID. *Null* if not authenticated.
     */
    val userId: String? get() = jwtPayload?.userId

    /**
     * Sets and persists session access token.
     *
     * @return true if token is valid false otherwise
     */
    private fun setAccessToken(token: String?): Boolean {
        token ?: return false
        val payload = JwtPayload.decode(token) ?: return false
        accessToken = token
        jwtPayload = payload
        return true
    }

    /**
     * Login overload.
     * Performs login and immediately starts listening for beacons
     */
    fun login(username: String?, password: String?, listener: OnCompleteListener) {
        login(username, password, true, listener)
    }

    /**
     * Performs login with beaconsmind [username] and [password] credentials. For login via client credentials see [importAccount]
     *
     * Invokes user logged in touchpoint.
     *
     * @param shouldStartService true if [BeaconListenerService] should automatically start after success.
     *
     * @see [UserManager.importAccount]
     */
    fun login(
        username: String?,
        password: String?,
        shouldStartService: Boolean,
        listener: OnCompleteListener,
    ) {
        try {
            authApi.tokenAsync("password", username, password, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    val token = result.accessToken
                    val done = setAccessToken(token)
                    if (done) {
                        updateDeviceInfo()
                        TouchPointHelper.notifyUserLoggedIn()
                        if (shouldStartService) {
                            BeaconListenerService.getInstance().startListeningBeacons(null)
                        }

                        // update remote debugging when user logs in
                        RemoteDebuggingManager.fetchConfig(BeaconsmindSdk.appContext, true)

                        listener.onSuccess()
                    } else {
                        listener.onError(Error.InvalidToken)
                    }
                },
                _onFailure = { e, statusCode, _ ->
                    TimberBMS.e(e, "Login network error")
                    if (statusCode == STATUS_UNAUTHORIZED) {
                        listener.onError(Error.Unauthorized)
                    } else {
                        listener.onError(Error.NetworkError(e))
                    }
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Login network error")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Create account overload.
     * Creates a new account, login and immediately starts listening for beacons
     */
    fun createAccount(userRequest: CreateUserRequest?, listener: OnCompleteListener) {
        createAccount(userRequest, true, listener)
    }

    /**
     * Creates a new account on beaconsmind backend and starts user session with the access token.
     *
     * Invokes user registered touch point.
     *
     * @param shouldStartService true if [BeaconListenerService] should automatically start after success.
     */
    fun createAccount(
        userRequest: CreateUserRequest?,
        shouldStartService: Boolean,
        listener: OnCompleteListener,
    ) {
        try {
            accountsApi.createUserAsync(userRequest, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    if (result.token == null) {
                        listener.onError(Error.BadResponse)
                        return@KotlinApiCallback
                    }
                    val token = result.token!!.accessToken
                    val done = setAccessToken(token)
                    if (done) {
                        updateDeviceInfo()
                        TouchPointHelper.notifyUserRegistered()
                        if (shouldStartService) {
                            BeaconListenerService.getInstance().startListeningBeacons(null)
                        }

                        // update remote debugging when user logs in
                        RemoteDebuggingManager.fetchConfig(BeaconsmindSdk.appContext, true)

                        listener.onSuccess()
                    } else {
                        listener.onError(Error.InvalidToken)
                    }
                },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "Create account network error")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Create account network error")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Import account overload.
     * Import account login and immediately start listening for beacons
     */
    fun importAccount(userRequest: ImportUserRequest, listener: OnCompleteListener) {
        importAccount(userRequest, true, listener)
    }

    /**
     * Imports an account authenticated via client backend and starts beaconsmind user session.
     * Call after you authenticate the user on your platform.
     *
     * Invokes user logged in touchpoint.
     *
     * @param shouldStartService true if [BeaconListenerService] should automatically start after success.
     */
    fun importAccount(
        userRequest: ImportUserRequest,
        shouldStartService: Boolean,
        listener: OnCompleteListener,
    ) {
        try {
            accountsApi.importUserAsync(userRequest, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    val token = result.accessToken
                    val done = setAccessToken(token)
                    if (done) {
                        updateDeviceInfo()
                        TouchPointHelper.notifyUserLoggedIn()

                        // save imported username
                        PersistenceManager.getInstance().importedUsername = userRequest.email
                        if (shouldStartService) {
                            BeaconListenerService.getInstance().startListeningBeacons(null)
                        }

                        // update remote debugging when user logs in
                        RemoteDebuggingManager.fetchConfig(BeaconsmindSdk.appContext, true)

                        listener.onSuccess()
                    } else {
                        listener.onError(Error.InvalidToken)
                    }
                },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "Import account network error")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Import account network error")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * External login overload.
     * Import account from social media provider, login and immediately start listening for beacons
     */
    fun externalLogin(
        providerType: ProviderType,
        externalAccessToken: String,
        firstName: String? = null,
        lastName: String? = null,
        language: String? = null,
        listener: OnCompleteListener,
    ) {
        externalLogin(
            providerType = providerType,
            externalAccessToken = externalAccessToken,
            firstName = firstName,
            lastName = lastName,
            language = language,
            shouldStartService = true,
            listener = listener
        )
    }

    /**
     * Imports account authenticated via third party (social media) provider.
     * Call after you authenticate user via external providers.
     *
     * Invokes user logged in touchpoint.
     *
     * @param shouldStartService true if [BeaconListenerService] should automatically start after success.
     */
    fun externalLogin(
        providerType: ProviderType,
        externalAccessToken: String,
        firstName: String? = null,
        lastName: String? = null,
        language: String? = null,
        shouldStartService: Boolean,
        listener: OnCompleteListener,
    ) {
        val socialRequest = RegisterViaSocialProviderRequest().apply {
            this.providerType = providerType
            this.externalAccessToken = externalAccessToken
            this.firstName = firstName
            this.lastName = lastName
            this.language = language
        }

        try {
            accountsApi.registerViaSocialProvidersAsync(socialRequest, KotlinApiCallback(
                _onSuccess = { result, _, _ ->
                    val token = result.accessToken
                    val done = setAccessToken(token)
                    if (done) {
                        updateDeviceInfo()
                        TouchPointHelper.notifyUserLoggedIn()

                        // save imported username
                        PersistenceManager.getInstance().importedUsername = result.userId
                        if (shouldStartService) {
                            BeaconListenerService.getInstance().startListeningBeacons(null)
                        }

                        // update remote debugging when user logs in
                        RemoteDebuggingManager.fetchConfig(BeaconsmindSdk.appContext, true)

                        listener.onSuccess()
                    } else {
                        listener.onError(Error.InvalidToken)
                    }
                },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "External login network error: OnFailure")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Import account network error: Catch Exception")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Clears *local* user session cache only. Does **not** unregister device token from push notifications
     */
    @Deprecated("Use logout with OnCompleteListener param")
    fun logout() {
        logoutClearCache()
    }

    /**
     * Clears *local* user session cache.
     */
    private fun logoutClearCache() {
        accessToken = null
        jwtPayload = null
        PersistenceManager.getInstance().clearSession()
        BeaconListenerService.getInstance().logout()
    }

    /**
     * Unregisters device token and clears local user session cache.
     *
     * @param onCompleteListener returns onSuccess if user is logged out from BMS backend.
     */
    fun logout(onCompleteListener: OnCompleteListener) {
        val token = PersistenceManager.getInstance().platformDeviceId
        val unsubscribeRequest = UnsubscribeRequest().apply {
            platformId = token
            platformType = BeaconsmindSdk.config.nativeIdProvider.platformType
        }

        try {
            notificationsApi.unsubscribeAsync(unsubscribeRequest, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    TimberBMS.d("Device token unsubscribed: $token")
                    logoutClearCache()

                    // update remote config when user logs out
                    RemoteDebuggingManager.fetchConfig(BeaconsmindSdk.appContext, true)

                    onCompleteListener.onSuccess()
                },
                _onFailure = { e, statusCode, responseHeaders ->
                    TimberBMS.e(e, "LogoutDevice _onFailure")
                    onCompleteListener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "LogoutDevice exception")
        }
    }


    /**
     * Internal call which uses [NativeIdProvider] to fetch device token.
     */
    private fun updateDeviceInfo() {
        BeaconsmindSdk.config.nativeIdProvider.provideId(object :
            com.beaconsmind.sdk.NativeIdProvider.ProvideIdCallback {
            override fun error(e: Exception?) = TimberBMS.e(e, "Failed to get nativeId")
            override fun success(providedId: String) = updateDeviceInfo(providedId)
        })
    }

    /**
     * Device Info is updated by the SDK upon successful login.
     *
     * Make sure to call it manually after notification token changes. For example in your
     * `FirebaseMessagingService#onNewToken`
     * ```kotlin
     *  override fun onNewToken(s: String) {
     *      UserManager.getInstance().updateDeviceInfo(s)
     *  }
     * ```
     */
    fun updateDeviceInfo(token: String?) {
        if (!isAuthenticated) return

        PersistenceManager.getInstance().platformDeviceId = token

        val subscribeRequest = SubscribeRequest().apply {
            platformId = token
            platformType = BeaconsmindSdk.config.nativeIdProvider.platformType
            phoneOS = PhoneOS.Android
            phoneOsVersion = Build.VERSION.SDK_INT.toString()
            phoneModel = Build.MODEL
            phoneManufacturer = Build.BRAND
        }

        try {
            notificationsApi.subscribeAsync(subscribeRequest, KotlinApiCallback(
                _onSuccess = { result, statusCode, responseHeaders ->
                    TimberBMS.d("Device token subscribed $token")
                },
                _onFailure = { e, statusCode, responseHeaders ->
                    TimberBMS.e(e,
                        "UpdateDeviceInfo _onFailure")
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "UpdateDeviceInfo network exception")
        }
    }

    /**
     * Loads user account details
     */
    fun getAccount(listener: OnResultListener<ProfileResponse>) {
        try {
            accountsApi.getProfileAsync(KotlinApiCallback(
                _onSuccess = { result, _, _ -> listener.onSuccess(result) },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "Get account network error")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Get account network error")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Updates user account. Updates fields that were set on [request] (non-null).
     *
     * For password change see [UserManager.changePassword].
     *
     * @param listener returns new account details when successful.
     *
     * @see [UserManager.changePassword]
     */
    fun updateAccount(
        request: UpdateProfileRequest,
        listener: OnResultListener<ProfileResponse>,
    ) {
        try {
            accountsApi.updateProfileAsync(request, KotlinApiCallback(
                _onSuccess = { result, _, _ -> listener.onSuccess(result) },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "Update account network failure")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Update account network error")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Update account overload.
     */
    fun updateAccount(
        request: UpdateProfileRequest,
        listener: OnCompleteListener,
    ) {
        updateAccount(request, object : OnResultListener<ProfileResponse> {
            override fun onSuccess(result: ProfileResponse) = listener.onSuccess()
            override fun onError(error: Error) = listener.onError(error)
        })
    }

    /**
     * Changes user password for accounts authenticated via beaconsmind backend.
     */
    fun changePassword(
        request: ChangePasswordRequest,
        listener: OnCompleteListener,
    ) {
        try {
            passwordApi.changePasswordAsync(request, KotlinApiCallback(
                _onSuccess = { _, _, _ -> listener.onSuccess() },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "Change password failure")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Change password network error")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Requests password change code to be sent to user's [email].
     * For accounts authenticated via beaconsmind backend.
     */
    fun requestPasswordReset(
        email: String,
        listener: OnCompleteListener,
    ) {
        try {
            passwordApi.deprecatedResetPasswordRequestAsync(email, false, KotlinApiCallback(
                _onSuccess = { _, _, _ -> listener.onSuccess() },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "Request reset password failure")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Request reset password network error")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * Resets account password authorized by code send from [UserManager.requestPasswordReset].
     * For accounts authenticated via beaconsmind backend.
     */
    fun resetPasswordWithCode(
        code: String,
        email: String,
        newPassword: String,
        listener: OnCompleteListener,
    ) {
        try {
            val request = ResetPasswordRequest()
                .token(code)
                .email(email)
                .password(newPassword)

            passwordApi.resetPasswordAsync(request, KotlinApiCallback(
                _onSuccess = { _, _, _ -> listener.onSuccess() },
                _onFailure = { e, _, _ ->
                    TimberBMS.e(e, "Reset password failure")
                    listener.onError(Error.NetworkError(e))
                }
            ))
        } catch (e: ApiException) {
            TimberBMS.e(e, "Reset password network error")
            listener.onError(Error.NetworkError(e))
        }
    }

    /**
     * [UserManager] singleton holder.
     */
    companion object {
        private const val STATUS_UNAUTHORIZED = 401

        @Volatile
        private var INSTANCE: UserManager? = null

        /**
         * Returns [UserManager] singleton instance.
         *
         * Ensure that you called [BeaconsmindSdk.initialize] before obtaining an instance
         *
         * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
         */
        @Throws(SdkNotInitializedException::class)
        @JvmStatic
        fun getInstance(): UserManager = guardInitialization {
            synchronized(this) {
                val checkInstanceAgain = INSTANCE
                if (checkInstanceAgain != null) {
                    checkInstanceAgain
                } else {
                    val created = UserManager()
                    INSTANCE = created
                    created
                }
            }
        }

        /**
         * Return userId if initialized and authenticated. Null otherwise.
         */
        @JvmStatic
        fun getUserIdOrNull(): String? = INSTANCE?.userId
    }

    /**
     * @see com.beaconsmind.sdk.NativeIdProvider
     */
    @Deprecated(
        message = "Moved to other package.",
        replaceWith = ReplaceWith("com.beaconsmind.sdk.NativeIdProvider"),
        level = DeprecationLevel.ERROR
    )
    interface NativeIdProvider {
        @Deprecated(
            "Moved to other package.",
            replaceWith = ReplaceWith("com.beaconsmind.sdk.NativeIdProvider.ProvideCallback"),
        )

        interface ProvideCallback {
            fun error(e: Exception?)
            fun success(providedId: String)
        }

        val className: String
        val platformType: PlatformType
        fun provideId(callback: ProvideCallback)
    }
}