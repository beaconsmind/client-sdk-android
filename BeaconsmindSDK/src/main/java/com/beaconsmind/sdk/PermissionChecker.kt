package com.beaconsmind.sdk

import android.app.Activity
import android.content.Context
import android.os.Parcelable
import androidx.activity.result.ActivityResultCaller
import com.beaconsmind.sdk.permissions.IPermissionTexts
import com.beaconsmind.sdk.permissions.PermissionCheckerImpl
import com.beaconsmind.sdk.permissions.SdkPermissionsHelper
import kotlinx.parcelize.Parcelize

/**
 * Helper class for prompting the user for required sdk permissions.
 *
 * Automatically starts beacon listening if the user grants required permissions.
 *
 * @param permissionTexts Used to display customized rationale to the user
 * @see SdkPermissionsHelper
 * @see com.beaconsmind.sdk.permissions.PermissionTexts
 */
@Deprecated("Use SdkPermissionHelper instead")
class PermissionChecker<R> @JvmOverloads constructor(
    owner: R, permissionTexts: IPermissionTexts = PermissionTexts(),
) where R : Activity, R : ActivityResultCaller {

    /**
     * Delegating all work to [SdkPermissionsHelper] implementation
     */
    private val permissionCheckerDelegate =
        PermissionCheckerImpl(
            caller = owner,
            permissionTexts = com.beaconsmind.sdk.permissions.PermissionTexts(permissionTexts),
            context = owner, activity = owner)

    @Deprecated("Use SdkPermissionHelper#requestPermissions")
    @JvmOverloads
    fun askForMissingPermission(alwaysAlertOnPermissionsDenied: Boolean = false) {
        permissionCheckerDelegate.askForMissingPermission(alwaysAlertOnPermissionsDenied)
    }

    @Deprecated("Use com.beaconsmind.sdk.permissions.PermissionTexts")
    @Parcelize
    data class PermissionTexts(
        override val permissionsDeniedTitle: String? = "Location permissions denied",
        override val permissionsDeniedText: String = "Without location permissions you won't get exclusive offers. Please grant location permissions in your settings",
        override val permissionsDeniedAction: String = "Grant permissions",
        override val permissionRationaleTitle: String? = null,
        override val permissionRationaleText: String = "This app needs location permissions to provide you with exclusive offers.",
        override val permissionRationaleAction: String = "Grant permissions",
        override val backgroundPermissionTitle: String? = "Background location",
        override val backgroundPermissionText: String = "This app needs background location permission to provide you with exclusive offers even when you are not in the app. Please enable it in phone's settings.",
        override val backgroundPermissionAction: String = "Enable background location",
    ) : IPermissionTexts, Parcelable

    @Deprecated("Use SdkPermissionHelper")
    companion object {

        @Deprecated("Use SdkPermissionHelper")
        @JvmStatic
        fun getMissingPermissions(context: Context): Set<String> =
            SdkPermissionsHelper.getMissingPermissions(context)

        @Deprecated("Use SdkPermissionHelper")
        @JvmStatic
        fun getRequiredPermissions(): Set<String> =
            SdkPermissionsHelper.getRequiredPermissions()
    }
}