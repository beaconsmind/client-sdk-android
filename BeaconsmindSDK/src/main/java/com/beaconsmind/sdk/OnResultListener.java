package com.beaconsmind.sdk;

/**
 * Request callbacks used for interaction with Beaconsmind API via SDK managers.
 *
 * Used with requests that return data. For requests with no data check {@link OnCompleteListener}
 *
 * @see UserManager
 * @see AvatarManager
 * @see OffersManager
 */
public interface OnResultListener<T> {
    /**
     * Invoked when request completes successfully.
     *
     * @param result network response data
     */
    void onSuccess(T result);

    /**
     * Invoked when request error occurs.
     *
     * @param error wrapped network error
     */
    void onError(Error error);
}
