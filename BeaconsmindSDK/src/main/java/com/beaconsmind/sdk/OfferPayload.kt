package com.beaconsmind.sdk

import android.content.Intent


/**
 * Data payload return with offer notification. To obtain an instance use [Companion] helper methods.
 *
 * @property offerId ID associated with the offer
 * @property imageUrl Offer notification image url
 * @property callToActionTitle Title of call to action button
 * @property callToActionLink Link associated with call to action button
 * @property rawPayload key-value pairs contained in the offer. Useful if sending custom notification data.
 */
class OfferPayload private constructor(
    val offerId: Int,
    val imageUrl: String? = null,
    val callToActionTitle: String? = null,
    val callToActionLink: String? = null,
    val rawPayload: Map<String, String>,
) {

    /**
     * Helper for parsing [OfferPayload]
     */
    companion object {
        private const val FCM_OFFER_ID = "offerId"
        private const val FCM_IMG_URL = "offerImageUrl"
        private const val FCM_CALL_TO_ACTION_TITLE = "offerCallToActionTitle"
        private const val FCM_CALL_TO_ACTION_LINK = "offerCallToActionLink"

        /**
         * Parse [OfferPayload] from raw key-value pairs
         */
        @JvmStatic
        fun tryParse(data: Map<String, String>): OfferPayload? {
            val fcmOfferId = data[FCM_OFFER_ID]
            return if (isInteger(fcmOfferId)) {
                OfferPayload(
                    offerId = fcmOfferId!!.toInt(),
                    imageUrl = data[FCM_IMG_URL],
                    callToActionTitle = data[FCM_CALL_TO_ACTION_TITLE],
                    callToActionLink = data[FCM_CALL_TO_ACTION_LINK],
                    rawPayload = data.toMap()
                )
            } else {
                null
            }
        }

        /**
         * Parse [OfferPayload] from intent extras bundle
         */
        @JvmStatic
        fun fromIntent(intent: Intent): OfferPayload? {
            return tryParse(getBundleMap(intent))
        }

        /**
         * Converts intent extras bundle to a map.
         */
        @JvmStatic
        fun getBundleMap(i: Intent): Map<String, String> {
            val b = i.extras ?: return emptyMap()
            val map: MutableMap<String, String> = HashMap()
            val ks = b.keySet()
            for (key in ks) {
                val value = b.getString(key)
                if (value != null) {
                    map[key] = value
                }
            }
            return map
        }

        private fun isInteger(value: String?): Boolean {
            if (value == null) {
                return false
            }
            try {
                val d = value.toInt()
            } catch (nfe: NumberFormatException) {
                return false
            }
            return true
        }
    }
}