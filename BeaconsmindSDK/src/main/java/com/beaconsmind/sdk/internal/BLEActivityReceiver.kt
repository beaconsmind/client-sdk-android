package com.beaconsmind.sdk.internal

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.models.BLEActivityState
import com.beaconsmind.sdk.utils.BLEUtils.getState
import com.beaconsmind.sdk.utils.BLEUtils.isLocationOn

/**
 * Broadcast receiver that observes Bluetooth and Location adapter state.
 * Exposes LiveData with current [BLEActivityState] via [activityLD].
 */
internal class BLEActivityReceiver : BroadcastReceiver() {
    private val mLD: MutableLiveData<BLEActivityState> = MutableLiveData()

    /**
     * Exposes adapters state via LiveData
     */
    val activityLD: LiveData<BLEActivityState>
        get() = mLD

    /**
     * Observers Bluetooth & Location adapter changes and updates the [activityLD]
     */
    override fun onReceive(context: Context, intent: Intent) {
        TimberBMS.d("BLE state changed")
        val action = intent.action
        if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
            val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)
            when (state) {
                BluetoothAdapter.STATE_OFF -> TimberBMS.d("Bluetooth adapter sate turned off")
                BluetoothAdapter.STATE_ON -> TimberBMS.d("Bluetooth adapter state turned on")
            }
        } else if (action == LocationManager.MODE_CHANGED_ACTION) {
            if (isLocationOn(context)) {
                TimberBMS.d("Location state turned on")
            } else {
                TimberBMS.d("Location state turned off")
            }
        }
        val newState = getState(context)
        TimberBMS.d("New BLE state %s", newState)
        mLD.postValue(newState)
    }

    /**
     * Registers this broadcast receiver instance and updates the [activityLD] with current value
     *
     * @see Context.registerReceiver
     */
    fun register(context: Context) {
        mLD.postValue(getState(context))
        context.registerReceiver(this, intentFilter)
    }

    /**
     * Unregisters this broadcast receiver
     */
    fun unregister(context: Context) {
        try {
            context.unregisterReceiver(this)
        } catch (e: IllegalArgumentException) {
            TimberBMS.w(e, "Receiver not registered")
        }
    }

    private companion object {
        /**
         * Returns [IntentFilter] required for this broadcast receiver functionality.
         */
        val intentFilter: IntentFilter
            get() {
                val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
                filter.addAction(LocationManager.MODE_CHANGED_ACTION)
                return filter
            }
    }
}