package com.beaconsmind.sdk.internal

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Context.BATTERY_SERVICE
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.BatteryManager
import android.os.Build
import android.os.SystemClock
import android.text.format.DateUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.work.*
import com.beaconsmind.api.DiagnosticsApi
import com.beaconsmind.api.client.ApiException
import com.beaconsmind.api.models.AppDiagnosticsRequest
import com.beaconsmind.api.models.Metric
import com.beaconsmind.sdk.*
import com.beaconsmind.sdk.api.KotlinApiCallback
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.permissions.SdkPermissionsHelper
import com.beaconsmind.sdk.utils.BLEUtils
import com.beaconsmind.sdk.utils.getAppVersion
import com.beaconsmind.sdk.utils.getPackageInfo
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


/**
 * Helper for sending periodic diagnostic data back to beaconsmind server.
 */
internal object DiagnosticsScheduler {
    private const val JOB_TAG = "DiagnosticsWorkRequest"
    private const val PERIODIC_WORK_NAME = "DiagnosticsWorkRequest.periodic"
    private const val SINGLE_WORK_NAME = "DiagnosticsWorkRequest.single"

    private val networkConstraints
        get() = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()

    /**
     * Sets up periodic diagnostic requests when [intervalSeconds] != null, otherwise clears scheduled diagnostics.
     *
     * @param intervalSeconds period  in which the diagnostics are sent. *null* if disabled.
     */
    fun setupDiagnostics(context: Context, intervalSeconds: Long?) {
        if (intervalSeconds != null) {
            scheduleIndefinitely(context, intervalSeconds)
        } else {
            clearScheduled(context)
        }
    }

    /**
     * This replaces the currently running request so it can push data in interval less than specified (if the sdk initializes again).
     */
    private fun scheduleIndefinitely(context: Context, intervalMinutes: Long) {
        val periodicWork =
            PeriodicWorkRequestBuilder<DiagnosticsWorkRequest>(intervalMinutes, TimeUnit.SECONDS)
                .setConstraints(networkConstraints)
                .addTag(JOB_TAG)
                .build()

        // enqueue periodic request
        WorkManager.getInstance(context).enqueueUniquePeriodicWork(
            PERIODIC_WORK_NAME, ExistingPeriodicWorkPolicy.REPLACE, periodicWork
        )
    }

    /**
     * Removes diagnostics job from work manager queue.
     */
    private fun clearScheduled(context: Context) {
        WorkManager.getInstance(context).cancelUniqueWork(JOB_TAG)
    }
}


/**
 * Collects diagnostics and sends them to beaconsmind server
 *
 * Useful for debugging and verifying the state of the sdk running on end user devices.
 */
internal class DiagnosticsWorkRequest(val appContext: Context, workerParameters: WorkerParameters) :
    CoroutineWorker(appContext, workerParameters) {

    private val diagnosticsApi get() = DiagnosticsApi(BeaconsmindSdk.getApiClient())

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            saveDiagnostics()
        } catch (e: ApiException) {
            TimberBMS.e(e, "Save Diagnostics Error")
            return@withContext Result.failure()
        }
        return@withContext Result.success()
    }


    /**
     * Doesn't send metrics when it doesn't exist, or when it is in a "normal" state.
     *
     * location -> null
     * missingPermissions -> [emptyList]
     */
    private suspend fun saveDiagnostics() = suspendCoroutine<Unit> {
        val persistenceManager = PersistenceManager.getInstanceWithContext(appContext)

        val diagnosticsRequest = AppDiagnosticsRequest()
            .utcDateTime(
                Date()
            )
            .userId(
                JwtPayload.decode(persistenceManager.accessToken)?.userId
            )
            .uniqueDeviceId(
                persistenceManager.deviceUUID
            )
            .platformDeviceId(
                persistenceManager.platformDeviceId
            )
            .addMetric("platform") { // Detects platform engine the sdk is running on. Can be native (Android), flutter (Android-flutter), or react (Android-react).
                detectPlatform()
            }
            .addMetric("osVersion") { // used to be kernel version but should be Android API level
                Build.VERSION.SDK_INT.toString()
            }
            .addMetric("device") { // device manufacturer + model
                "${Build.MANUFACTURER} ${Build.MODEL}"
            }
            .addMetric("adapters") { // device adapters state; bluetooth_off, location_off, both_disabled,  enabled
                BLEUtils.getState(appContext).toString()
            }
            .addMetric("missingPermissions") { // list of missing permissions required for normal sdk functioning
                val notificationPermission: List<String> =
                    if (Build.VERSION.SDK_INT >= 33 /* Tiramisu */) {
                        // to keep the compile SDK level 31
                        "android.permission.POST_NOTIFICATIONS".takeIf {
                            appContext.checkSelfPermission(it) != PackageManager.PERMISSION_GRANTED
                        }?.let { listOf(it) } ?: emptyList()
                    } else emptyList()

                val missing =
                    SdkPermissionsHelper.getMissingPermissions(appContext) + notificationPermission
                return@addMetric if (missing.isNotEmpty()) {
                    missing.joinToString()
                } else null
            }
            .addMetric("battery") { // battery level
                appContext.getBatteryLevel()
            }
            .addMetric("isBackgroundRestricted") { // Detects if OS/user placed this app in background restriction mode.Doesn't guarantee the app is not forcefully killed. It depends on manufacturer's implementation
                appContext.isBackgroundRestricted().toString()
            }
            .addMetric("sdkVersion") { // beaconsmind SDK
                BuildConfig.SDK_VERSION
            }
            .addMetric("packageName") {
                appContext.packageName
            }
            .addMetric("appVersion") { // app version in which the sdk is running
                getAppVersion(appContext)
            }
            .addMetric("targetSdkVersion") { // android sdk version the app is targeting
                appContext.getPackageInfo().applicationInfo.targetSdkVersion.toString()
            }
            .addMetric("compileSdkVersion") { // android sdk version the app was compiled against
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    val compileSdkVersion =
                        appContext.getPackageInfo().applicationInfo.compileSdkVersion
                    if (compileSdkVersion > 0) {
                        compileSdkVersion.toString()
                    } else null // not specified
                } else {
                    null // not available on API versions  < S"
                }
            }
            .addMetric("location") { // last location without generating an active location request. It currently doesn't send exact location to keep user's privacy, just location information age if it was present. not sent if unknown
                appContext.getLastLocation()
            }
            .addMetric("serviceStarted") { // true if developer called start service, and we persisted it to keep it running
                persistenceManager.isServiceActive.toString()
            }
            .addMetric("serviceRunning") { // true if service started successfully for current app process
                try {
                    BeaconListenerService.getInstance().isRunning.toString()
                } catch (e: Exception) {
                    false.toString()
                }
            }
            .addMetric("isInitialized") { // true if sdk was initialized correctly
                BeaconsmindSdk.isInitialized.toString()
            }
            .addMetric("passiveScanning") {
                try {
                    BeaconsmindSdk.config.usePassiveScanning.toString()
                } catch (e: BeaconsmindException) {
                    null // not initialized
                }
            }
            .addMetric("inBackground") { // sdk was initialized and the app is currently in background
                BeaconsmindSdk.inBackground?.toString()
            }
            .addMetric("processId") {
                android.os.Process.myPid().toString()
            }
            .addMetric("processExitInfo") {
                appContext.getProcessExitInfo()
            }

        TimberBMS.i("Sending device diagnostics: ${diagnosticsRequest.metrics}")

        try {
            diagnosticsApi.saveDiagnosticsAsync(diagnosticsRequest, KotlinApiCallback(
                _onSuccess = { _, _, _ ->
                    it.resumeWith(kotlin.Result.success(Unit))
                },
                _onFailure = { e, _, _ ->
                    it.resumeWithException(e)
                }
            ))
        } catch (e: ApiException) {
            it.resumeWithException(e)
        }
    }
}

/**
 * Helper [AppDiagnosticsRequest] builder extension function.
 *
 * Adds metric entry only if the value is non-null
 *
 * @receiver [AppDiagnosticsRequest]
 */
private inline fun AppDiagnosticsRequest.addMetric(
    key: String,
    lambda: () -> String?,
): AppDiagnosticsRequest {
    val value = lambda()
    if (value != null) {
        addMetricsItem(
            Metric().key(key).value(value)
        )
    }
    return this
}

/**
 * Detects platform engine the sdk is running on.
 *
 * Can be native (Android), flutter (Android-flutter), or react (Android-react).
 */
private fun detectPlatform(): String {
    val flutter = try {
        // flutter engine should have this on classpath
        Class.forName("io.flutter.embedding.engine.FlutterJNI")
        "Android-flutter"
    } catch (e: ClassNotFoundException) {
        null
    }

    val react = try {
        Class.forName("com.facebook.react.ReactInstanceManager");
        "Android-react"
    } catch (e: ClassNotFoundException) {
        null
    }

    val xamarin = try {
        Class.forName("mono.MonoRuntimeProvider")
        "Android-xamarin"
    } catch (e: ClassNotFoundException) {
        null
    }

    return flutter ?: react ?: xamarin ?: "Android"
}


/**
 * Detects if OS/user placed this app in background restriction mode.
 *
 * Doesn't guarantee the app is not forcefully killed. It depends on manufacturer's implementation (mercy).
 */
private fun Context.isBackgroundRestricted(): Boolean {
    val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        am.isBackgroundRestricted
    } else {
        false
    }
}

/**
 * Gets current phone battery level
 */
private fun Context.getBatteryLevel(): String {
    val bm = getSystemService(BATTERY_SERVICE) as BatteryManager
    return bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY).toString()
}

/**
 * Tries to fetch the last location without generating an active location request.
 *
 * It currently doesn't send exact location to keep user's privacy.
 */
@SuppressLint("MissingPermission")
private fun Context.getLastLocation(): String? {
    val locationManager = getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager

    return try {
        /**
         * [LocationManager.PASSIVE_PROVIDER] is always available and it will pick up last user location
         * without initiating a new search
         */
        return locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)?.let {
            /**
             * We can only get the time since location was obtained correctly since
             * device clock can be false/
             */
            val age =
                (SystemClock.elapsedRealtimeNanos() - it.elapsedRealtimeNanos) / 1_000_000_000

            val location = JsonObject().apply {
                // location gathering disabled in order to respect user's privacy.
                // TODO: Enable enable location sharing with a network/local config
//                addProperty("provider", it.provider)
//                addProperty("lat", it.latitude)
//                addProperty("lng", it.longitude)
                addProperty("age", DateUtils.formatElapsedTime(age))
            }

            return location.toString()
        }
    } catch (e: Exception) {
        // permissions missing or provider not available
        null
    }
}

/**
 * Holds a reference to the last process exit info so it's not sent if the same process is still running.
 */
private var _lastExitInfoTimestamp: Long? = null

/**
 * Return previous process exit info if it is available (API >= 30).
 * Sends it only on SDK initialization since [_lastExitInfoTimestamp] will be set on consecutive
 * calls if we are still in the same process
 */
private fun Context.getProcessExitInfo(): String? {
    val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        val exitInfo = am.getHistoricalProcessExitReasons(null, 0, 1).getOrNull(0)
        // We will send exit info if this is the first diagnostics request for current process
        if (_lastExitInfoTimestamp == null) {
            exitInfo?.let {
                _lastExitInfoTimestamp = it.timestamp
                "${it.reason} (${it.description})"
            }
        } else null
    } else null
}
