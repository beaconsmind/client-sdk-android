package com.beaconsmind.sdk.internal

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.beaconsmind.api.models.TouchpointType
import com.beaconsmind.sdk.TouchPointHelper
import com.beaconsmind.sdk.UserManager

/**
 * Tracks application lifecycle to determine if the app (SDK) is running in foreground or background
 */
internal class ForegroundObserver : LifecycleObserver {

    /**
     * Current app background state.
     */
    var inBackground = true
        private set

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onEnterBackground() {
        inBackground = true
    }

    /**
     * The SDK sends [TouchpointType.AppOpen] on app open.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onEnterForeground() {
        inBackground = false
        if (UserManager.getInstance().isAuthenticated) {
            TouchPointHelper.notifyAppOpened()
        }
    }
}