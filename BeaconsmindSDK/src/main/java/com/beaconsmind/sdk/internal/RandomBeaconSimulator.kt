package com.beaconsmind.sdk.internal

import android.os.SystemClock
import com.beaconsmind.api.models.StoreBeaconInfo
import com.beaconsmind.sdk.logging.TimberBMS
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.simulator.BeaconSimulator
import java.util.*

internal class RandomBeaconSimulator : BeaconSimulator {
    private var lastInvocation: Long = 0
    private var allBeacons: List<StoreBeaconInfo> = emptyList()
    private var activeBeacons: List<Beacon> = emptyList()
    private val random = Random()

    fun setBeacons(beacons: List<StoreBeaconInfo>) {
        allBeacons = beacons
    }

    private fun changeActiveBeacons() {
        val ranIndex = random.nextInt(allBeacons.size)
        val sb = allBeacons[ranIndex]
        TimberBMS.d("Simulating beacon contact: %s:%s:%s", sb.uuid, sb.major, sb.minor)
        activeBeacons = listOf(
            Beacon.Builder()
                .setId1(sb.uuid)
                .setId2(sb.major)
                .setId3(sb.minor)
                .setRssi(-50 - random.nextInt(50))
                .build()
        )
    }

    override fun getBeacons(): List<Beacon> {
        val currTime = SystemClock.elapsedRealtime()
        val elapsedTime = (currTime - lastInvocation).toDouble()
        if (elapsedTime > 30000 && allBeacons.isNotEmpty()) {
            changeActiveBeacons()
            lastInvocation = currTime
        }
        return activeBeacons
    }
}