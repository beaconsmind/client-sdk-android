package com.beaconsmind.sdk.internal

import android.content.Context
import androidx.work.*
import com.beaconsmind.api.ConfigurationApi
import com.beaconsmind.api.client.ApiException
import com.beaconsmind.api.models.StoreBeaconInfo
import com.beaconsmind.sdk.*
import com.beaconsmind.sdk.api.KotlinApiCallback
import com.beaconsmind.sdk.logging.TimberBMS
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


/**
 * Helper for scheduling periodic beacon fetching.
 */
internal object FetchBeaconsJobScheduler {
    val context get() = BeaconsmindSdk.appContext
    private const val JOB_TAG = "FetchBeaconsJobScheduler"

    /**
     * Schedules a [FetchBeaconsWorkRequest] using [WorkManager] when there is network connection.
     *
     * Replaces current job so it will always update beacons when it is called.
     */
    fun scheduleNext(context: Context) {
        val constraints =
            Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()

        val periodicBuilder =
            PeriodicWorkRequestBuilder<FetchBeaconsWorkRequest>(
                Constants.BEACON_REFRESH_INTERVAL,
                TimeUnit.MILLISECONDS
            )
                .setConstraints(constraints)
                .addTag(JOB_TAG)

        // enqueue periodic request
        WorkManager.getInstance(context)
            .enqueueUniquePeriodicWork(
                JOB_TAG,
                ExistingPeriodicWorkPolicy.REPLACE,
                periodicBuilder.build()
            )
    }


    /**
     * Stops periodic requests when beacon scanning is canceled.
     */
    fun cancelJobs(context: Context) {
        WorkManager.getInstance(context).cancelUniqueWork(JOB_TAG)
    }
}


/**
 * Fetches beacons using [ConfigurationApi] and passes them to [BeaconListenerService]
 */
internal class FetchBeaconsWorkRequest(appContext: Context, workerParameters: WorkerParameters) :
    CoroutineWorker(appContext, workerParameters) {

    private val configApi get() = ConfigurationApi(BeaconsmindSdk.getApiClient())

    override suspend fun doWork(): Result {
        val beacons = try {
            fetchBeacons()
        } catch (e: ApiException) {
            TimberBMS.e(e, "Fetch Beacons Error")
            return Result.failure()
        }

        BeaconListenerService.getInstance().setBeacons(beacons)
        return Result.success()
    }


    /**
     * Executes [ConfigurationApi.getBeaconsAsync] network request
     */
    private suspend fun fetchBeacons() = suspendCoroutine<List<StoreBeaconInfo>> {
        try {
            configApi.getBeaconsAsync(KotlinApiCallback(
                _onSuccess = { result, _, _ ->
                    TimberBMS.d("Received %d beacons from backend", result.size)
                    it.resumeWith(kotlin.Result.success(result))
                },
                _onFailure = { e, _, _ ->
                    it.resumeWithException(e)
                }
            ))
        } catch (e: ApiException) {
            it.resumeWithException(e)
        }
    }
}