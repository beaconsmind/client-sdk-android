package com.beaconsmind.sdk.internal

import android.annotation.TargetApi
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.beaconsmind.sdk.BeaconListenerService
import com.beaconsmind.sdk.models.BLEActivityState


/**
 *  Workaround helper for a know bug in altbeacon-lib v2.19.4
 *  [issue/1092](https://github.com/AltBeacon/android-beacon-library/issues/1092)
 *
 *  If bluetooth scan is started when bluetooth adapter is turned off,
 *  the search won't get any results. Android specific thing.
 * */
@TargetApi(Build.VERSION_CODES.O)
internal class PassiveBleScanHelper(private val restartListener: () -> Unit) :
    Observer<BLEActivityState> {

    private var liveData: LiveData<BLEActivityState>? = null

    private lateinit var prevState: BLEActivityState

    /**
     * Tracks weather BLE scan is possible given [liveData] observing.
     */
    private var isPossible = false
        private set


    /**
     * Starts observing [liveData] indefinitely. Should be called when [BeaconListenerService] starts beacon listening.
     *
     * @param liveData adapters state livedata
     */
    fun onStartListening(liveData: LiveData<BLEActivityState>) {
        Handler(Looper.getMainLooper()).post {
            synchronized(this) {
                this.liveData?.removeObserver(this)
                this.liveData = liveData
                liveData.observeForever(this)
            }
        }
    }

    /**
     * Should be called when [BeaconListenerService] stops beacon listening.
     *
     * Removes registered [liveData] observer
     */
    fun onStopListening() {
        Handler(Looper.getMainLooper()).post {
            synchronized(this) {
                liveData?.removeObserver(this)
            }
        }
    }

    /**
     * Monitors [BLEActivityState] and invokes [restartListener] if BLE scan restart is required.
     *
     * Restart is required if BLE scan started while adapters were disabled and are now enabled.
     */
    override fun onChanged(state: BLEActivityState) {
        if (::prevState.isInitialized.not()) {
            // first state we receive is enabled so we don't have to restart
            if (state == BLEActivityState.ENABLED) {
                isPossible = true
            }
        } else if (prevState != BLEActivityState.ENABLED && state == BLEActivityState.ENABLED) {
            isPossible = true
            restartListener()
        } else if (state != BLEActivityState.ENABLED) {
            isPossible = false
        }

        prevState = state
    }
}