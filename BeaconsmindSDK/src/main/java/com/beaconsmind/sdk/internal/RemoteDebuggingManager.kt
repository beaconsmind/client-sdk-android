package com.beaconsmind.sdk.internal

import android.content.Context
import androidx.work.*
import com.beaconsmind.api.ConfigurationApi
import com.beaconsmind.api.client.ApiException
import com.beaconsmind.api.models.PhoneOS
import com.beaconsmind.sdk.BeaconsmindSdk
import com.beaconsmind.sdk.JwtPayload
import com.beaconsmind.sdk.PersistenceManager
import com.beaconsmind.sdk.api.KotlinApiCallback
import com.beaconsmind.sdk.logging.RemoteLogger
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.models.RemoteDebugConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


/**
 * Manages remote SDK debugging related tasks.
 * Fetches remote config and applies the settings to [DiagnosticsScheduler] and [RemoteLogger]
 */
internal object RemoteDebuggingManager {
    private const val JOB_TAG = "RemoteConfigScheduler"

    /**
     * Restores config from persisted storage so the logging/diagnostics can start right away.
     */
    fun restoreConfig(context: Context) {
        PersistenceManager.getInstanceWithContext(context).remoteConfig?.let {
            applyRemoteConfig(context, it)
        }
    }

    /**
     * Schedules a [RemoteConfigWorkRequest] using [WorkManager] when network connection is available.
     *
     * Replaces current job so it will always fetch configuration when it is called.
     *
     * @param forceRestart true if config should be updated even if it didn't change
     */
    fun fetchConfig(context: Context, forceRestart: Boolean) {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val oneTimeWork =
            OneTimeWorkRequestBuilder<RemoteConfigWorkRequest>()
                .setConstraints(constraints)
                .addTag(JOB_TAG)
                .setInputData(Data.Builder()
                    .putBoolean(RemoteConfigWorkRequest.KEY_FORCE, forceRestart)
                    .build()
                )
                .build()

        // enqueue single request
        WorkManager.getInstance(context).enqueueUniqueWork(
            JOB_TAG, ExistingWorkPolicy.REPLACE, oneTimeWork
        )
    }
}


/**
 * Applies the [config] to [DiagnosticsScheduler] and [RemoteLogger]
 *
 * @param config a config to apply
 */
private fun applyRemoteConfig(context: Context, config: RemoteDebugConfig) {
    // configure diagnostics
    DiagnosticsScheduler.setupDiagnostics(context, config.metricsIntervalSeconds?.toLong())

    // configure remote logging
    when (val key = config.loggingKey) {
        null -> RemoteLogger.setDisabled()
        else -> RemoteLogger.enableRemoteLogging(context, key, config.getLogLevel())
    }
}


/**
 * Fetches app config using [ConfigurationApi] and sets up remote logging and diagnostics
 */
internal class RemoteConfigWorkRequest(
    val appContext: Context,
    workerParameters: WorkerParameters,
) : CoroutineWorker(appContext, workerParameters) {
    companion object {
        /**
         * Input data key for Boolean value if config should be updated even if it didn't change
         */
        const val KEY_FORCE = "FORCE"
    }

    private val configApi get() = ConfigurationApi(BeaconsmindSdk.getApiClient())

    /**
     * Fetches the config, applies and persists it if changed from last fetch or if it's a force update
     */
    override suspend fun doWork(): Result {
        try {
            val config = fetchConfig()

            val oldConfig = PersistenceManager
                .getInstanceWithContext(applicationContext)
                .remoteConfig

            // apply config if changed or if its force update
            if (config != oldConfig || inputData.getBoolean(KEY_FORCE, false)) {
                PersistenceManager.getInstanceWithContext(applicationContext).remoteConfig = config
                withContext(Dispatchers.Main) {
                    applyRemoteConfig(applicationContext, config)
                }
            }
        } catch (e: ApiException) {
            TimberBMS.e(e, "Fetch AppConfig Error")
            return Result.failure()
        }

        return Result.success()
    }

    /**
     * Executes network request and returns the result
     */
    private suspend fun fetchConfig() = suspendCoroutine<RemoteDebugConfig> {
        configApi.getAppConfigurationAsync(
            JwtPayload.decode(PersistenceManager.getInstanceWithContext(appContext).accessToken)?.userId,
            PhoneOS.Android,
            appContext.packageName,
            KotlinApiCallback(
                _onSuccess = { result, _, _ ->
                    TimberBMS.i("Received remote config $result")
                    it.resumeWith(kotlin.Result.success(RemoteDebugConfig(result)))
                },
                _onFailure = { e, _, _ -> it.resumeWithException(e) }
            ))
    }
}
