package com.beaconsmind.sdk.internal

import com.beaconsmind.api.models.EventModel
import com.beaconsmind.api.models.StoreBeaconInfo
import com.beaconsmind.sdk.models.BeaconContactsSummary


/**
 * Keeps track of beacon contacts.
 *
 * Adds new beacons and updates already contained ones.
 */
internal class BeaconContactsSummaryList : Iterable<BeaconContactsSummary> {
    private val map = HashMap<String, BeaconContactsSummary>()

    /**
     * Adds a new beacon to map
     */
    fun addBeacon(beacon: StoreBeaconInfo) {
        val key = String.format("%s:%s:%s", beacon.uuid, beacon.major, beacon.minor)
        if (!map.containsKey(key)) {
            map[key] = BeaconContactsSummary(beacon)
        }
    }

    /**
     * Adds beacon contacts to beacon summary.
     */
    fun addBeaconContacts(events: List<EventModel>) {
        events.forEach { addBeaconContact(it) }
    }


    /**
     * Adds beacon contacts if summary exists.
     */
    private fun addBeaconContact(event: EventModel) {
        val key = String.format("%s:%s:%s", event.uuid, event.major, event.minor)
        val beaconSummary = map[key]
        if (beaconSummary != null) {
            beaconSummary.addNewBeaconContact(event)
        }
    }

    /**
     * Clears all beacons
     */
    fun clear() {
        map.clear()
    }

    override fun iterator(): MutableIterator<BeaconContactsSummary> {
        return map.values.iterator()
    }
}