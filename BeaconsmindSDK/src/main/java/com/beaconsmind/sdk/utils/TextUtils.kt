package com.beaconsmind.sdk.utils

import android.os.Build
import android.text.Html
import android.text.Spanned

/**
 * Text transformatios utils
 */
object TextUtils {
    /**
     *  Converts html string into a [Spanned] CharSequence
     */
    @JvmStatic
    fun fromHtml(html: String?): Spanned? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(html)
        }
    }
}