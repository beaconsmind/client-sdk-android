package com.beaconsmind.sdk.utils

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.ActivityResultCaller
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.beaconsmind.sdk.BeaconsmindSdk
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.permissions.SdkPermissionsHelper

/**
 * Lifecycle callbacks object attached to [Application.registerActivityLifecycleCallbacks] for
 * monitoring explicit permission settings.
 */
private var lifecycleCallbacks: Application.ActivityLifecycleCallbacks? = null


/**
 * Development helper extension on [BeaconsmindSdk] receiver
 *
 * Attaches [Application.ActivityLifecycleCallbacks] and asks for permissions on every Activity#onCreate
 * Shows toast if bluetooth and location are not enabled in device's settings
 *
 * **required** your Activity needs to implement [ActivityResultCaller] interface.
 *
 * @receiver [BeaconsmindSdk]
 */
fun BeaconsmindSdk.startExplicitMode(context: Context) {
    if (lifecycleCallbacks != null) return // callbacks are already registered.

    val app = context.applicationContext as Application

    if (SdkPermissionsHelper.getMissingPermissions(context).isNotEmpty()) {
        // try to get permissions from current context
        requestPermissions(context)
    }

    // register lifecycle callback so we can enforce permissions granted
    // and bluetooth + location turned ON

    lifecycleCallbacks = object : Application.ActivityLifecycleCallbacks {

        /**
         * Registers [SdkPermissionsHelper]'s permission requesting mechanism on every created Activity.
         */
        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            if (SdkPermissionsHelper.getMissingPermissions(context).isNotEmpty()) {
                requestPermissions(activity)
            }
        }

        /**
         * Notifies the developer if BLE adapters are disabled.
         */
        override fun onActivityStarted(activity: Activity) {
            if (!BLEUtils.isBluetoothOn || !BLEUtils.isLocationOn(context)) {
                TimberBMS.w("Beacon ranging not possible. Enable bluetooth and location for beacon testing.")
                Toast.makeText(
                    activity,
                    "beaconsmindSdk: enable bluetooth and location",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        override fun onActivityResumed(activity: Activity) = Unit
        override fun onActivityPaused(activity: Activity) = Unit
        override fun onActivityStopped(activity: Activity) = Unit
        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) = Unit
        override fun onActivityDestroyed(activity: Activity) = Unit
    }

    // register callbacks
    app.registerActivityLifecycleCallbacks(lifecycleCallbacks)
}

/**
 * Tires to invoke [SdkPermissionsHelper]'s request permission implementation on a context.
 *
 * Successful if [context] is [AppCompatActivity] or [FragmentActivity].
 */
private fun requestPermissions(context: Context) {
    if (context !is Activity) {
        return
    }

    when (context) {
        is AppCompatActivity -> SdkPermissionsHelper.requestPermissions(context, true)
        is FragmentActivity -> SdkPermissionsHelper.requestPermissions(context, true)
        else -> {
            TimberBMS.e("Unsupported Activity base class, permissions are not requested. " +
                    "Please use AppCompatActivity or FragmentActivity")
        }
    }
}
