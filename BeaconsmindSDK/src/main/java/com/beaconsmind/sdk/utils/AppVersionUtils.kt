package com.beaconsmind.sdk.utils

import android.content.Context
import android.content.pm.PackageInfo
import androidx.core.content.pm.PackageInfoCompat


/**
 * Fetches host app version using [PackageInfoCompat] formatted as *VersionName (VersionCode)*
 */
internal fun getAppVersion(context: Context): String {
    val packageInfo = context.getPackageInfo()

    val versionCode: String = PackageInfoCompat.getLongVersionCode(packageInfo).toString()
    val versionName: String? = packageInfo.versionName

    return when {
        versionName == null -> versionCode
        else -> "$versionName ($versionCode)"
    }
}

/**
 * Extension function on [Context] receiver.
 * Returns this application's PackageInfo
 */
internal fun Context.getPackageInfo(): PackageInfo {
    val packageName = packageName
    val packageManager = packageManager
    return packageManager.getPackageInfo(packageName, 0)
}