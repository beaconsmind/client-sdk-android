package com.beaconsmind.sdk.utils

import android.os.Build
import java.util.*

/**
 * Beaconsmind SDK utils
 */
internal object Utils {

    /**
     * Calculates distance based on signal [rssi]
     */
    fun calcDistance(rssi: Double): Double {
        val N = 2.0
        val MP = -69.0
        return Math.round(Math.pow(10.0, (MP - rssi) / (10 * N)) * 100.0) / 100.0
    }

    /**
     * Returns formatted device info.
     *
     * Android *kernel* (*model+manufacturer*) (API *level*)
     */
    val deviceInfo: String
        get() {
            val osVersion = System.getProperty("os.version")
            val model = Build.MODEL
            val brand = Build.BRAND
            return String.format(Locale.US, "Android %s (%s %s) (API %d)",
                osVersion,
                brand,
                model,
                Build.VERSION.SDK_INT)
        }
}