package com.beaconsmind.sdk.utils

import android.app.ActivityManager
import android.content.Context
import android.os.Build
import android.os.PowerManager

/**
 * Utils related to battery optimization, and process longevity. Doesn't guarantee the app is safe.
 */
internal object BatteryRestrictionUtils {

    /**
     * @see PowerManager.isIgnoringBatteryOptimizations
     */
    @JvmStatic
    fun isIgnoringBatteryOptimizations(context: Context): Boolean {
        val pwrm = context.applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
        val name = context.applicationContext.packageName
        return pwrm.isIgnoringBatteryOptimizations(name)
    }

    /**
     * @see ActivityManager.isBackgroundRestricted
     * @since [Build.VERSION_CODES.P]
     */
    @JvmStatic
    fun isBackgroundRestricted(context: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            val activityManager =
                context.getSystemService(Context.ACTIVITY_SERVICE) as? ActivityManager
            activityManager?.isBackgroundRestricted ?: false
        } else {
            false
        }
    }
}