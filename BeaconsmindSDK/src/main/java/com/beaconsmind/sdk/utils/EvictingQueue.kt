package com.beaconsmind.sdk.utils

import java.util.concurrent.ArrayBlockingQueue

/**
 * Evicting Queue implemented as a set.
 */
internal class UniqueEvictingQueue<E : Any>(size: Int) : EvictingQueue<E>(size) {

    /**
     * Adds an element to queue if it doesn't exist
     */
    override fun add(element: E): Boolean {
        return if (this.contains(element)) {
            false
        } else super.add(element)
    }
}


/**
 * FIFO queue with limited size
 */
internal open class EvictingQueue<E : Any>(override val size: Int) : ArrayBlockingQueue<E>(size) {
    var last: E? = null
        private set

    @Synchronized
    override fun addAll(elements: Collection<E>): Boolean {
        for (item in elements) {
            add(item)
        }
        return !elements.isEmpty()
    }

    @Synchronized
    override fun add(element: E): Boolean {
        // Check if queue full already?
        if (super.size == size) {
            // remove element from queue if queue is full
            this.remove()
        }
        last = element
        return super.add(element)
    }

    val first: E?
        get() = peek()
}