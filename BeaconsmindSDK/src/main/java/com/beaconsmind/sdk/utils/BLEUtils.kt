package com.beaconsmind.sdk.utils

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.location.LocationManager
import android.os.Build
import com.beaconsmind.sdk.models.BLEActivityState


/**
 * Bluetooth + Location adapters util. Determines if BLE detections are possible.
 *
 * @see [BLEActivityState]
 */
object BLEUtils {

    /**
     * Returns state of BLE adapters (Location + Bluetooth) as enum
     *
     * @see [BLEActivityState]
     */
    @JvmStatic
    fun getState(context: Context): BLEActivityState {
        val bt = isBluetoothOn
        val location = isLocationOn(context)
        return if (!bt && !location) BLEActivityState.DISABLED else if (!bt) BLEActivityState.BT_OFF else if (!location) BLEActivityState.LOCATION_OFF else BLEActivityState.ENABLED
    }

    /**
     * Returns the state of Bluetooth adapter
     */
    @JvmStatic
    val isBluetoothOn: Boolean
        get() {
            val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            return mBluetoothAdapter?.isEnabled ?: false
        }

    /**
     * Returns the state of Location adapter
     */
    @JvmStatic
    fun isLocationOn(context: Context): Boolean {
        val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            lm.isLocationEnabled
        } else {
            lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
            )
        }
    }
}