package com.beaconsmind.sdk.utils

import android.media.MediaDrm
import android.os.Build
import android.util.Base64
import java.util.*


/**
 * Device UUID helper.
 */
internal object UniqueDeviceID {

    /**
     * Tries to get stable device UUID which is the same across app re-installs. Falls back to random UUID
     */
    fun getDeviceUUID(): String {
        return getWidevineId() ?: UUID.randomUUID().toString()
    }

    /**
     * Unique device ID across app re-installs. Could possibly be null in some cases??
     *
     * UUID for the Widevine DRM scheme.
     *
     * Widevine is supported on Android devices running Android 4.3 (API Level 18) and up.
     *
     * StackOverflow [discussion](https://stackoverflow.com/questions/58103580/android-10-imei-no-longer-available-on-api-29-looking-for-alternatives)
     */
    private fun getWidevineId(): String? {

        val WIDEVINE_UUID = UUID(-0x121074568629b532L, -0x5c37d8232ae2de13L)
        var wvDrm: MediaDrm? = null
        try {
            wvDrm = MediaDrm(WIDEVINE_UUID)
            val widevineId = wvDrm.getPropertyByteArray(MediaDrm.PROPERTY_DEVICE_UNIQUE_ID)
            return Base64.encodeToString(widevineId, Base64.DEFAULT)
        } catch (e: Exception) {
            //WIDEVINE is not available
            return null
        } finally {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                wvDrm?.close()
            } else {
                wvDrm?.release()
            }
        }
    }
}