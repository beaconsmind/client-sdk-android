package com.beaconsmind.sdk.permissions

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.beaconsmind.sdk.BeaconListenerService
import com.beaconsmind.sdk.PersistenceManager
import com.beaconsmind.sdk.logging.TimberBMS


/**
 * Helper class for prompting the user for required sdk permission using [ActivityResultCaller] API.
 *
 * Automatically starts beacon listening if the user grants required permissions.
 *
 * @param permissionTexts Used to display customized rationale to the user
 * @param context required context
 * @param caller component which will handle ActivityResultCaller API
 * @param activity activity to determine shouldShowRequestPermissionRationale, can be null in that case we rely on SharedPreferences
 */
internal class PermissionCheckerImpl internal constructor(
    // fragments as caller don't implement Context
    private val caller: ActivityResultCaller,
    permissionTexts: IPermissionTexts = PermissionTexts(),
    // so you have to provide Context separately (it will fail without valid context)
    private val context: Context = caller as Context,
    // activity is used to determine if one should display permissions rationale
    private val activity: Activity? = caller as? Activity,
) : IPermissionTexts by permissionTexts {

    private val backgroundPermissionLauncher: ActivityResultLauncher<String>
    private val requestMultiplePermissionsLauncher: ActivityResultLauncher<Array<String>>
    private val settingsPermissionsLauncher: ActivityResultLauncher<Intent>

    private val persistenceManager get() = PersistenceManager.getInstanceWithContext(context)

    /**
     * Helper for determining PermissionDenied status
     */
    private enum class Result {
        SHOW_RATIONALE, DENIED
    }

    /**
     * Notify activity caller that permission checker's flow is completely over
     * so that he can start his own flow.
     */
    private fun invokeActivityCallback() {
        val callback = activity as? SdkPermissionsHelper.ActivityResultCallback
        callback?.onBeaconsmindPermissionsResult(
            startPossible = SdkPermissionsHelper.canStartBeaconService(context)
        )
    }

    /**
     * Displays alert dialog with denied permissions rationale.
     * Will be shown first time user denied permissions or if
     * [PermissionCheckerImpl.askForMissingPermission] is called with alwaysAlertOnPermissionsDenied = true
     *
     * @see [IPermissionTexts.permissionsDeniedTitle]
     * @see [IPermissionTexts.permissionsDeniedText]
     * @see [IPermissionTexts.permissionsDeniedAction]
     */
    private fun showPermissionDenied() {
        TimberBMS.i("Permissions denied: ${SdkPermissionsHelper.getMissingPermissions(context)}")

        if (alwaysAlertOnPermissionsDenied || !persistenceManager.permissionsPrompted) {
            //request denied, send to settings
            val builder = AlertDialog.Builder(context)
                .setTitle(permissionsDeniedTitle)
                .setMessage(permissionsDeniedText)
                .setOnCancelListener { _ ->
                    TimberBMS.d("Permission rationale canceled for package settings")
                    invokeActivityCallback()
                }
                .setPositiveButton(permissionsDeniedAction) { _, _ ->
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri: Uri = Uri.fromParts("package", context.packageName, null)
                    intent.data = uri

                    // This will take the user to a page where they have to click twice
                    // to drill down to grant the permission
                    settingsPermissionsLauncher.launch(intent)
                }
            persistenceManager.permissionsPrompted = true
            builder.show()
        } else {
            invokeActivityCallback()
        }
    }

    init {
        /**
         * Launcher for handling BLE permissions.
         */
        requestMultiplePermissionsLauncher = caller.registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { result: MutableMap<String, Boolean> ->
            val deniedList: List<String> = result.filter { !it.value }.map { it.key }

            when {
                deniedList.isNotEmpty() -> {
                    val map = deniedList.groupBy { permission ->
                        if (activity.shouldShowRequestPermissionRationale(permission)) Result.SHOW_RATIONALE else Result.DENIED
                    }

                    when {
                        map[Result.DENIED].isNullOrEmpty().not() -> showPermissionDenied()
                        map[Result.SHOW_RATIONALE] != null -> map[Result.SHOW_RATIONALE]?.let {
                            // rationale required , request again
                            AlertDialog.Builder(context)
                                .setTitle(permissionRationaleTitle)
                                .setMessage(permissionRationaleText)
                                .setPositiveButton(permissionRationaleAction) { dialog: DialogInterface?, which: Int ->
                                    askForMissingPermission(alwaysAlertOnPermissionsDenied)
                                }.setOnCancelListener {
                                    TimberBMS.d("Permission rationale canceled for $it")
                                    invokeActivityCallback()
                                }
                                .show()
                        }
                    }

                }
                else -> {
                    // we can start listening for beacons here since the user has already given
                    // proper foreground scanning permissions
                    if (BeaconListenerService.shouldRestartService(context)) {
                        BeaconListenerService.getInstance().startListeningBeacons(null)
                    }

                    // check for background location permission
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        askForBackgroundLocation()
                    } else {
                        TimberBMS.i("All permissions granted, Android < Q")
                        invokeActivityCallback()
                    }
                }
            }
        }

        /**
         * Launcher for handling [Manifest.permission.ACCESS_BACKGROUND_LOCATION]
         */
        backgroundPermissionLauncher =
            caller.registerForActivityResult(ActivityResultContracts.RequestPermission()) { backgroundGranted ->
                if (backgroundGranted) {
                    TimberBMS.i("All permissions granted")
                    invokeActivityCallback()
                } else {
                    showPermissionDenied()
                }
            }

        /**
         * Launcher for launching app settings when user has denied permissions via permissions API
         */
        settingsPermissionsLauncher = caller.registerForActivityResult(StartActivityForResult()) {
            if (SdkPermissionsHelper.canStartBeaconService(context)) {
                if (BeaconListenerService.shouldRestartService(context)) {
                    BeaconListenerService.getInstance().startListeningBeacons(null)
                }

                // check for background location permission
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    askForBackgroundLocation()
                } else {
                    TimberBMS.i("All permissions granted, Android < Q")
                    invokeActivityCallback()
                }
            } else {
                showPermissionDenied()
            }
        }
    }

    /**
     * Launches background location permission flow separately from others (it has to be requested separately).
     *
     * Shows alert dialog with background permission rationale
     *
     * @see IPermissionTexts.backgroundPermissionTitle
     * @see IPermissionTexts.backgroundPermissionText
     * @see IPermissionTexts.backgroundPermissionAction
     */
    @RequiresApi(Build.VERSION_CODES.Q)
    private fun askForBackgroundLocation() {
        if (context.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (activity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                AlertDialog.Builder(context)
                    .setTitle(backgroundPermissionTitle)
                    .setMessage(backgroundPermissionText)
                    .setPositiveButton(backgroundPermissionAction) { _: DialogInterface?, _: Int ->
                        backgroundPermissionLauncher.launch(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                    }
                    .setOnCancelListener {
                        TimberBMS.d("Permission rationale canceled for background location")
                        invokeActivityCallback()
                    }
                    .show()
            } else {
                backgroundPermissionLauncher.launch(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            }
        } else {
            invokeActivityCallback()
        }
    }

    /**
     * Extension function on nullable [Activity] caller
     *
     * If activity is null it will be false if permissions were already prompted
     */
    private fun Activity?.shouldShowRequestPermissionRationale(permission: String): Boolean {
        val shouldShow = this?.shouldShowRequestPermissionRationale(permission)
        return shouldShow
            ?: (alwaysAlertOnPermissionsDenied || !persistenceManager.permissionsPrompted)
    }

    /**
     * Flag if we should show alert on permissions denied.
     *
     * Persisted in current object to handle internal permission requesting flow.
     */
    private var alwaysAlertOnPermissionsDenied: Boolean = false

    /**
     * Prompts the user for runtime sdk permission. Displays alerts with [PermissionTexts].
     *
     * @param alwaysAlertOnPermissionsDenied display alert every time permissions are denied.
     * If set to false the user will be prompted exactly once.
     * Disable it for production apps since it can get annoying for the user.
     */
    @JvmOverloads
    fun askForMissingPermission(alwaysAlertOnPermissionsDenied: Boolean = false) {
        this.alwaysAlertOnPermissionsDenied = alwaysAlertOnPermissionsDenied

        // we have to filter background location because it is not possible to ask it with others
        val multiPermissions = SdkPermissionsHelper.getRequiredPermissions().let { required ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                required - Manifest.permission.ACCESS_BACKGROUND_LOCATION
            } else {
                required
            }
        }

        requestMultiplePermissionsLauncher.launch(multiPermissions.toTypedArray())
    }
}