package com.beaconsmind.sdk.permissions


import android.app.Activity
import android.os.Bundle
import android.os.Parcelable
import androidx.activity.result.ActivityResultCaller
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.beaconsmind.sdk.logging.TimberBMS


/**
 * Workaround for android's [ActivityResultCaller] API limitations which requires
 * [ActivityResultCaller.registerForActivityResult] to be called before Activity/Fragment's
 * onStart lifecycle callback.
 *
 * It will use activity's [androidx.fragment.app.FragmentManager] to inflate an empty [DummyPermissionsFragment]
 * that will take care of asking for permissions using [ActivityResultCaller] API.
 */
internal fun _askPermissionsWorkaround(
    activity: Activity,
    fm: FragmentManager,
    texts: IPermissionTexts,
    always: Boolean,
) {
    /**
     * Find if fragment was already added, it will be recreated from Activity's savedInstanceState
     */
    val fragment = fm.findFragmentByTag(FRAGMENT_TAG) as? DummyPermissionsFragment

    if (SdkPermissionsHelper.getMissingPermissions(activity).isEmpty()) {
        /**
         * If fragment was added, remove it from the hierarchy so it doesn't get re-craeted
         * from savedInstanceState in [Activity.onCreate]
         */
        fragment?.let {
            fm.beginTransaction().apply {
                remove(it)
                commit()
            }
            TimberBMS.d("_internalAskPermissions: permissions fragment removed")
        }

        // return result
        (activity as? SdkPermissionsHelper.ActivityResultCallback)?.onBeaconsmindPermissionsResult(true)

        return
    }

    // ask for permissions
    if (fragment != null) {
        TimberBMS.d("_internalAskPermissions: permissions fragment restarted")
        fragment.askPermissions()
    } else {
        /**
         * First time fragment creation. Permissions will be asked since fragment
         * will go trough [DummyPermissionsFragment.onCreate] lifecycle callback
         */
        val permissionTexts = texts as? PermissionTexts ?: PermissionTexts(texts) // wrap IPermissionTexts if using deprecated PermissionChecker.PermissionTexts class
        val dummy = DummyPermissionsFragment.newInstance(permissionTexts, always)
        fm.beginTransaction().apply {
            TimberBMS.d("_internalAskPermissions: permissions fragment created")
            add(dummy, FRAGMENT_TAG)
            commit()
        }
    }
}

private const val FRAGMENT_TAG = "com.beaconsmind.sdk.PERMISSIONS_DUMMY"
private const val KEY_ALWAYS_ALERT = "DummyPermissionsFragment.alwaysAlert"
private const val KEY_PERMISSION_TEXTS = "DummyPermissionsFragment.permissionTexts"


/**
 * Empty fragment which will be added into activity's view hierarchy.
 * It will use ActivityResultCaller API for permissions.
 */
internal class DummyPermissionsFragment : Fragment() {

    private var checker: PermissionCheckerImpl? = null
    private var texts: PermissionTexts? = null
    private var shouldAlwaysAlert: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        texts = requireArguments().getParcelable(KEY_PERMISSION_TEXTS)!!
        shouldAlwaysAlert = requireArguments().getBoolean(KEY_ALWAYS_ALERT)

        val activity = requireActivity()

        checker = PermissionCheckerImpl(
            caller = this,
            permissionTexts = texts!!,
            context = activity,
            activity = activity
        )

        if (savedInstanceState == null) {
            // ask for permissions only the first time fragment was added to activity.
            // if it exists it will be taken care of by the caller.
            askPermissions()
        }
    }

    /**
     * Ask permissions using [ActivityResultCaller]
     */
    fun askPermissions() {
        checker!!.askForMissingPermission(shouldAlwaysAlert!!)
    }

    override fun onDestroy() {
        checker = null
        super.onDestroy()
    }

    companion object {
        fun newInstance(texts: PermissionTexts, always: Boolean) =
            DummyPermissionsFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(KEY_ALWAYS_ALERT, always)
                    putParcelable(KEY_PERMISSION_TEXTS, texts)
                }
            }
    }
}