package com.beaconsmind.sdk.permissions

import android.Manifest.permission.ACCESS_BACKGROUND_LOCATION
import android.app.Activity
import android.os.Parcelable
import com.beaconsmind.sdk.PermissionChecker
import kotlinx.parcelize.Parcelize


/**
 * Strings used by [SdkPermissionsHelper] for prompting user for permissions
 */
interface IPermissionTexts {
    /**
     * AlertDialog title when user denies permissions
     */
    val permissionsDeniedTitle: String?

    /**
     * AlertDialog body when user denies permissions
     */
    val permissionsDeniedText: String

    /**
     * AlertDialog positive action button when user denies permissions
     */
    val permissionsDeniedAction: String

    /**
     * AlertDialog title when [Activity.shouldShowRequestPermissionRationale] returns true
     */
    val permissionRationaleTitle: String?

    /**
     * AlertDialog body when [Activity.shouldShowRequestPermissionRationale] returns true
     */
    val permissionRationaleText: String

    /**
     * AlertDialog positive action button when [Activity.shouldShowRequestPermissionRationale] returns true
     */
    val permissionRationaleAction: String

    /**
     * AlertDialog title when [ACCESS_BACKGROUND_LOCATION] permission is denied
     */
    val backgroundPermissionTitle: String?

    /**
     * AlertDialog body when [ACCESS_BACKGROUND_LOCATION] permission is denied
     */
    val backgroundPermissionText: String

    /**
     * AlertDialog positive action button when [ACCESS_BACKGROUND_LOCATION] permission is denied
     */
    val backgroundPermissionAction: String
}

/**
 * Default [IPermissionTexts] implementation. Can be used as a builder.
 *
 * @see IPermissionTexts
 */
@Parcelize
data class PermissionTexts(
    override val permissionsDeniedTitle: String? = "Location permissions denied",
    override val permissionsDeniedText: String = "Without location permissions you won't get exclusive offers. Please grant location permissions in your settings",
    override val permissionsDeniedAction: String = "Grant permissions",
    override val permissionRationaleTitle: String? = null,
    override val permissionRationaleText: String = "This app needs location permissions to provide you with exclusive offers.",
    override val permissionRationaleAction: String = "Grant permissions",
    override val backgroundPermissionTitle: String? = "Background location",
    override val backgroundPermissionText: String = "This app needs background location permission to provide you with exclusive offers even when you are not in the app. Please enable it in phone's settings.",
    override val backgroundPermissionAction: String = "Enable background location",
) : IPermissionTexts, Parcelable {

    /**
     * Helper constructor to migrate from deprecated [PermissionChecker.PermissionTexts]
     *
     * Also used as Parcelable wrapper for provided [texts]
     */
    internal constructor(texts: IPermissionTexts) : this(
        permissionsDeniedTitle = texts.permissionsDeniedTitle,
        permissionsDeniedText = texts.permissionsDeniedText,
        permissionsDeniedAction = texts.permissionsDeniedAction,
        permissionRationaleTitle = texts.permissionRationaleTitle,
        permissionRationaleText = texts.permissionRationaleText,
        permissionRationaleAction = texts.permissionRationaleAction,
        backgroundPermissionTitle = texts.backgroundPermissionTitle,
        backgroundPermissionText = texts.backgroundPermissionText,
        backgroundPermissionAction = texts.backgroundPermissionAction
    )
}