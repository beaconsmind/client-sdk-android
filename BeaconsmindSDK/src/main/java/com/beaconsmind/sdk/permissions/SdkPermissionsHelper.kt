package com.beaconsmind.sdk.permissions

import android.Manifest
import android.Manifest.permission.ACCESS_BACKGROUND_LOCATION
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.activity.result.ActivityResultCaller
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.beaconsmind.sdk.BeaconListenerService
import com.beaconsmind.sdk.logging.TimberBMS
import com.beaconsmind.sdk.permissions.SdkPermissionsHelper.requestPermissions

/**
 * Helper for all BLE permission related stuff.
 *
 * You can use [requestPermissions] static method to handle missing permissions if you
 * are using [AppCompatActivity] or [FragmentActivity] as your base activity class,
 *
 * [SdkPermissionsHelper.requestPermissions] includes a workaround for android's [ActivityResultCaller] API limitations.
 * It will request and handle permissions even if it's not called from [android.app.Activity.onCreate]
 */
object SdkPermissionsHelper {

    /**
     * Make your caller activity implement this interface if you wish to receive explicit permissions result
     */
    interface ActivityResultCallback {
        /**
         * Callback invoked when user finishes interaction with [SdkPermissionsHelper] and has granted or rejected BLE permissions
         *
         * @param startPossible if true the sdk can successfully start scanning for beacons.
         */
        fun onBeaconsmindPermissionsResult(startPossible: Boolean)
    }

    /**
     * Returns runtime permissions that were not granted, but are required for
     * correct SDK functionality.
     */
    @JvmStatic
    fun getMissingPermissions(context: Context): Set<String> {
        return getRequiredPermissions().filter {
            context.checkSelfPermission(it) != PackageManager.PERMISSION_GRANTED
        }.toSet()
    }

    /**
     * Returns all runtime permissions required by the SDK
     *
     * *Note* that you cannot ask for multiple permissions with [ACCESS_BACKGROUND_LOCATION], it
     * needs to be requested separately.
     */
    @JvmStatic
    @SuppressLint("ObsoleteSdkInt")
    fun getRequiredPermissions(): Set<String> {
        val permissions: MutableSet<String> = HashSet()
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            permissions.add(Manifest.permission.BLUETOOTH_SCAN)
        }

        return permissions
    }

    /**
     * Checks weather we can access BLUETOOTH and LOCATION sensitive APIs.
     * Without these permissions [BeaconListenerService] would crash.
     *
     * *note*: it can START without [ACCESS_BACKGROUND_LOCATION] permission for API 29+
     */
    @JvmStatic
    fun canStartBeaconService(context: Context): Boolean {
        val missingPermissions = getMissingPermissions(context)
        return missingPermissions.isEmpty() ||
                // API 29+ can start beacon monitoring without background location
                missingPermissions.size == 1 && missingPermissions.contains(
            ACCESS_BACKGROUND_LOCATION)
    }

    /**
     * Prompts the user for runtime sdk permission. Displays alerts with [permissionTexts].
     *
     * Returns result via [ActivityResultCallback]
     *
     * @param alwaysAlertOnPermissionsDenied display alert every time permissions are denied.
     * If set to false the user will be prompted exactly once.
     * Disable it for production apps since it can get annoying for the user.
     *
     * @see PermissionCheckerImpl
     * @see _askPermissionsWorkaround
     */
    @JvmStatic
    @JvmOverloads
    fun requestPermissions(
        activity: AppCompatActivity,
        alwaysAlertOnPermissionsDenied: Boolean = false,
        permissionTexts: IPermissionTexts = PermissionTexts(),
    ) {
        try {
            val permissionChecker = PermissionCheckerImpl(
                caller = activity,
                permissionTexts = permissionTexts,
                context = activity,
                activity = activity
            )

            permissionChecker.askForMissingPermission(alwaysAlertOnPermissionsDenied)
        } catch (e: Exception) {
            // Wasn't called from Activity#onCreate
            TimberBMS.w(e, "Exception while requesting permissions, trying a workaround.")
            _askPermissionsWorkaround(
                activity,
                activity.supportFragmentManager,
                permissionTexts,
                alwaysAlertOnPermissionsDenied
            )
        }
    }

    /**
     * Prompts the user for runtime sdk permission. Displays alerts with [permissionTexts].
     *
     * Returns result via [ActivityResultCallback]
     *
     * @param alwaysAlertOnPermissionsDenied display alert every time permissions are denied.
     * If set to false the user will be prompted exactly once.
     * Disable it for production apps since it can get annoying for the user.
     *
     * @see PermissionCheckerImpl
     * @see _askPermissionsWorkaround
     */
    @JvmStatic
    @JvmOverloads
    fun requestPermissions(
        activity: FragmentActivity,
        alwaysAlertOnPermissionsDenied: Boolean = false,
        permissionTexts: IPermissionTexts = PermissionTexts(),
    ) {
        try {
            val permissionChecker = PermissionCheckerImpl(
                caller = activity,
                permissionTexts = permissionTexts,
                context = activity,
                activity = activity
            )

            permissionChecker.askForMissingPermission(alwaysAlertOnPermissionsDenied)
        } catch (e: Exception) {
            // Wasn't called from Activity#onCreate
            TimberBMS.w(e, "Exception while requesting permissions, trying a workaround.")
            _askPermissionsWorkaround(
                activity,
                activity.supportFragmentManager,
                permissionTexts,
                alwaysAlertOnPermissionsDenied
            )
        }
    }
}