package com.beaconsmind.sdk

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.beaconsmind.api.models.StoreBeaconInfo
import com.beaconsmind.sdk.Constants.PREFERENCES_ACCESS_TOKEN
import com.beaconsmind.sdk.Constants.PREFERENCES_BEACONSMIND_CONFIG
import com.beaconsmind.sdk.Constants.PREFERENCES_IMPORTED_USERNAME
import com.beaconsmind.sdk.Constants.PREFERENCES_NAME
import com.beaconsmind.sdk.Constants.PREFERENCES_PERMISSION_PROMPTED
import com.beaconsmind.sdk.Constants.PREFERENCES_PLATFORM_DEVICE_ID
import com.beaconsmind.sdk.Constants.PREFERENCES_REMOTE_CONFIG
import com.beaconsmind.sdk.Constants.PREFERENCES_SERVICE_ACTIVE
import com.beaconsmind.sdk.Constants.PREFERENCES_STORE_BEACONS
import com.beaconsmind.sdk.Constants.PREFERENCES_UNIQUE_ID
import com.beaconsmind.sdk.models.RemoteDebugConfig
import com.beaconsmind.sdk.permissions.PermissionCheckerImpl
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


/**
 * Main object for managing data persisted by the SDK.
 *
 * To obtain an instance use [PersistenceManager.Companion]
 */
internal class PersistenceManager private constructor(context: Context) {

    private val GSON = GsonBuilder()
        .registerTypeAdapter(NativeIdProvider::class.java, NativeIdTypeAdapter<NativeIdProvider>())
        .create()

    private val preferences: SharedPreferences =
        context.applicationContext.getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE)


    /**
     * Unique device ID used to detect app download touchpoint (first time) and connecting logged-in users with app download (phone).
     * Ideally it should be unchanged across app re-installs.
     */
    var deviceUUID: String?
        get() = preferences.getString(PREFERENCES_UNIQUE_ID, null)
        set(uuid) = preferences.edit().putString(PREFERENCES_UNIQUE_ID, uuid).apply()

    /**
     * Server access token persisted when user authenticates.
     */
    var accessToken: String?
        get() = preferences.getString(PREFERENCES_ACCESS_TOKEN, null)
        set(token) = preferences.edit().putString(PREFERENCES_ACCESS_TOKEN, token).apply()

    /**
     * Device notification token persisted when user is authenticated and on token changes.
     */
    var platformDeviceId: String?
        get() = preferences.getString(PREFERENCES_PLATFORM_DEVICE_ID, null)
        set(value) = preferences.edit().putString(PREFERENCES_PLATFORM_DEVICE_ID, value).apply()


    var importedUsername: String?
        get() = preferences.getString(PREFERENCES_IMPORTED_USERNAME, null)
        set(value) = preferences.edit().putString(PREFERENCES_IMPORTED_USERNAME, value).apply()

    /**
     * Persisted service state. True if at one point the client called startListeningBeacons.
     * Listening state will be restored on process (re)start.
     */
    var isServiceActive: Boolean
        get() = preferences.getBoolean(PREFERENCES_SERVICE_ACTIVE, false)
        set(value) = preferences.edit().putBoolean(PREFERENCES_SERVICE_ACTIVE, value).apply()

    /**
     * Persisted beacons so the SDK can immediately startListeningBeacons when process is (re)started.
     */
    var storeBeacons: List<StoreBeaconInfo>?
        get() {
            val json = preferences.getString(PREFERENCES_STORE_BEACONS, "")
            return GSON.fromJson<List<StoreBeaconInfo>>(json)
        }
        set(value) = preferences.edit()
            .putString(PREFERENCES_STORE_BEACONS, GSON.toJson(value))
            .apply()

    /**
     * Persisted state for [PermissionCheckerImpl] prompts.
     */
    var permissionsPrompted: Boolean
        get() = preferences.getBoolean(PREFERENCES_PERMISSION_PROMPTED, false)
        set(value) = preferences.edit().putBoolean(PREFERENCES_PERMISSION_PROMPTED, value).apply()

    /**
     * Config persisted on sdk initialization
     */
    var beaconsmindConfig: BeaconsmindConfig?
        get() {
            val json = preferences.getString(PREFERENCES_BEACONSMIND_CONFIG, "")
            return GSON.fromJson<BeaconsmindConfig>(json)
        }
        set(value) = preferences.edit()
            .putString(PREFERENCES_BEACONSMIND_CONFIG, GSON.toJson(value))
            .apply()

    var remoteConfig: RemoteDebugConfig?
        get() {
            val json = preferences.getString(PREFERENCES_REMOTE_CONFIG, "")
            return GSON.fromJson<RemoteDebugConfig>(json)
        }
        set(value) {
            preferences.edit()
                .putString(PREFERENCES_REMOTE_CONFIG, GSON.toJson(value))
                .apply()
        }

    /**
     *  Clear all persisted data for current user session
     * */
    fun clearSession() {
        accessToken = null
        platformDeviceId = null
        isServiceActive = false
        storeBeacons = null
        permissionsPrompted = false
        beaconsmindConfig = null
    }


    /**
     * [PersistenceManager] singleton instance holder
     */
    companion object {
        @Volatile
        private var INSTANCE: PersistenceManager? = null

        /**
         * One time initialization.
         * Initialized in [BeaconsmindSdk.initialize]
         */
        @JvmStatic
        fun initialize(context: Context) = synchronized(this) {
            val checkInstanceAgain = INSTANCE
            if (checkInstanceAgain == null) {
                val created = PersistenceManager(context)
                INSTANCE = created
            }
        }

        /**
         * Returns [PersistenceManager] singleton instance.
         *
         * Ensure that you called [BeaconsmindSdk.initialize] before obtaining an instance
         *
         * @throws [SdkNotInitializedException] runtime exception if BeaconsmindSdk is not initialized
         */
        @Throws(SdkNotInitializedException::class)
        @JvmStatic
        fun getInstance(): PersistenceManager = guardInitialization {
            synchronized(this) {
                INSTANCE!!
            }
        }

        /**
         * Initializes [PersistenceManager] without SDK initialization.
         * Use it to obtain an instance when sdk initialization is not required (for example in diagnostics request).
         */
        fun getInstanceWithContext(context: Context) = synchronized(this) {
            val checkInstanceAgain = INSTANCE
            if (checkInstanceAgain != null) {
                checkInstanceAgain
            } else {
                val created = PersistenceManager(context.applicationContext)
                INSTANCE = created
                created
            }
        }
    }
}


/**
 * Type adapter for [NativeIdProvider] gson (de)serialization.
 *
 * Serializes [NativeIdProvider.className] and deserializes the object via reflection from className
 */
private class NativeIdTypeAdapter<T : NativeIdProvider> : JsonDeserializer<T>,
    JsonSerializer<T> {

    /**
     * Deserialization from [NativeIdProvider.className]
     */
    @Throws(JsonParseException::class)
    override fun deserialize(
        jsonElement: JsonElement, type: Type?,
        deserializationContext: JsonDeserializationContext,
    ): T {
        val jsonObject: JsonObject = jsonElement.asJsonObject
        val prim: JsonPrimitive = jsonObject.get(CLASSNAME) as JsonPrimitive
        val className: String = prim.asString
        val clazz = getClassInstance(className)
        return deserializationContext.deserialize(jsonObject, clazz)
    }

    /**
     * Serializes [NativeIdProvider.className]
     */
    override fun serialize(
        src: T,
        typeOfSrc: Type?,
        context: JsonSerializationContext,
    ): JsonElement {
        val json = JsonObject()
        json.add(CLASSNAME, JsonPrimitive(src.className))
        return json
    }

    @Suppress("UNCHECKED_CAST")
    private fun getClassInstance(className: String): Class<T> {
        return try {
            Class.forName(className) as Class<T>
        } catch (cnfe: ClassNotFoundException) {
            throw JsonParseException(cnfe.message)
        }
    }

    private companion object {
        const val CLASSNAME = "classname"
    }

}

/**
 * Helper for complex objects JSON serialization
 *
 * @receiver [Gson]
 */
private inline fun <reified T> Gson.fromJson(json: String?) =
    fromJson<T>(json, object : TypeToken<T>() {}.type)